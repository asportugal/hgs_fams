<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function usersList()
    {
        //add items to array below to add users with "default" password; 

        return [
            [
                'username' => 'admin', 
                'email' => 'famsadmin@teamhgs.com', 
                'fullname' => 'HGS FAMS|Administrator|'
            ], 
            [
                'username' => 'hgscustodian', 
                'email' => 'hgscustodian@teamhgs.com',
                'fullname' => 'HGS FAMS|Custodian|'
            ],
            [
                'username' => 'jmperalta', 
                'email' => 'jmperalta@teamhgs.com',
                'fullname' => 'Jose Marie|Peralta|'
            ]
        ];
    }


    public function commonFields()
    {
        return [
            'created_by' => 'SYSTEM',
            'updated_by' => 'SYSTEM',
            'updated_at' => Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s')),
            'created_at' => Carbon::createFromFormat('Y-m-d H:i:s', date('Y-m-d H:i:s'))
        ];
    }


    public function run()
    {
        $this->command->info('User table succesfully seeded!');

        DB::table('users')->truncate();
        DB::table('sites')->truncate();
        DB::table('menus')->truncate();
        // DB::table('lookup_values')->truncate();
        DB::table('site_access')->truncate();
        DB::table('user_permissions')->truncate();

        $this->createUsers($this->usersList());
        $this->generateMenus();
        // $this->generateLookupValues();

        DB::table('sites')->insert([
            'name' => 'HGS Cyberpark - Libis',
            'code' => 'H1800'
        ] + $this->commonFields());
    }

    public function createUsers($usersArray)
    {
        foreach($usersArray as $user) {
            $userID = DB::table('users')->insertGetId([
                'username' => $user['username'],
                'email' => $user['email'],
                'full_name' => $user['fullname'],
                'password' => bcrypt('default'),
                'role' => '1',
                'group' => '0'
            ] + $this->commonFields());

            DB::table('site_access')->insert([
                'site_id' => 1,
                'user_id' => $userID]);
        }
    }

    public function generateMenus()
    {
    	$menus = [
	    	'0||transactions/new-asset', 
            '1||transactions/open-records', 
            '2||transactions/bulk-upload',
			'3||transactions/pre-asset',
			'4||transactions/location-transfer',
			'5||transactions/asset-disposal',
			'6||transactions/asset-replacement',
			'7||transactions/loan-assets',
			'8||transactions/farm-in',
			'9||transactions/farm-out',
            '0||reports/warranty-report',
            '0||administration/user-management',
            '1||administration/asset-tag-printing',
			'2|administration|master-files/categories',
			'3|administration|master-files/subcategories',
			'4|administration|master-files/notifications',
			'5|administration|master-files/vendors',
			'6|administration|master-files/brands',
			'7|administration|master-files/business-entities',
			'8|administration|master-files/sites',
			'9|administration|master-files/employees',
			'10|administration|master-files/departments',
		];

		foreach($menus as $menu)
		{
            $menuAttributes = explode('|', $menu);
            $order = $menuAttributes[0];
            $header = $menuAttributes[1];

			$menuProps = explode('/', $menuAttributes[2]);
			
            $parent = ($header) ? $header . '/' . $menuProps[0] : $menuProps[0];
			$name = ($menuProps[1]) ? $menuProps[1] : $menuProps[0];
			
			$menuID = DB::table('menus')->insertGetId([
                'order' => $order,
                'name' => str_replace('-', ' ', title_case($name)),
                'link' => $menuAttributes[2],
                'transactions' => 'CREATE|EDIT|DELETE',
                'parent' => $parent
        	] + $this->commonFields());

            $i = 1;
            while($i <= 3){
            	DB::table('user_permissions')->insert([
                    'permission' => 'CREATE|EDIT|DELETE',
                    'menu_id' => $menuID,
                    'user_id' => $i
            	]);
                $i++;
            }
		}
    }

    // public function generateLookupValues()
    // {
    // 	$lookupValues = [
    // 		'attachment_type' => [
    // 			'asset_image', 'delivery_attachments', 'asset_attachment', 'employee_image', 'user_image'
    // 		],
    // 		'asset_status' => [
    // 			'in-storage', 'in-use', 'replacement', 'disposed', 'defective', 'repaired',
    //             'sold', 'missing/stolen', 'loaned-out'
    // 		],
    //         'employee_status' => [
    //             'active', 'resigned', 'terminated', 'on-leave', 'on-hold'
    //         ],
    //         'role' => [
    //             'requestor', 'finance', 'approver', 'administrator', 'custodian', 'helpdesk'
    //         ],
    //         'financial_treatment' => [
    //             'capitalized', 'expensed'
    //         ],
    //         'group' => [
    //             'baed', 'i.t.'
    //         ],
    //         'disposal_method' => [
    //             'destruction', 'donation', 'lost/stolen', 'resell', 'vendor-take-back'
    //         ]
    // 	];

    // 	foreach ($lookupValues as $tag => $lookupCategories) {
    // 		foreach($lookupCategories as $k => $category) {
		  //       $name = ($tag != 'entity') ? str_replace('-', ' ', strtoupper($category)): $k;
		  //       DB::table('lookup_values')->insert([
	   //              'tag' => strtoupper($tag),
	   //              'name' => $name,
	   //              'value' => $category
	   //      	] + $this->commonFields());
    // 		}
    // 	}
    // }
}
