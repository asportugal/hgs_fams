<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetReplacementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_replacement', function (Blueprint $table) {
            $table->increments('replacement_id');
            $table->string('asset_id', 50);
            $table->string('itaf_no', 50);
            $table->string('replacement_status', 50);
            $table->string('serial_no', 50);
            $table->string('replacement_approver_group', 50);
            $table->string('replacement_approver', 50);
            $table->string('replacement_remarks', 50);
            $table->timestamp('approved_date')->nullable();
            $table->string('approved_by')->nullable();
            $table->string('requested_by', 100);
            $table->string('created_by', 50);
            $table->string('updated_by', 50);
            $table->string('deleted_by', 50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_replacement');
    }
}
