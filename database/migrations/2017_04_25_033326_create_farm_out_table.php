<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFarmOutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farm_out', function (Blueprint $table) {
            $table->increments('farm_out_id');
            $table->string('control_no', 100);
            $table->string('peza_permit_status', 50)->nullable();
            $table->string('peza_form_no', 50)->nullable();
            $table->string('peza_approval_no', 50)->nullable();
            $table->string('location', 100)->nullable();
            $table->string('vendor', 100)->nullable();
            $table->string('proforma_invoice_no', 100)->nullable();
            $table->timestamp('proforma_invoice_date')->nullable();
            $table->string('final_invoice_no', 100)->nullable();
            $table->timestamp('final_invoice_date')->nullable();
            $table->timestamp('request_date');
            $table->timestamp('date_filed')->nullable();
            $table->string('remarks', 255)->nullable();
            $table->string('code');
            $table->string('created_by', 50);
            $table->string('updated_by', 50);
            $table->string('deleted_by', 50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farm_out');
    }
}
