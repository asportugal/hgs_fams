<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_assets', function (Blueprint $table) {
            $table->increments('loan_id');
            $table->string('asset_id', 50);
            $table->string('itaf_no', 50);
            $table->string('checked_out_by', 100)->nullable();
            $table->timestamp('checked_out_date')->nullable();
            $table->string('checked_in_by', 100)->nullable();
            $table->timestamp('checked_in_date')->nullable();
            $table->string('checked_in_remarks', 100)->nullable();
            $table->string('loan_to', 100);
            $table->string('loan_approver_group', 50);
            $table->string('loan_approver', 50);
            $table->string('loan_status', 50);
            $table->timestamp('loan_start_date');
            $table->timestamp('loan_end_date');
            $table->timestamp('approved_date')->nullable();
            $table->string('approved_by')->nullable();
            $table->string('loan_remarks', 100);
            $table->string('requested_by', 100);
            $table->string('created_by', 50);
            $table->string('updated_by', 50);
            $table->string('deleted_by', 50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_assets');
    }
}
