<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('delivery_no', 50)->nullable();
            $table->string('received_by', 50)->nullable();
            $table->timestamp('delivery_date')->nullable();
            $table->string('airway_delivery_no', 50)->nullable();
            $table->timestamp('airway_delivery_date')->nullable();
            $table->string('shipper', 50)->nullable();
            $table->string('cargo', 50)->nullable();
            $table->string('consignee_name', 50)->nullable();
            $table->string('code');
            $table->string('created_by', 50);
            $table->string('updated_by', 50);
            $table->string('deleted_by', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
