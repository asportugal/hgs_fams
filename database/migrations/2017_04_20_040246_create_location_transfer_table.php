<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationTransferTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('location_transfer', function (Blueprint $table) {
            $table->increments('location_transfer_id');
            $table->string('asset_id', 50);
            $table->string('control_no', 50)->nullable();
            $table->string('itaf_no', 50);
            $table->string('transfer_status', 50);
            $table->string('from_site', 50);
            $table->string('from_floor', 50)->nullable();
            $table->string('from_cube', 50)->nullable();
            $table->string('to_site', 50);
            $table->string('to_floor', 50)->nullable();
            $table->string('to_cube', 50)->nullable();
            $table->string('transfer_approver_group', 50);
            $table->string('transfer_approver', 50);
            $table->string('transfer_remarks', 50);
            $table->timestamp('approved_date')->nullable();
            $table->string('approved_by')->nullable();
            $table->string('requested_by', 100);
            $table->string('created_by', 50);
            $table->string('updated_by', 50);
            $table->string('deleted_by', 50)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('location_transfer');
    }
}
