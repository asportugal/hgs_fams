<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFarmInItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farm_in_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('farm_in_id');
            $table->integer('delivery_id')->nullable();
            $table->string('description');
            $table->bigInteger('quantity');
            $table->integer('status')->default(0);
            $table->string('currency');
            $table->bigInteger('amount');
            $table->timestamp('warranty_start_date')->nullable();
            $table->timestamp('warranty_end_date')->nullable();
            $table->text('notes')->nullable();
            $table->string('created_by', 50);
            $table->string('updated_by', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farm_in_items');
    }
}
