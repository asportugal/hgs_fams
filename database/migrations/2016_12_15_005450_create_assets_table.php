<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('asset_id', 50)->unique();
            $table->string('amr_no', 50)->nullable();
            $table->string('gr_no', 50)->nullable();
            $table->string('serial_no', 50);
            $table->integer('category_id');
            $table->integer('subcategory_id');
            $table->integer('business_entity_id');
            $table->integer('brand_id');
            $table->integer('employee_id')->nullable();
            $table->string('model')->nullable();
            $table->string('floor')->nullable();
            $table->string('cube')->nullable();
            $table->integer('farm_in_item_id');
            $table->integer('farm_out_item_id');
            $table->timestamp('tag_date')->nullable();
            $table->text('notes')->nullable();
            $table->integer('status');
            $table->integer('vat_paid');
            $table->timestamp('service_date')->nullable();
            $table->string('maintenance_vendor', 50)->nullable();
            $table->timestamp('maintenance_start_date')->nullable();
            $table->timestamp('maintenance_end_date')->nullable();
            $table->text('maintenance_remark');
            $table->string('maintenance_email_notify')->nullable();
            $table->string('contract_type', 50)->nullable();
            $table->timestamp('contract_start_date')->nullable();
            $table->timestamp('contract_end_date')->nullable();
            $table->text('contract_remarks')->nullable();
            $table->integer('financial_treatment');
            $table->string('local_cost_center', 50)->nullable();
            $table->string('cost_center', 50)->nullable();
            $table->bigInteger('asset_life')->nullable();
            $table->bigInteger('monthly_depreciation')->nullable();
            $table->bigInteger('loa_no')->nullable();
            $table->timestamp('loa_valid_until_date')->nullable();
            $table->integer('consumed_asset_life')->nullable();
            $table->bigInteger('netbook_value');
            $table->bigInteger('remaining_asset_life')->nullable();
            $table->timestamp('date_fully_depreciated')->nullable();
            $table->string('code');
            $table->string('created_by', 50);
            $table->string('updated_by', 50);
            $table->string('deleted_by', 50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
