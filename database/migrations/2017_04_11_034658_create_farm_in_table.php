<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFarmInTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('farm_in', function (Blueprint $table) {
            $table->increments('id');
            $table->string('purchase_order_no', 100);
            $table->integer('peza_permit_status');
            $table->string('peza_form_no', 50)->nullable();
            $table->string('peza_approval_no', 50)->nullable();
            $table->integer('site_id');
            $table->integer('vendor_id');
            $table->string('proforma_invoice_no', 100)->nullable();
            $table->timestamp('proforma_invoice_date')->nullable();
            $table->string('final_invoice_no', 100)->nullable();
            $table->timestamp('final_invoice_date')->nullable();
            $table->timestamp('request_date')->nullable();
            $table->string('remarks', 255)->nullable();
            $table->string('code');
            $table->string('created_by', 50);
            $table->string('updated_by', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('farm_in');
    }
}
