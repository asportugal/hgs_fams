<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Dashboard
Route::get('/', 'DashboardController@index')->name('home');
Route::get('/home', 'DashboardController@index')->name('home');

// Pages
Route::get('/transactions/asset-disposal', 'PageController@asset_disposal')->name('asset-disposal');
Route::get('/transactions/asset-replacement', 'PageController@asset_replacement')->name('asset-replacement');
Route::get('/transactions/bulk-upload', 'PageController@bulk_upload')->name('bulk-upload');
Route::get('/transactions/loan-assets', 'PageController@loan_asset')->name('loan-assets');
Route::get('/transactions/location-transfer', 'PageController@location_transfer')->name('location-transfer');
Route::get('/transactions/new-asset', 'PageController@new_asset')->name('new-asset');
Route::get('/transactions/open-records', 'PageController@open_records')->name('open-records');
Route::get('/transactions/pre-asset', 'PageController@pre_asset')->name('pre-asset');
Route::get('/transactions/farm-in', 'PageController@farm_in')->name('farm-in');
Route::get('/transactions/farm-out', 'PageController@farm_out')->name('farm-out');
Route::get('/master-files/categories', 'PageController@categories')->name('categories');
Route::get('/master-files/subcategories', 'PageController@subcategories')->name('subcategories');
Route::get('/master-files/brands', 'PageController@brands')->name('brands');
Route::get('/master-files/sites', 'PageController@sites')->name('sites');
Route::get('/master-files/departments', 'PageController@departments')->name('departments');
Route::get('/master-files/employees', 'PageController@employees')->name('employees');
Route::get('/master-files/notifications', 'PageController@notifications')->name('notifications');
Route::get('/master-files/vendors', 'PageController@vendors')->name('vendors');
Route::get('/master-files/business-entities', 'PageController@business_entity')->name('business-entities');
Route::get('/administration/user-management', 'PageController@user_management')->name('user-management');
Route::get('/administration/print-assets', 'PageController@print_assets')->name('print-assets');
Route::get('/administration/request-approvers', 'PageController@request_approvers')->name('request-approvers');
Route::get('/change-password', 'PageController@change_password')->name('change-password');
Route::get('/reports/warranty-report', 'PageController@warranty_report')->name('warranty-report');

//Print Routes
Route::get('/location-transfer/print/{id?}', 'LocationTransferController@print_form')->name('location-transfer.print');

// Export Routes
Route::get('/categories/export/{type?}', 'CategoryController@export')->name('categories.export');
Route::get('/subcategories/export/{type?}', 'SubcategoryController@export')->name('subcategories.export');
Route::get('/brands/export/{type?}', 'BrandController@export')->name('brands.export');
Route::get('/sites/export/{type?}', 'SiteController@export')->name('sites.export');
Route::get('/notifications/export/{type?}', 'NotificationController@export')->name('notifications.export');
Route::get('/vendors/export/{type?}', 'VendorController@export')->name('vendors.export');
Route::get('/departments/export/{type?}', 'DepartmentController@export')->name('departments.export');
Route::get('/employees/export/{type?}', 'EmployeeController@export')->name('employees.export');
Route::get('/business-entities/export/{type?}', 'BusinessEntityController@export')->name('business-entities.export');
Route::get('/user-management/export/{type?}', 'UserController@export')->name('user-management.export');
Route::get('/pre-asset/export/{type?}', 'PreAssetController@export')->name('pre-asset.export'); 
Route::get('/farm-in/export/{type?}', 'FarmInController@export')->name('farm-in.export');
Route::get('/farm-out/export/{type?}', 'FarmOutController@export')->name('farm-out.export');
Route::get('/open-records/export/{type?}', 'AssetController@export')->name('open-records.export');
Route::get('/loan-assets/export/{type?}', 'LoanAssetController@export')->name('loan-assets.export');
Route::get('/asset-disposal/export/{type?}', 'AssetDisposalController@export')->name('asset-disposal.export');
Route::get('/location-transfer/export/{type?}', 'LocationTransferController@export')->name('location-transfer.export');
Route::get('/asset-replacement/export/{type?}', 'AssetReplacementController@export')->name('asset-replacement.export');
Route::get('/new-asset/export/{type?}', 'PreAssetController@pendings_export')->name('new-asset.export');

// Single Route API's
Route::post('/bulk-upload/employees', 'BulkUploadController@employees')->name('bulk.employees');
Route::post('/bulk-upload/assets', 'BulkUploadController@assets')->name('bulk.assets');
Route::get('/attachments/{type}/{id}', 'AttachmentController@show')->name('attachment.show');
Route::get('/asset/{fragment}', 'AssetController@generate')->name('asset.generate');
Route::get('/search-employee', 'EmployeeController@name')->name('employee.name');
Route::get('/search-employees', 'EmployeeController@search')->name('employee.search');
Route::get('/search-pos', 'FarmInController@search')->name('farm-in.search');
Route::get('/po-details/{po}', 'FarmInController@details')->name('farm-in.details');
Route::get('/search-users', 'UserController@search')->name('users.search');
Route::post('/change-password', 'UserController@change')->name('change');
Route::put('/asset-loan/{id}', 'LoanAssetController@action')->name('loan.action');
Route::put('/user-management/reset-password/{id}', 'UserController@reset')->name('user-management.reset');
Route::put('/user-management/set-activation/{id}', 'UserController@activation')->name('user-management.activation');
Route::put('/dispose-asset/{id}', 'AssetDisposalController@action')->name('disposal.action');
Route::put('/replace-asset/{id}', 'AssetReplacementController@action')->name('replace.action');
Route::put('/transfer-asset/{id}', 'LocationTransferController@action')->name('transfer.action');
Route::get('/forgot-password', 'UserController@forgot')->name('forgot-password');
Route::get('/approval/request/{id}', 'ApprovalController@details')->name('request');
Route::get('/request', 'ApprovalController@action')->name('request.action');
Route::get('/history/{id}', 'HistoryController@retrieve')->name('history');
Route::get('/employee/{id}', 'EmployeeController@accountable')->name('accountable');
Route::get('/assets-summary/{scope?}', 'AssetController@summary')->name('asset-summary');
Route::get('/report/warranty/{scope?}', 'ReportController@warranty')->name('report.warranty');
Route::get('/subcategories/name/{id}', 'SubcategoryController@name')->name('subcategories.name');
Route::get('/search-menus', 'UserController@menus')->name('user.menus');
Route::get('/pending-deliveries', 'PreAssetController@pending')->name('pre_asset.pending');
Route::get('/print-assets', 'PrintAssetController@search')->name('print-asset.search');
Route::get('/print-assets/initialize/{printer}', 'PrintAssetController@initialize')->name('print-asset.initialize');
Route::get('/line-items/{po}', 'PreAssetController@items')->name('print-asset.items');

// Resource Routes
Route::resource('approvals', 'ApprovalController', ['only' => ['update']]);
Route::resource('categories', 'CategoryController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::resource('subcategories', 'SubcategoryController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::resource('vendors', 'VendorController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::resource('brands', 'BrandController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::resource('notifications', 'NotificationController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::resource('sites', 'SiteController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::resource('business-entities', 'BusinessEntityController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::resource('employees', 'EmployeeController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::resource('departments', 'DepartmentController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::resource('pre-asset', 'PreAssetController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::resource('loan-assets', 'LoanAssetController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);
Route::resource('asset-disposal', 'AssetDisposalController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);
Route::resource('asset-replacement', 'AssetReplacementController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);
Route::resource('location-transfer', 'LocationTransferController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);
Route::resource('new-asset', 'AssetController', ['only' => ['store']]);
Route::resource('user-management', 'UserController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::resource('open-records', 'AssetController', ['only' => ['index', 'show', 'update', 'destroy']]);
Route::resource('farm-in', 'FarmInController', ['only' => ['index', 'show', 'store', 'update', 'destroy']]);
Route::resource('farm-out', 'FarmOutController', ['only' => ['index', 'show', 'update', 'destroy']]);
Route::resource('lookup-values', 'LookupController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);
Route::resource('request-approvers', 'RequestApproverController', ['only' => ['index', 'store', 'show', 'update', 'destroy']]);

Route::get('/fillables', function(Illuminate\Http\Request $request, \App\Models\Asset $asset) {
	if($request->query('q')) {
		$query = $request->query('q');
		$fillables = array_filter($asset->getFillable(), function($value) use ($query) {
			return str_contains($value, strtolower($query));
		});
	} else {
		$fillables = $asset->getFillable();
	}
	return objarr_encode(array_values($fillables));
})->name('get-fillables');