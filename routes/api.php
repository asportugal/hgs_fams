<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');

Route::post('/login', function(Request $request) {
	echo var_dump($request->all());
	// echo var_dump($request->input('username'));
	// $response = response('Invalid Credentials', 500);
	// if (Auth::attempt(['username' => 'admin', 'password' => 'default'], 1)) {
	//      $response = response(Auth::user()->remember_token, 200);
	// }
	// return $response;
});
Route::post('api/asset_details/{id}', function(Request $request, $id) {
	// $remember = \App\User::where('remember_token', $request->input('token'))->get();
	// $response = response('Invalid Credentials', 401);
	// if(!$remember->isEmpty()) {
		$asset_info = \App\Asset::where('asset_id', $id)->first();
		if($asset_info) {
			$assetArray = [
				$asset_info->gr_no,
				$asset_info->serial_no,
				$asset_info->amr_no,
				$asset_info->category,
				$asset_info->subcategory,
				$asset_info->supplier,
				$asset_info->lob_owner,
				$asset_info->bldg,
				$asset_info->floor,
				$asset_info->cube,
				$asset_info->employee,
				$asset_info->brand,
				$asset_info->model,
				$asset_info->notes,
				$asset_info->sales_invoice_no,
				$asset_info->delivery_receipt_no,
				$asset_info->delivery_receipt_date,
				$asset_info->purchase_order_no,
				$asset_info->purchase_order_date,
				$asset_info->tag_date,
				$asset_info->service_date,
				$asset_info->warranty_start_date,
				$asset_info->warranty_end_date,
			];
			$assetConcats = implode('|', $assetArray);
			$response = response($assetConcats, 200);	
		} else {
			$response = response('No asset details found', 404);
		}
	// }
	return $response;
});
