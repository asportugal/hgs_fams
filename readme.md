<h1>Fixed Asset Management System</h1>
The FAMS is a system which purpose is to track assets preventive maintance their current location, quantity and condition.

<h1>Installing</h1>
1. Clone this repository
	- Open cmd in your desired project directory and run "git clone https://gitlab.com/asportugal/hgs_fams".
2. Fetch project dependencies
	- Inside your project directory open cmd and run "npm install".
3. Fetch composer dependencies
	- Inside your project directory open cmd and run "composer update".
4. Migrate database columns
	- Create a new local database.
	- Inside your project directory open cmd and run "php artisan migrate & php artisan db:seed".
5. Serve!
	- Inside your project directory open cmd and run "php artisan serve".

<h1>Author</h1>
- Andrew Portugal