@extends('base')
@section('content')
@setvar('view', 'lookup-values')
<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">
	
		<div class="form-group" :class="{'has-error' : form.errors.has('tag') }">
			<label>Tag</label>
			<multiselect id="tag" :options="@arr2str($tags)" :searchable="true" :show-labels="false" name="tag" placeholder = "" v-model="form.tag"></multiselect>
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('tag')"></span>
		</div>

		<div class="form-group" :class="{'has-error' : form.errors.has('name') }">
			<label>Name</label>
			<input type="type" class="form-control" name="name"  v-model="form.name" required :disabled="form.disabled">
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('name')"></span>
		</div>

	</form>
	
	<div class="table-section">
		<dataset api="{{ route('lookup-values.index') }}" primary="lookup_id" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)"></dataset>
	</div>

</div>
@endsection
