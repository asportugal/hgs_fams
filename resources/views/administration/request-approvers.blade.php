@extends('base')
@section('content')
@setvar('view', 'request-approvers')
<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">
	
		<div class="form-group" :class="{'has-error' : form.errors.has('group_name') }">
			<label>Group</label>
			<input type="type" class="form-control" name="group_name"  v-model="form.group_name" required :disabled="form.disabled">
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('group_name')"></span>
		</div>

		<div class="form-group" :class="{'has-error' : form.errors.has('approver'), 'disabled' : form.disabled }">
			<label>Approver:</label>
			<multiselect :options="users" v-model="user" :searchable="true" :loading="isLoading" :internal-search="true" @search-change="asyncFindUsers" ref="approvers" :custom-label="customLabel" :show-labels="false" track-by="full_name" label="full_name" placeholder="">
				<template slot="option" scope="props">
				<div class="option__desc"><span class="option__title">@{{ props.option.full_name }}</span><span class="option__small"><p class="pull-right">@{{ props.option.username }}</p></span></div>
	    		</template>
				<span slot="noResult">No records found.</span>
	    	</multiselect>
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('approver')"></span>
		</div>

	</form>
	
	<div class="table-section">
		<dataset utils="{{ json_encode(['search', 'export']) }}" api="{{ route('request-approvers.index') }}" primary="approver_id" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)"></dataset>
	</div>

</div>
@endsection
