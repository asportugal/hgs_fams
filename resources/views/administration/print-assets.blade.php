@extends('base')
@section('content')
@setvar('view', 'print-assets')
<div :class="component">
	<form method="POST" ref="{{ $view }}">
		<div class="row" ref="form">
			<div class="heading text-right">
				<h4>Search Assets</h4> 
				<button @click="searchAssets()" type="button" class="btn btn-primary">Search <i class="fa fa-fw fa-search"></i></button>
			</div>
			<div class="content">
				<div>
					<div class="form-group horizontal">
						<label>Asset ID:</label>
						<input type="text" class="form-control" name="asset_id" v-model="form.asset_id">
					</div>
					<div class="form-group horizontal">
						<label>AMR No:</label>
						<input type="text" class="form-control" name="amr_no" v-model="form.amr_no">
					</div>
					<div class="form-group horizontal">
						<label>GR No:</label>
						<input type="text" class="form-control" name="gr_no" v-model="form.gr_no">
					</div>
					<div class="form-group horizontal">
						<label>Serial No:</label>
						<input type="text" class="form-control" name="serial_no" v-model="form.serial_no">
					</div>
					<div class="form-group horizontal">
						<label>Description:</label>
						<input type="text" class="form-control" name="description" v-model="form.description">
					</div>
					<div class="form-group horizontal">
						<label>Brand:</label>
						<combobox :options="@arr2str($brands)" name="brand" :searchable="true" v-model="form.brand"></combobox>
					</div>
					<div class="form-group horizontal">
						<label>Model:</label>
						<input type="text" class="form-control" name="model" v-model="form.model">
					</div>
				</div>
				<div>
					<div class="form-group horizontal">
						<label>Status:</label>
						<combobox :options="@arr2str($status)" name="status" :searchable="false" v-model="form.status"></combobox>
					</div>
					<div class="form-group horizontal">
						<label>Supplier:</label>
						<combobox :options="@arr2str($vendors)" name="vendor" :searchable="true" v-model="form.vendor"></combobox>
					</div>
					<div class="form-group horizontal">
						<label>Category:</label>
						<combobox :options="@arr2str($categories)" name="category" :searchable="true" v-model="form.category"></combobox>
					</div>
					<div class="form-group horizontal">
						<label>Subcategory:</label>
						<input type="text" class="form-control" name="subcategory" v-model="form.subcategory">
					</div>
					<div class="form-group horizontal">
						<label>Lob Owner:</label>
						<combobox :options="@arr2str($lobs)" name="lob_owner" :searchable="true" v-model="form.lob_owner"></combobox>
					</div>
					<div class="form-group horizontal">
						<label>Invoice No:</label>
						<input type="text" class="form-control" name="invoice_no" v-model="form.invoice_no">
					</div>
					<div class="form-group horizontal">
					    <label>Delivery Date:</label>
					    <div class="input-group">
						    <datepicker v-model="form.delivery_receipt_date" :options="{allowInput: true, dateFormat: 'Y-m-d' }" name="delivery_receipt_date"></datepicker> 
						    <span class="input-group-addon">
		                        <i class="fa fa-fw fa-calendar"></i>
		                    </span>
	                    </div>
					</div>
				</div>
				<div>
					<div class="form-group horizontal">
						<label>Bldg:</label>
						<combobox :options="@arr2str($sites)" name="bldg" :searchable="true" v-model="form.bldg"></combobox>
					</div>
					<div class="form-group horizontal">
						<label>Floor:</label>
						<input type="text" class="form-control" name="floor" v-model="form.floor">
					</div>
					<div class="form-group horizontal">
						<label>Cube:</label>
						<input type="text" class="form-control" name="cube" v-model="form.cube">
					</div>
					<div class="form-group horizontal">
						<label>Employee:</label>
						<input type="text" class="form-control" name="employee" v-model="form.employee">
					</div>
					<div class="form-group horizontal">
						<label>PO No:</label>
						<input type="text" class="form-control" name="purchase_order_no" v-model="form.purchase_order_no">
					</div>
					<div class="form-group horizontal">
						<label>DR No:</label>
						<input type="text" class="form-control" name="delivery_receipt_no" v-model="form.delivery_receipt_no">
					</div>
				</div>
			</div>
		</div>
		<div class="print-utils" v-show="toFront">
			<div class="form-group horizontal" style="padding-left: 0">
				<button type="button" class="btn btn-primary" @click="toggleForm()">Display Form
				</button>
			</div>
			<div class="form-group horizontal">
				<label style="margin-right: 10px;">Printer Name:</label>
				<combobox :options="@arr2str($printers)" name="printers" v-model="printer_name"></combobox>
			</div>
		</div>
		<hr style="display: block; width: 100%">
		<dataset ref="records" :marker="true" :hide-primary="false" primary="asset_id" api="{{ route('print-asset.search') }}" :paginate="true" :async="true" :info="true" :excludes="['active', 'vat_paid', 'maintenance_vendor', 'maintenance_start_date', 
		'maintenance_end_date', 'maintenance_remark', 'maintenance_email_notify', 
		'contract_type', 'contract_start_date', 'contract_end_date', 'contract_remarks', 'financial_treatment', 'local_cost_center',
		'cost_center', 'asset_life', 'monthly_depreciation', 'loa_no', 'loa_valid_until_date', 'consumed_asset_life', 'netbook_value', 
		'remaining_asset_life', 'date_fully_depreciated', 'purchase_price_dollar', 'purchase_price_php', 'pre_asset', 'farm_in']"></dataset>
	</form>
</div>
@endsection