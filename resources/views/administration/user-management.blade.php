@extends('base', ['search' => 'true'])
@section('content')
@setvar('view', 'user-management')

<modal ref="modal" size="90" title="Users">
	<dataset api="{{ route('user-management.index') }}" primary="id" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('user-management.export') }}"></dataset>
</modal>

<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">
		<div>

			@if(Auth::user()->getOriginal('role') == App\Models\User::ROLE_ADMINISTRATOR)
				<div v-if="identifier" class="form-group text-right">
					<button v-if="enabled" type="button" class="btn btn-danger" value="0" @click="setActivation($event)">
						<i></i> Deactivate Account
					</button>
					<button v-else type="button" class="btn btn-success" value="1" @click="setActivation($event)">
						<i></i> Activate Account
					</button>
					<button type="button" class="btn btn-warning" @click="resetPassword($event)">
						<i></i> Reset Password
					</button>
				</div>
			@endif

			<div class="form-group" :class="{'has-error' : form.errors.has('username') }">
				<label>Username:</label>
				<input type="text" class="form-control" name="username" v-model="form.username" :disabled="form.disabled || identifier">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('username')"></span>
			</div>

			<div class="form-group horizontal"  :class="{'has-error' : form.errors.has('role'), 'disabled' : form.disabled }">
				<label>Role:</label>
				<multiselect :options="{{ $roles }}" :disabled="form.disabled" name="role" v-model="form.role"></multiselect>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('role')"></span>
			</div>

			<div class="form-group horizontal"  :class="{'has-error' : form.errors.has('group'), 'disabled' : form.disabled }">
					<label>Group:</label>
					<div style="display: inline-block; margin-left: 10px" ref="groups">
						@foreach($groups as $key => $group)
							@if($key > 0) 
							<label class="group-checkbox">
							<input type="checkbox" :checked="this.form.group == '0' || this.form.group == '{{ $key }}'" @click="setGroup($event)" :disabled="form.disabled" value="{{ $key }}">{{ strtoupper($group) }}</label>
							@endif
						@endforeach
					</div>
			</div>

			<div class="form-group" :class="{'has-error' : form.errors.has('email') }">
				<label>Email:</label>
				<input type="email" class="form-control" name="email" v-model="form.email" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('email')"></span>
			</div>

			<div class="form-group" :class="{'has-error' : form.errors.has('first_name') }">
				<label>First Name:</label>
				<input type="first_name" class="form-control" name="first_name" v-model="form.first_name" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('first_name')"></span>
			</div>

			<div class="form-group" :class="{'has-error' : form.errors.has('middle_name') }">
				<label>Middle Name:</label>
				<input type="middle_name" class="form-control" name="middle_name" v-model="form.middle_name" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('middle_name')"></span>
			</div>

			<div class="form-group" :class="{'has-error' : form.errors.has('last_name') }">
				<label>Last Name:</label>
				<input type="last_name" class="form-control" name="last_name" v-model="form.last_name" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('last_name')"></span>
			</div>

			<div class="form-group"  :class="{'disabled': form.disabled, 'has-error' : form.errors.has('scope') }">
				<label>Scope:</label>
				<multiselect v-model="form.scope" :disabled="form.disabled" :multiple="true" :options="{{ $sites }}" name="scope" :searchable="true"></multiselect>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('scope')"></span>
			</div>
		</div>		
		<div>
			<div class="panel-head">Access Control</div>
			<div class="panel-content">
				<div class="list-utils">
					<div class="form-group" :class="{'has-error' : form.errors.has('modules'), 'disabled' : form.disabled }">
						<multiselect ref="menus" :disabled="form.disabled" :options="parseOption({{ $menus }})" v-model="form.modules" name="modules" :searchable="true" :multiple="true" :input-value="current" :hide-tags="true" @remove="getRemoved($event)" :close-on-select="true" @select="getSelected($event)"></multiselect>
					</div>
					<div class="form-group">
						<div v-if="transactions" class="checkbox">
							<label v-for="(i, item) in transactions">
							<input type="checkbox" v-model="transactions[item]" value="1">@{{ item }}</label>
							<button class="btn btn-default" :disabled=" this.form.errors.has('user_permission')" @click="addUserPrivilege()"><i class="fa fa-fw fa-plus"></i></button>
						</div>
					</div>
				</div>
			</div>
			<table class="table table-hover" v-if="form.modules.length" :class="{ 'disabled' : form.disabled }">
				<thead>
					<th>Modules</th>
					<th>Permission</th>
				</thead>
				<tbody>
					<tr v-for="(item, index) in accessPrivilege" v-on:click="!form.disabled && displaySelected(index)">
						<td>@{{ item.name }}</td>
						<td>@{{ item.permission }}</td>
					</tr>
				</tbody>
			</table>
		</div>
	</form>
</div>
@endsection
