@extends('base')
@section('content')
@setvar('view', 'sites')
<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">
		

		<div class="form-group" :class="{'has-error' : form.errors.has('site_name') }">
			<label>Site Name</label>
			<input type="type" class="form-control" name="site_name"  v-model="form.site_name" required :disabled="form.disabled">
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('site_name')"></span>
		</div>

        <div class="form-group" :class="{'has-error' : form.errors.has('site_code') }">
			<label>Site Code</label>
			<input type="type" class="form-control" name="site_code"  v-model="form.site_code" required :disabled="form.disabled">
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('site_code')"></span>
		</div>

        <div class="form-group" :class="{'has-error' : form.errors.has('address') }">
			<label>Address</label>
			<textarea class="form-control" name="address" rows="10" v-model="form.address" :disabled="form.disabled"></textarea>
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('address')"></span>
		</div>
	</form>
	<div class="table-section">
		<dataset api="{{ route('sites.index') }}" primary="site_id" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :custom-header="headerDefs" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('sites.export') }}"></dataset>
	</div>

</div>

@endsection
