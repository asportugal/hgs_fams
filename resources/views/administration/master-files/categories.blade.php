@extends('base')
@section('content')
@setvar('view', 'categories')
<div :class="component">
	<form method="POST" @submit.prevent ref="{{ $view }}" @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">

		<div class="form-group" :class="{'has-error' : form.errors.has('category_name')}">
			<label>Category Name</label>
			<input type="text" class="form-control" name="category_name" v-model="form.category_name" :disabled="form.disabled">
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('category_name')"></span>
		</div>

		<div class="form-group" :class="{'has-error' : form.errors.has('category_code')}">
			<label>Category Code</label>
			<input type="text" class="form-control" name="category_code" v-model="form.category_code" :disabled="form.disabled">
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('category_code')"></span>
		</div>
	</form>
	
	<div class="table-section">
		<dataset api="{{ route('categories.index') }}" primary="category_id" :excludes="['type']" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" :custom-header="headerDefs" export="{{ route('categories.export') }}"></dataset>
	</div>
</div>
@endsection