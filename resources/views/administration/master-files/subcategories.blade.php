@extends('base')
@section('content')
@setvar('view', 'subcategories')
<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">

		<div class="form-group" :class="{'has-error' : form.errors.has('category'), 'disabled' : form.disabled} ">
			<label>Category</label>
			<combobox :options="{{ $categories }}" name="category" v-model="form.category" :searchable="true" label="name" track-by="code"></combobox>
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('category')"></span>
		</div>

		<div class="form-group" :class="{'has-error' : form.errors.has('subcategory_name') }">
			<label>Subcategory Name</label>
			<input type="type" class="form-control" name="subcategory_name" v-model="form.subcategory_name" :disabled="form.disabled">
			<div :is="form.errors.message" v-bind:error-message="form.errors.get('subcategory_name')"></div>
		</div>

	</form>
	<div class="table-section">
		<dataset api="{{ route('subcategories.index') }}" primary="category_id" :excludes="['type']" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" :custom-header="headerDefs" export="{{ route('subcategories.export') }}"></dataset>
	</div>
</div>

@endsection
