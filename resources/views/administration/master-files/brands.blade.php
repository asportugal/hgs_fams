@extends('base')
@section('content')
@setvar('view', 'brands')
<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">
	
		<div class="form-group" :class="{'has-error' : form.errors.has('brand_name') }">
			<label>Brand Name</label>
			<input type="type" class="form-control" name="brand_name"  v-model="form.brand_name" required :disabled="form.disabled">
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('brand_name')"></span>
		</div>

	</form>
	
	<div class="table-section">
		<dataset api="{{ route('brands.index') }}" primary="brand_id" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('brands.export') }}"></dataset>
	</div>

</div>
@endsection
