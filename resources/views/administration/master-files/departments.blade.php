@extends('base')
@section('content')
@setvar('view', 'departments')
<div :class="component">
    <form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">
    
        <div class="form-group" :class="{'has-error' : form.errors.has('department_name') }">
            <label>Department Name</label>
            <input type="type" class="form-control" name="department_name"  v-model="form.department_name" required :disabled="form.disabled">
            <span :is="form.errors.message" v-bind:error-message="form.errors.get('department_name')"></span>
        </div>
        
    </form>
    
    <div class="table-section">
        <dataset api="{{ route('departments.index') }}" primary="department_id" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('departments.export') }}"></dataset>
    </div>

</div>
@endsection
