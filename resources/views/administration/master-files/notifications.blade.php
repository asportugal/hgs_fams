@extends('base')
@section('content')
@setvar('view', 'notifications')
<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent>
		<div class="form-group" :class="{'has-error' : form.errors.has('group_name') }">
			<label>Group Name</label>
			<input @keyup="form.errors.clear('group_name')" type="type" class="form-control" name="group_name"  v-model="form.group_name" required :disabled="form.disabled">
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('group_name')"></span>
		</div>

        <div class="form-group" :class="{'has-error' : has_error('to_address') }">
			<label>To Address: </label><p class="field-info pull-right help-block"><i class="fa fa-fw fa-info-circle"></i>Insert multiple emails separated by comma ";"</p>
			<textarea rows="5" class="form-control" name="to_address" v-model="to" required :disabled="form.disabled"></textarea>
			<span :is="form.errors.message" v-bind:error-message="get_error('to_address')"></span>
		</div>

		<div class="form-group" :class="{'has-error' : has_error('cc_address') }">
			<label>Cc Address: </label><p class="field-info pull-right help-block"><i class="fa fa-fw fa-info-circle"></i>Insert multiple emails separated by comma ";"</p>
			<textarea rows="5" class="form-control" name="cc_address" v-model="cc" required :disabled="form.disabled"></textarea>
			<span :is="form.errors.message" v-bind:error-message="get_error('cc_address')"></span>
		</div>

	</form>

	<div class="table-section">
		<dataset api="{{ route('notifications.index') }}" primary="notification_id" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('notifications.export') }}"></dataset>
	</div>
</div>

@endsection
