@extends('base')
@section('content')
@setvar('view', 'business-entities')
<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">
	
		<div class="form-group" :class="{'has-error' : form.errors.has('lob') }">
			<label>LOB Owner</label>
			<input type="type" class="form-control" name="lob"  v-model="form.lob" required :disabled="form.disabled">
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('lob')"></span>
		</div>

	</form>
	<div class="table-section">
		<dataset api="{{ route('business-entities.index') }}" primary="business_entity_id" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" :custom-header="{'name': 'LOB Owner'}"  export="{{ route('business-entities.export') }}"></dataset>
	</div>
</div>

@endsection
