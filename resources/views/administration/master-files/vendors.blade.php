@extends('base')
@section('content')
@setvar('view', 'vendors')
<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">

		<div class="form-group" :class="{'has-error' : form.errors.has('supplier_name') }">
			<label>Supplier Name</label>
			<input type="type" class="form-control" name="supplier_name"  v-model="form.supplier_name" required :disabled="form.disabled">
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('supplier_name')"></span>
		</div>

        <div class="form-group" :class="{'has-error' : form.errors.has('contact_person') }">
			<label>Contact Person</label>
			<input type="type" class="form-control" name="contact_person"  v-model="form.contact_person" required :disabled="form.disabled">
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('contact_person')"></span>
		</div>

        <div class="form-group" :class="{'has-error' : form.errors.has('contact_number') }">
			<label>Contact Number</label>
			<input type="type" class="form-control" name="contact_number"  v-model="form.contact_number" required :disabled="form.disabled">
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('contact_number')"></span>
		</div>

        <div class="form-group" :class="{'has-error' : form.errors.has('supplier_address') }">
			<label>Supplier Address</label>
			<textarea class="form-control" name="supplier_address" rows="10" v-model="form.supplier_address" :disabled="form.disabled"></textarea>
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('supplier_address')"></span>
		</div>

	</form>
	<div class="table-section">
		<dataset api="{{ route('vendors.index') }}" primary="vendor_id" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" :custom-header="headerDefs" export="{{ route('vendors.export') }}"></dataset>
	</div>
</div>

@endsection
