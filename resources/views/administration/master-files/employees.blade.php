@extends('base', ['search' => 'true'])
@section('content')
@setvar('view', 'employees')
<modal ref="modal" size="90" title="Employees">
	<dataset api="{{ route('employees.index') }}" primary="employee_id" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('employees.export') }}"></dataset>
</modal>

<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">
		<div class="row">
			<div>
				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('employee_no') }">
					<label>Employee ID:</label>
					<input type="text" class="form-control" name="employee_no" required v-model="form.employee_no" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('employee_no')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('first_name') }">
					<label>First Name:</label>
					<input type="text" class="form-control" name="first_name" required v-model="form.first_name" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('first_name')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('middle_name') }">
					<label>Middle Name:</label>
					<input type="text" class="form-control" name="middle_name" v-model="form.middle_name" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('middle_name')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('last_name') }">
					<label>Last Name:</label>
					<input type="text" class="form-control" name="last_name" v-model="form.last_name" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('last_name')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('work_phone') }">
					<label>Work Phone:</label>
					<input type="text" class="form-control" name="work_phone" v-model="form.work_phone" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('work_phone')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('email') }">
					<label>Email:</label>
					<input type="email" class="form-control" name="email" required v-model="form.email" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('email')"></span>
				</div>
			</div>
			<div>
				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('employee_status'), 'disabled': form.disabled }">
					<label>Employee Status:</label>
					<combobox :options="@arr2str($emp_status)" name="employee_status" v-model="form.employee_status"></combobox>
					</multiselect>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('employee_status')"></span>
				</div>
				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('department'), 'disabled': form.disabled }">
					<label>Department:</label>
					<combobox :options="@arr2str($departments)" name="department" v-model="form.department"></combobox>
					</multiselect>		
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('department')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('department_head') }">
					<label>Dept. Head:</label>
					<input type="text" class="form-control" name="department_head" required v-model="form.department_head" :disabled="form.disabled" @open="form.errors.clear('department_head')">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('department_head')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('supervisor') }">
					<label>Supervisor:</label>
					<input type="text" class="form-control" name="supervisor" required v-model="form.supervisor" :disabled="form.disabled" @open="form.errors.clear('supervisor')">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('supervisor')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('date_hired') }">
					<label>Date Hired:</label>
					<div class="input-group">
						<datepicker v-model="form.date_hired" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="date_hired"></datepicker> 
						<span class="input-group-addon">
	                        <i class="fa fa-fw fa-calendar"></i>
	                    </span>
                    </div>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('date_hired')"></span>
				</div>
			</div>
			<div>
				<div class="img-frame" :class="{active : !form.disabled}">
					<div class="img-wrapper">
						<img id="dp" alt="Employee Image" :src="empImage" />
						<input type="file" name="employee_image" class="hidden" @change="assignImage($event)" accept="image/*">
						<span class="uploader" v-if="!form.disabled" @click="$event.target.previousElementSibling.click()"><i class="fa fa-camera fa-fw"></i> Upload Image</span>
					</div>
				</div>
			</div>
		</div>
		<div class="tab" v-if="identifier">
		<ul>
			<li v-for="(item, key) in togglables" class="items" :class="{active : selected == key}" @click="selected = key">@{{ item }}</li>
		</ul>
		<div class="tab-content" id="accountable_assets" v-if="selected == 0 || selected == ''">
			<div>
				<dataset api="{{ route('open-records.index') }}"  :excludes="['pre_asset']" :default-search="form.first_name + ' ' + form.middle_name + ' ' + form.last_name" :async="true" :paginate="true" link-to="{{ route('open-records') }}" :info="true" ref="accountable_assets"></dataset>
				</div>
			</div>
		<div class="tab-content" id="loaned_assets" v-if="selected == 1">
			<div></div>
		</div>
	</div>
	</form>
	<div class="mass-upload" v-if="upload_mass && !identifier">
		<div class="separator">Mass Create</div>
		<form action="{{ route('bulk.employees') }}" method="POST" enctype="multipart/form-data">
			{{ csrf_field() }}
			<div>
				<a href="/templates/EMPLOYEES_TEMPLATE.xlsx" download><font size="40"><i class="fa fa-file-text-o fa-5X"></i></font>
				<span class="help-block">Click here to download the template.</span></a>
			</div>
			<div>
				<font size="40"><i class="fa fa-info-circle fa-5X"></i></font>
				<span class="help-block">Maximum no. of rows is 1,500. <br>
					Use only template downloaded, to avoid errors.</span>
			</div>
			<div>
				<div>
					<label>Employees File: </label>
					<input type="file" name="employees" @change="massUpload($event)" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
					<p class="help-block">Maximum upload file size: 2MB</p>
				</div>
					<component v-bind:is="process_result"></component>
			</div>
		</form>
	</div>
</div>

@endsection