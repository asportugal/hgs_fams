{{-- <div class="requests">
	<div class="panel-title">Requests</div>		
	<table class="table table-hover">
		<thead>
			<th>Module</th>
			<th>Requestor</th>
			<th>Approver</th>	
			<th>Request Date</th>
		</thead>
		<tbody>
			@foreach($requests as $request)
				<tr>
					<td>{{ $request->entity	}}</td>
					<td>{{ $request->requested_by }}</td>
					<td>{{ $request->approver }}</td>
					<td>{{ date('m/d/Y', strtotime($request->created_at)) }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="approvals">
	<div class="panel-title">Approvals</div>
	<table class="table table-hover">
		<thead>
			<th>Requestor</th>
			<th>Module</th>
			<th>Date</th>
		</thead>
		<tbody>
			@foreach($requests as $request)
				<tr>
					<td>{{ $request->requested_by }}</td>
					<td>{{ $request->entity	}}</td>
					<td>{{ date('m/d/Y', strtotime($request->created_at)) }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
--}}
<div class="assets">
	<div class="panel-title">Assets
		<span class="scopes-option pull-right">
			<label>Scope</label>
			<select class="form-control" id="chartScope" onchange="getScope()">
				<option value="site">Site</option>
				<option value="category">Category</option>
				<option value="status">Status</option>
			</select>
		</span>
	</div>
	<canvas id="assetChart" width="800"></canvas>
</div>
@push('script-head')
window.onload = function () {
	getScope()
}
@endpush