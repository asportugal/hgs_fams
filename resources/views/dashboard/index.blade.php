@extends('base')
@section('content')
@setvar('view', 'dashboard')
<div class="{{ $view }}" id="welcome">
@include('dashboard.administrator')
{{-- <div class="requests">
	<div class="panel-title">Requests</div>		
	<table class="table table-hover">
		<thead>
			<th>Module</th>
			<th>Requestor</th>
			<th>Approver</th>	
			<th>Request Date</th>
		</thead>
		<tbody>
			@foreach($requests as $request)
				<tr>
					<td>{{ $request->entity	}}</td>
					<td>{{ $request->requested_by }}</td>
					<td>{{ $request->approver }}</td>
					<td>{{ date('m/d/Y', strtotime($request->created_at)) }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="approvals">
	<div class="panel-title">Approvals</div>
	<table class="table table-hover">
		<thead>
			<th>Requestor</th>
			<th>Module</th>
			<th>Date</th>
		</thead>
		<tbody>
			@foreach($requests as $request)
				<tr>
					<td>{{ $request->requested_by }}</td>
					<td>{{ $request->entity	}}</td>
					<td>{{ date('m/d/Y', strtotime($request->created_at)) }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>
--}}
</div>
@endsection
@push('script-head')
	var assets;
	var js = document.createElement("script");
		js.type = "text/javascript";
		js.src = "/js/Chart.min.js";
	document.head.appendChild(js);
	document.getElementById('app').style = { visibility: 'visible' }
	document.getElementById('welcome').style.height = this.innerHeight + 'px';

	function generateChart(chart_data) {
		var charts = { label: [], data: [] };
		for(var i=0; i < chart_data.length; i++) {
			charts.label.push(toProperCase(chart_data[i].label));
			charts.data.push(chart_data[i].asset_count);
		}
		var scope = document.getElementById('chartScope').value;
		var assetCanvas = document.getElementById('assetChart').getContext('2d');
		if(assets) 	assets.destroy();
		
		Chart.defaults.global.defaultFontColor = '#000';
		assets = new Chart(assetCanvas, {
		    type: 'horizontalBar',
		    data: {
		        labels: charts.label,
		        datasets: [{
		            label: '# of assets per ' + scope,
		            data: charts.data,
		            backgroundColor: [
		            	'#00a65a',
		            	'#f39c12',
		                '#00c0ef',
		                '#dd4b39',
		                '#3c8dbc',
		            ],
		            borderWidth: 1
		        }]
		    },
		    options: {
			    legend: {
		            display: false,
		        },
		        scales: {
				    xAxes: [{
		                gridLines: {
		                    display:false
		                },
		                legend: {

		                }
		            }],
		            yAxes: [{
		                gridLines: {
		                    color: '#a2a2a2',
		                    offsetGridLines: true,
		                }, 
		            }],
			    }
			}
		});
	}

	function toProperCase(str) {
		if(str !== null) {
			return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
		} else {
			return 'None'
		}
	}

	function getScope() {
		var route = '{{ route('asset-summary') }}';
		var scope = document.getElementById('chartScope').value;
		var xhttp = new XMLHttpRequest();
		xhttp.responseType = 'json';
		xhttp.onreadystatechange = function() {
			if (xhttp.readyState == 4 && xhttp.status == 200) {
			 	generateChart(xhttp.response);
			}
		};
		xhttp.open('GET', route + '/' + scope, true);
		xhttp.send();
	}
@endpush