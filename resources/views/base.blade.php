<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>FAMS - {{ title_case(str_replace('-', ' ', Route::currentRouteName())) }}</title>
    <link rel="icon" type="image/png" href="/img/favico.png"/>
    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <!-- Scripts -->
    <!-- {{-- <script src="https://cdn.polyfill.io/v2/polyfill.js"></script> --}} -->
    <!-- {{-- <script src="https://cdn.jsdelivr.net/minifill/0.0.3/minifill.min.js"> </script> --}} -->
    <!--[if IE]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <![endif]--> 
</head>
<body>
    @include('partials.notification')
    @include('partials.header')
    @include('partials.sidebar')
    <div id="app" class="main container" style="visibility: hidden">
        @include('partials.buttons')
        @yield('content')
    </div>
    <script type="text/javascript">
        @stack('script-head')
    </script>
    <script src="/js/manifest.js"></script>
    <script src="/js/vendor.js"></script>
    <script src="/js/app.js"></script>
</body>

</html>