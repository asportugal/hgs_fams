@extends('base')
@section('content')
<div id="unpermitted">
	<div class="text-center" style="padding-top: 100px;">
	<h3>
	<i class="fa fa-fw fa-times-circle fa-lg" style="color: #ff6363"></i>
	You do not have permission to access this page.
	</h3>
	</div>
</div>
@endsection
@push('script-head')
	var content = document.getElementById('unpermitted');
	content.parentNode.style['visibility'] = 'visible';
	content.parentNode.style['height'] = window.innerHeight - content.offsetTop + 'px';
@endpush