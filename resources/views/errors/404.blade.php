<head>
	<link href="/css/app.css" rel="stylesheet">
	<style>
		body {
			background: #eee;
		}
		.not_found {
			margin-top: 0;
		    font-weight: bolder;
		    text-shadow: #686586 1px 1px 2px;
		}
		.txt_404 {
			color: #7b6969;
			display: inline-block;
			font-size: 200px;
			vertical-align: middle;
			line-height: 0.8;
			text-shadow: #000 3px 1px 0px;
		}
		.img_404 {
		    display: inline-block;
		    box-shadow: #000 1px 1px 4px 1px;
		    border: 1px solid #000;
		    border-radius: 60px;
		    margin: 20px 10px;
		    background: #7b6969;
		}
	</style>
</head>
<body background="{{ asset('/img/favico.png') }}">
	<header>
	    <div class="banner">
	        <h1>Fixed Asset Management System</h1>
	        <img src="{{ asset('/img/logo.png') }}" border="0" alt="header" />
	    </div>
	    <div class="horizontal-bar" style="height: 25px;">
		    <div class="digital-clock">
	            <span id="clock">
	            </span>
	        </div>
		    <div class="user-details">
		    	&nbsp;
		    </div>
		</div>
	</header>
	<div class="text-center" style="padding: 80px">
		<diV>
			<h1 class="txt_404">4</h1>
			<img height="160" class="img_404" src="{{ asset('/img/404.png') }}" border="0" alt="404" />
			<h1 class="txt_404">4</h1>
		</div>
		<h1 class="not_found">Page not found</h1>
		<a href="/home">return to home</a>
	</div>
	<script>
		var container = document.getElementById('clock');
		setInterval(() => {
		    var d = new Date();
		    container.innerText = d.toDateString() +' | '+ d.toLocaleTimeString();
		}, 1000)
	</script>
</body>
