<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FAMS - Login</title>
    <link href="/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <style type="text/css">
    body {
        margin-top: 100px;
        background: url('./img/background.jpg');
    }
    </style>
</head>
<body>
    <div class="login">
        <form method="POST" action="{{ url('login') }}">
            <h2>FAMS Login</h2>
            <div class="form-group {{ $errors->has('username') ? ' has-error tool-tip' : '' }}">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                    <input type="text" class="form-control" name="username" placeholder="Username">
                </div>
                @if($errors->has('username'))
                    <div class="tip">
                        <p class="text error"><i class="fa fa-warning"></i> {{ $errors->first('username') }}</p>
                    </div>
                @endif
            </div>
            <div class="form-group {{ $errors->has('password') ? ' has-error tool-tip' : '' }}">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                    <input type="password" class="form-control" name="password" placeholder="Password">                
                </div>
                  @if($errors->has('password'))
                    <div class="tip">
                        <p class="text error"><i class="fa fa-warning"></i> {{ $errors->first('password') }}</p>
                    </div>
                @endif
            </div>
            <div class="controls">
                <label class="checkbox-inline">
                  <input type="checkbox" value="1" name="remember"> Remember Me
                </label>
                <button type="submit" class="btn btn-default pull-right">Submit</button>
            </div>
            {{ csrf_field() }}
        </form>
    </div>
</body>
</html>