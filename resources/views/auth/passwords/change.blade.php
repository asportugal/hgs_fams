<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>FAMS - {{ title_case(str_replace('-', ' ', Route::currentRouteName())) }}</title>
    <link rel="icon" type="image/png" href="/img/favico.png"/>
    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
    <!-- Scripts -->
    <!-- {{-- <script src="https://cdn.polyfill.io/v2/polyfill.js"></script> --}} -->
    <!-- {{-- <script src="https://cdn.jsdelivr.net/minifill/0.0.3/minifill.min.js"> </script> --}} -->
    <!--[if IE]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <![endif]--> 
</head>
<body>
    @include('partials.notification')
    @include('partials.header')
    @include('partials.sidebar')
    <div id="app" class="main container">
    	<div id="change-password">
    		<form action="{{ route('change') }}" method="POST" style="width: 30%">
    		{{ csrf_field() }}
    			<div class="form-group">
	    			<label>Old Password:</label>
	    			<input  type="password" class="form-control" name="old_password"/>
    			</div>
    			<div class="form-group">
	    			<label>New Password:</label>
	    			<input  type="password" class="form-control" name="new_password"/>
    			</div>
    			<div class="form-group">
	    			<label>Repeat Password:</label>
	    			<input  type="password" class="form-control" name="repeat_password"/>
    			</div>
    			<div class="form-group">
    				<button class="btn btn-primary">Change Password</button>
    			</div>
    		</form>
    	</div>
    </div>
</body>
<script type="text/javascript">
	var content = document.getElementById('change-password');
	content.parentNode.style['height'] = window.innerHeight - content.offsetTop + 'px';
    @stack('script-head')
</script>
</html>