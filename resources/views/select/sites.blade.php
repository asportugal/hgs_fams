<multiselect 
	:options="{{ $sites }}" 
	:option-height="26"
	:disabled="form.disabled"
	ref="sites"
	@foreach($attribs as $k => $v)
		{{ $k }}="{{ $v }}"
	@endforeach
></multiselect>