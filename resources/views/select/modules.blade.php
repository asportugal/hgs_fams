<multiselect 
	:options="{{ $modules }}" 
	:option-height="26"
	:disabled="form.disabled"
	@foreach($attribs as $k => $v)
		{{ $k }}="{{ $v }}"
	@endforeach
></multiselect>