@extends('base')
@section('content')
@setvar('view', 'warranty_report')
<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">
		<div>
			<div class="form-group horizontal">
				<label>Filter by:</label>
				<multiselect v-model="filter_by" :options="{{ $options }}"></multiselect>
			</div>
		</div>
		<div></div>
		<div>
			<div class="form-group horizontal">
				<div class="text-right">
					<label>Chart Type:</label>
					<div class="radio-input">
						<span><input type="radio" name="chart_type" v-model="chart_type" value="PIE"> PIE</span>
						<span><input type="radio" name="chart_type" v-model="chart_type" value="BAR"> BAR</span>
					</div>
				</div>
				<p class="help-text">Right click on chart and click save image to download.</p>
			</div>
		</div>
	</form>
	<div style="margin: 0 auto;">
		<report ref="report" :data-set="{{ json_encode($report) }}" :chart-type="chart_type"></report>
	</div>
</div>
@endsection
