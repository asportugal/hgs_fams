@if(isset($buttons))
<div class="form-buttons">
	
	<div class="pull-left search" @click="$refs['modal'].show = true" v-show="form.alert == false" v-if="{{ $search or 'false' }}">
		<label>Search:</label> 
		<input type="search" name="search" class="form-control" size="30" @keyup.enter="search">
	</div>

	@if(in_array('delete', $buttons))
	<div class="delete-warn pull-left" v-if="form.alert"><span><i class="fa fa-warning"></i> 
		Are you sure want to remove the selected {{ str_replace('-', ' ', str_singular(Route::currentRouteName())) }}?</span>
		<div class="pull-right">
		<button type="submit" @click="submitForm('delete')">Yes</button>
		<button @click="form.alert = false">No</button></div>
	</div>
	@endif

	@yield('custom-tags', '')
	
	<div class="pull-right">

		@yield('custom-buttons', '')
		
		@if(in_array('bulk', $buttons))
			<button class="btn btn-sm" @click="bulk()" :disabled="bulkDisable">
				<i class="fa fa-fw fa-save blue"></i> Bulk
			</button>
		@endif

		@if(in_array('save', $buttons))
			<button class="btn btn-sm" @click="submitForm('save')" :disabled="!asset_id || form.errors.any()">
				<i v-if="componentEvent('save')" class="fa fa-spinner fa-spin green"></i>
				<i v-else class="fa fa-fw fa-pencil-square-o green"></i> Save / New
			</button>
		@endif

		@if(in_array('search', $buttons))
			<button class="btn btn-sm" disabled="form.disabled">
				<i class="fa fa-fw fa-search green"></i> Search
			</button>
		@endif

		@if(in_array('print', $buttons))
			<button class="btn btn-sm" @click="print()" :disabled="printDisabled">
				<i class="fa fa-fw fa-print blue"></i> Print
			</button>
		@endif

		@if(in_array('create', $buttons))
			<button class="btn btn-sm" @click="submitForm('create')" :disabled="identifier || form.errors.any()">
				<i v-if="componentEvent('create')" class="fa fa-spinner fa-spin blue"></i>
				<i v-else class="fa fa-fw fa-save blue"></i> Create
			</button>
		@endif

		@if(in_array('edit', $buttons))
			<button class="btn btn-sm" @click="submitForm('update')" :disabled="!identifier || form.disabled || form.errors.any()">
				<i v-if="componentEvent('update')" class="fa fa-spinner fa-spin green"></i>
				<i v-else class="fa fa-fw fa-pencil green"></i> Update
			</button>

			<button class="btn btn-sm" @click="form.disabled=false" :disabled="!identifier || form.alert || form.errors.any()">
				<i class="fa fa-fw fa-pencil-square-o green"></i> Edit 
			</button>
		@endif

		@if(in_array('delete', $buttons))
			<button class="btn btn-sm" @click="form.alert = true" :disabled="!identifier || !form.disabled || form.errors.any()">
				<i v-if="componentEvent('delete')" class="fa fa-spinner fa-spin red"></i>
				<i v-else class="fa fa-fw fa-trash red"></i> Delete 
			</button>
		@endif

		@if(in_array('generate', $buttons))
			<!--button class="btn btn-sm">
				<i class="fa fa-fw fa-table blue"></i> Generate 
			</button>-->
		@endif

		<button class="btn btn-sm" @click="cancelTransaction()">
			<i class="fa fa-fw fa-times red"></i> Cancel
		</button>
	</div>
</div>
@endif