<header>
    <div class="banner">
        <h1>Fixed Asset Management System</h1>
        <img src="{{ asset('/img/logo.png') }}" border="0" alt="header" />
    </div>
    <div class="horizontal-bar">
        <div class="digital-clock">
            <span id="clock">
            </span>
        </div>
        <div class="user-details">
            <span>Current User: {{ Auth::user()->username }}</span>
            <span>Role: {{ strtoupper(Auth::user()->role) }}</span>
        </div>
        <div class="navigation-buttons">
            <strong> {{ str_replace('-', ' ', Route::currentRouteName()) }} </strong>
            <a href="{{ route('home') }}" role="button">
                <i class="fa fa-fw fa-home"></i> Home
            </a>
            <a role="button" class="pull-right" style="width: 10%" onclick="document.getElementById('dropdown').classList.toggle('hidden')">
                <i class="fa fw fa-gear"></i>
                <ul id="dropdown" class="hidden">
                    <li>
                        <a href="{{ route('change') }}">
                            <i class="fa fw fa-change"></i> Change Password
                        </a>                        
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            <i class="fa fw fa-sign-out"></i> Logout
                        </a>
                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </a>
        </div>
    </div>
</header>
@push('script-head')
var container = document.getElementById('clock');
setInterval(() => {
    var d = new Date();
    container.innerText = d.toDateString() +' | '+ d.toLocaleTimeString();
}, 1000)
@endpush