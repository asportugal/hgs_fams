<div class="table-utils">
	<div>
		<label>Search: </label>
		<input type="search" class="form-control">
	</div>
	<div class="pull-right">
		<label>Export</label>
		<div class="btn-group btn-group-sm">
			<button class="btn btn-default">CSV</button>
			<button class="btn btn-default">EXCEL</button>
		</div>
	</div>
</div>
<table class="table table-hover">
	<thead>
		<tr>
			@foreach($columns as $col)
			@if($loop->first)
				<th width="1">#</th>
			@endif
				<th>{{ title_case(str_replace('_', ' ', $col)) }}</th>
			@endforeach
		</tr>
	</thead>
	<tbody>
		@forelse($data as $d)
			<tr @click="showData({{ $d['id'] }})">
				<td>{{ $loop->iteration }}</td>
				@tableRow(array_except($d, 'id'))
			</tr>
		@empty
			<tr>
				<td></td><td colspan="{{ count($columns) }}"><center>No records found!</center></td>
			</tr>
		@endforelse
	</tbody>
</table>
@if ($total > $per_page)
    <ul class="pagination">
        @if ($current_page == 1)
            <li class="disabled"><span>Previous</span></li>
        @else
            <li><a href="{{ $prev_page_url }}" rel="prev">Previous</a></li>
        @endif
        @setvar('i', 1)
        @while ($i <= $last_page)
        	@if($i == $current_page)
        		<li class="active"><span>{{ $i }}</span></li>
        	@else
        		<li><a href="{{ url()->current() . '?page=' . $i }}">{{ $i }}</a></li>
        	@endif
        	@setvar('i', $i+1)
        @endwhile
        @if ($current_page >= $last_page)
            <li class="disabled"><span>Next</span></li>
        @else
            <li><a href="{{ $next_page_url }}" rel="next">Next</a></li>
        @endif
    </ul>
@endif
