@if(session('status'))
    <div id="alert" class="push-notif alert alert-dismissable alert-success">
        <a class="close" data-dismiss="alert" aria-label="close" onclick="this.parentNode.remove();">&times;</a>
        {{ session()->pull('status', '') }}
    </div>
	@push('script-head')
		setTimeout(() => { 
		var alert = document.getElementById('alert');
		alert.style.opacity = '0';
		alert.remove();
		}, 3000);
	@endpush
@elseif(isset($errors) && count($errors) > 0)
	<div id="alert" class="push-notif alert alert-dismissable alert-danger">
        <a class="close" data-dismiss="alert" aria-label="close" onclick="this.parentNode.remove();">&times;</a>
        <ul class="errors">
        @foreach($errors->all() as $error)
        	<li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif