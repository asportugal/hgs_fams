<div class="sidebar">
	<h3>APPLICATION MENU</h3>
	<ul id="menu">
		@setvar('ctr', 0)
		@while($ctr < count($sidebar['items']))
			@setvar('currSub', '')
			@setvar('menu_arr', $sidebar['items'][$ctr])
			@foreach($menu_arr as $menu)
				@if ($loop->first)
			        <li class="menu-item togglable"><a class="{{ $menu['active'] }}"> {{ strtoupper($menu['parent']) }} </a>
			        <ul class="{{ ($menu['parent'] == $sidebar['active']['parent']) ? 'show' : 'hidden' }}">
			    @endif
			    @if ($menu['sub'] != '' && $currSub != $menu['sub'])
			    	@if($currSub != '')
			    		</ul></li>
			    	@endif
			    	<li class="sub-menu-item togglable"><a class="{{ $menu['active'] }}"> {{ title_case($menu['sub']) }} </a>
			    	<ul class="{{ ($menu['sub'] == $sidebar['active']['sub']) ? 'show' : 'hidden' }}"></li>
			    	@setvar('currSub', $menu['sub'])
			    @endif
				<li class="item"><a href="{{ $menu['link'] }}" class="{{ $menu['active'] }}"> {{ $menu['name'] }} </a></li>
			    @if ($loop->last)
			    	</ul></li>
			    	@setvar('ctr', $ctr + 1)
			    @endif
			@endforeach
		@endwhile
	</ul>
</div>
@push('script-head')
var menu = document.getElementById('menu');
var list = menu.getElementsByClassName('togglable');
for(var $i=0; $i<list.length; $i++) {
	list[$i].firstChild.addEventListener('click', (event) => {
		var childList = event.target.nextElementSibling; 
		event.target.classList.toggle('active');
		if(childList.className == 'show') {
			childList.classList.remove('show');
			childList.className = 'hidden';
		} else if (childList.className == 'hidden') {
			childList.classList.remove('hidden');
			childList.className = 'show';
		}
	});
}
@endpush