@if(session('status'))
	    <div id="alert" class="alert  alert-dismissable alert-success">
			<a class="close" data-dismiss="alert" aria-label="close" onclick="this.parentNode.remove();">&times;</a>
			{{ session()->pull('status', '') }}
		</div>
@endif

@if(count($errors))
	<div class="alert alert-dismissable alert-danger">
		<a class="close" data-dismiss="alert" aria-label="close" onclick="this.parentNode.remove();">&times;</a>
		<ul class="alert-list">
			@foreach ($errors->all() as $error)
            	<li>{{ $error }}</li>
        	@endforeach
		</ul>
	</div>
@endif