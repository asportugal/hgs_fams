<div class="tab">
	<ul>
		@foreach($items as $item)
			<li>{{ $item }}</li>
		@endforeach
	</ul>
	<div class="tab-content">
		@if(isset($pre_assets))
			<table class="table table-hover">
				@foreach($pre_assets as $pre_asset)
					@if($loop->first)
						<tr>
							<thead>
								<td> {{ $pre_asset }}</td>
							</thead>
						</tr>
					@endif
				@endforeach
			</table>
		@endif
	</div>
</div>