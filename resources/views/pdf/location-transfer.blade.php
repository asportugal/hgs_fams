<div>
<table style="width: 100%; font-family: Arial, sans-serif; font-size: 12px;">
<tr>
	<td width="2%" style="padding: 5px 0;"><img style="vertical-align: middle;  text-align: left;" src="img/logo.png" width="150px"/></td>
	<td width="4%" style="padding: 5px 0;"><h1 style="vertical-align: middle; text-align: center; margin: 0;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gate Pass</h1></td>
	<td width="4%" style="padding: 5px 0;"><h3 style="vertical-align: middle;  text-align: right; margin: 0;">{{ date('l, F d, Y') }}</h3></td>
</tr>
<tr>
	<td width="10%" colspan="3" style="padding: 5px 0;"><hr /></td>
</tr>
<tr>
	<td width="2%" style="padding: 5px 0;">Control No:</td>
	<td colspan="2" width="8%" style="padding: 5px 0;">{{ $data->control_no }}</td>
</tr>
<tr>
	<td width="2%" style="padding: 5px 0;">Asset ID:</td>
	<td colspan="2" width="8%" style="padding: 5px 0;">{{ $data->asset_id }}</td>
</tr>
<tr>
	<td width="2%" style="padding: 5px 0;">Brand:</td>
	<td colspan="2" width="8%" style="padding: 5px 0;">{{ $data->asset->brand }}</td>
</tr>
<tr>
	<td width="2%" style="padding: 5px 0;">Transfer Date: </td>
	<td colspan="2" width="8%" style="padding: 5px 0;"> {{ date('m/d/Y') }} </td>
</tr>
<tr>
	<td width="2%" style="padding: 5px 0;">Transfer By: </td>
	<td colspan="2" width="8%" style="padding: 5px 0;">{{ $data->requested_by }}</td>
</tr>
<tr>
	<td width="10%" colspan="3" style="padding: 5px 0;"><hr /></td>
</tr>
<tr>
	<td width="10%" colspan="3" style="padding: 5px 0;"><h3 style="margin: 0;">Asset</h3></td>
</tr>
</table>
<table style="width: 100%; font-family: Arial, sans-serif; border-collapse: collapse; font-size: 11px;">
<tr>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center; font-weight: bold;">Description</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center; font-weight: bold;">Serial No</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center; font-weight: bold;">From Site</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center; font-weight: bold;">From Floor</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center; font-weight: bold;">From Cube</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center; font-weight: bold;">To Site</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center; font-weight: bold;">To Floor</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center; font-weight: bold;">To Cube</td>
</tr>
<tr>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center;">{{ $data->asset->description }}</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center;">{{ $data->asset->serial_no }}</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center;">{{ $data->from_site }}</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center;">{{ $data->from_floor or 'N/A' }}</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center;">{{ $data->from_cube or 'N/A' }}</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center;">{{ $data->to_site }}</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center;">{{ $data->to_floor or 'N/A' }}</td>
	<td style="padding: 5px 0; border: 1px solid black; text-align: center;">{{ $data->to_cube or 'N/A' }}</td>
</tr>
<tr>
	<td colspan="8"><br><br></td>
</tr>
<tr>
	<td colspan="8" style="text-align: right; font-size: 12px;">Approved By: {{ $data->approved_by }} / {{ $data->approved_date }}</td>
</tr>
</table>
</div>