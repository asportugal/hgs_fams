@extends('base', ['search' => 'true'])
@section('content')
@setvar('view', 'asset-disposal')

<modal ref="modal" size="90" title="Asset Disposal">
	<dataset api="{{ route('asset-disposal.index') }}" :narrow-search="true" primary="disposal_id" :excludes="['delivery_reference']" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('asset-disposal.export') }}"></dataset>
</modal>

<div :class="component">
	<div class="asset-info">
		<h4>Asset Information</h4>
		<div class="img-frame">
			<div class="img-wrapper">
				<img id="dp" alt="Asset Image" :src="image_src" onload="window.URL.revokeObjectURL(this.src);" />
			</div>
		</div>
		<dl class="dl-horizontal" v-for="(val, key) in asset_info">
		  	<dt class="text-capitalize" v-text="key.replace(/_/g, ' ')"></dt>
			<dd v-if="val">@{{ val }}</dd><dd v-else> NONE </dd>
		</dl>
	</div>
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)" ref="{{ $view }}">
		<div v-if="status == 'PENDING' && isApprover('{{ Auth::user()->username }}')" class="text-center" style="width: 100%; padding: 5px 0px 20px;">
			<div class="btn-group-horizontal" role="group">
				<a class="btn btn-primary btn-md"  @click.prevent="actionRequest('APPROVED')" >
				<i v-if="actionLoader == 'APPROVED'" class="fa fa-spinner fa-spin"></i> <i v-else class="fa fa-fw fa-check"></i> Approve</a>
				<a class="btn btn-danger btn-md"   @click.prevent="actionRequest('DECLINED')" >
				<i v-if="actionLoader == 'DECLINED'" class="fa fa-spinner fa-spin"></i>
				<i v-else class="fa fa-fw fa-times"></i> Decline</a>
			</div>
		</div>
		<div>
			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('asset_id') }">
				<label>Asset ID:</label>
				<input type="text" class="form-control" name="asset_id" v-model="form.asset_id" :disabled="{{ Auth::user()->is_admin() }}">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('asset_id')"></span>
			</div>
			<div class="form-group horizontal">
					<dl class="dl-horizontal static">
					  	<dt>Approved By:</dt><dd v-if="approved_by">@{{ approved_by }}</dd><dd v-else> NONE </dd>
					</dl>
					<dl class="dl-horizontal static" style="margin-bottom:0">
					  	<dt>Approved Date:</dt><dd v-if="approved_date">@{{ approved_date }}</dd><dd v-else> NONE </dd>
					</dl>
				</div>
			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('disposal_status') }">
				<label>Disposal Status:</label>
				<input type="text" class="form-control" name="disposal_status" v-model="form.disposal_status" :disabled="true">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('disposal_status')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('itaf_no') }">
				<label>ITAF No:</label>
				<input type="text" class="form-control" name="itaf_no" v-model="form.itaf_no" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('itaf_no')"></span>
			</div>

			<div class="form-group horizontal" :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('disposal_method') }">
				<label>Disposal Method:</label>
				<multiselect v-model="form.disposal_method" :options="@arr2str($disposal_methods)" placeholder="" name="disposal_method" :show-labels="false" :searchable="true" @input="form.errors.clear('disposal_method')"></multiselect>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('disposal_method')"></span>
			</div>

			<div class="form-group horizontal"  :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('approver') }">
				<label>Approver:</label>
				<multiselect :options="{{ $loan_approver }}" :searchable="false" :show-labels="false" name="approver" placeholder="" v-model="approvers" @open="form.errors.clear('approver')" label="group_name" track-by="approver" ref="approvers"></multiselect>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('approver')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('disposal_vendor') }">
				<label>Disposal Vendor:</label>
				<input type="text" class="form-control" name="disposal_vendor" v-model="form.disposal_vendor" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('disposal_vendor')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('disposal_remarks') }">
				<label>Remarks:</label>
				<textarea style="height: 80px;" class="form-control" name="disposal_remarks" v-model="form.disposal_remarks" :disabled="form.disabled"></textarea>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('disposal_remarks')"></span>
			</div>
		</div>
	</form>
</div>
@endsection