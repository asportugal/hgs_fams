@extends('base')
@section('content')
@setvar('view', 'bulk-upload')

<div :class="component">
<form ref="{{ $view }}" action="{{ route('bulk.assets') }}" method="POST" enctype="multipart/form-data" @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">
		<div class="form-group">
			<label>Type of Upload</label>
			<a :href="'/templates/' + template" :download="template" class="pull-right" v-if="upload_type">
			Click to download @{{ upload_type.name }} template. 
			</a> 
			<multiselect :options="{{ collect([['type' => 'create', 'name' => 'Asset (new)'], ['type' => 'update', 'name' => 'Asset (update)']]) }}" :option-height="26" label="name" track-by="name" :searchable="false" :disabled="form.disabled" placeholder = "" v-model="upload_type"></multiselect>
			<input type="hidden" name="type" v-bind:value="getUploadType()">
		</div>

		<div class="form-group uploader" :class="{'has-error' : form.errors.has('file') }">
			<label>Choose File</label>
			<div>
				<input type="text" v-bind:value="file.name" class="form-control" disabled>
				<input type="file" name="assets" class="hidden" @change="parseExcel()" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
				<button class="btn btn-sm" type="button" @click.prevent="$event.target.previousElementSibling.click()">Browse</button>
			</div>
			<span :is="form.errors.message" v-bind:error-message="form.errors.get('file')"></span>
			<p class="help-block" style="color: #737373 !important">
			<small>
			    Maximum row limit accepted is 1000.
			</small></p>
		</div>

		<div class="form-group">
			<label>Comment</label>
			<textarea class="form-control" rows="8" name="comment" v-model="form.comment"></textarea>
		</div>
		{{ csrf_field() }}
	</form>
	<div class="excel-table">
		<p v-if="loader"><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span>Loading File Content</span></p>
		<table class="table" ref="exceltable">
		</table>
	</div>
</div>
@endsection