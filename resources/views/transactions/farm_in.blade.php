@extends('base', ['search' => 'true'])
@section('content')
@setvar('view', 'farm-in')

<modal ref="modal" size="90" title="PEZA Farm in (8105)">
<dataset api="{{ route('farm-in.index') }}" :hide-primary="true" primary="farm_in_id" :excludes="['line_items']" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('farm-in.export') }}" :narrow-search="true"></dataset>
</modal>


<div :class="component">	
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)" ref="{{ $view }}">
	<div class="row">	
		<div>
			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('purchase_order') }">
				<label>Purchase Order:</label>
				<input type="text" class="form-control" name="purchase_order" v-model="form.purchase_order" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('purchase_order')"></span>
			</div>

			<div class="form-group horizontal" :class="{'disabled' : form.disabled ,'has-error' : form.errors.has('location') }">
				<label>Location:</label>
				<combobox :options="@arr2str($sites)" name="location" :searchable="true" v-model="form.location"></combobox>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('location')"></span>
			</div>

			<div class="form-group horizontal" :class="{'disabled' : form.disabled ,'has-error' : form.errors.has('vendor') }">
				<label>Vendor:</label>
				<combobox :options="@arr2str($vendors)" name="vendor" :searchable="true" v-model="form.vendor"></combobox>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('vendor')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('proforma_invoice_no') }">
				<label>Proforma Invoice No:</label>
				<input type="text" class="form-control" name="proforma_invoice_no" v-model="form.proforma_invoice_no" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('proforma_invoice_no')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('proforma_invoice_date') }">
				    <label>Proforma Invoice Date:</label>
				    <div class="input-group">
					    <datepicker v-model="form.proforma_invoice_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="proforma_invoice_date"></datepicker> 
					    <span class="input-group-addon">
	                        <i class="fa fa-fw fa-calendar"></i>
	                    </span>
                    </div>
                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('proforma_invoice_date')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('final_invoice_no') }">
				<label>Final Invoice No:</label>
				<input type="text" class="form-control" name="final_invoice_no" v-model="form.final_invoice_no" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('final_invoice_no')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('final_invoice_date') }">
				    <label>Final Invoice Date:</label>
				    <div class="input-group">
					    <datepicker v-model="form.final_invoice_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="final_invoice_date"></datepicker> 
					    <span class="input-group-addon">
	                        <i class="fa fa-fw fa-calendar"></i>
	                    </span>
                    </div>
                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('final_invoice_date')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('total_amount') }">
				<label>Total Amount:</label>
				<div class="input-group">
					<span class="input-group-addon" style="cursor: pointer; font-weight: bolder;" title="Click to change currency" @click="form.currency = 'USD'" v-if="form.currency == 'PHP'">PHP</span>
					<span class="input-group-addon" style="cursor: pointer; font-weight: bolder;" title="Click to change currency" @click="form.currency = 'PHP'" v-else>USD</span>
					<input type="text" class="form-control" name="total_amount" v-model="form.total_amount" :disabled="form.disabled">
					<span class="input-group-addon">.00</span>
				</div>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('total_amount')"></span>
			</div>

			<div class="form-group horizontal">
				<label>Line Items:</label>
				<div class="inline-fields pull-right" style="width: 70%;" v-for="item in line_items" :class="{'has-error' : form.errors.has('description.'+(item-1)) || form.errors.has('quantity.'+(item-1)) }">
					<div class="inner-field" style="width: 75%;">
						<input type="text" :name="'description.'+(item-1)" v-model="form.description[item-1]" placeholder="Description" class="form-control" :disabled="form.disabled || line_items_status[item-1] == 1">
					</div>
					<div class="inner-field" style="width: 15%;">
						<input type="text" :name="'quantity.'+(item-1)" v-model="form.quantity[item-1]" placeholder="Qty" class="form-control" :disabled="form.disabled || line_items_status[item-1] == 1">
					</div>
					<button class="btn btn-primary" v-if="item == 1 && !form.disabled" @click="addLineItem()"><i class="fa fa-fw fa-plus"></i></button>
					<button class="btn btn-danger" v-if="item > 1 && (!form.disabled && (line_items_status[item-1] == 0 || !line_items_status[item-1])) " @click="removeLineItem(item-1)"><i class="fa fa-fw fa-minus"></i></button>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('description.'+(item-1))"></span>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('quantity.'+(item-1))"></span>
				</div>
			</div>
		</div>
		<div>
			<div class="form-group horizontal" :class="{'disabled' : form.disabled, 'has-error' : form.errors.has('peza_permit_status') }">
				<label>Peza Permit Status:</label>
				<combobox :options="@arr2str(['YES', 'NO', 'Not Applicable'])" name="peza_permit_status" v-model="form.peza_permit_status"></combobox>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('peza_permit_status')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('peza_form_no') }">
				<label>Peza Form No:</label>
				<input type="text" class="form-control" name="peza_form_no" v-model="form.peza_form_no" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('peza_form_no')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('peza_approval_no') }">
				<label>Peza Approval No:</label>
				<input type="text" class="form-control" name="peza_approval_no" v-model="form.peza_approval_no" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('peza_approval_no')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('request_date') }">
			    <label>Request Date:</label>
			    <div class="input-group">
				    <datepicker v-model="form.request_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="request_date"></datepicker> 
				    <span class="input-group-addon">
                        <i class="fa fa-fw fa-calendar"></i>
                    </span>
                </div>
                <span :is="form.errors.message" v-bind:error-message="form.errors.get('request_date')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('date_filed') }">
			    <label>Date Filed:</label>
			    <div class="input-group">
				    <datepicker v-model="form.date_filed" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="date_filed"></datepicker> 
				    <span class="input-group-addon">
                        <i class="fa fa-fw fa-calendar"></i>
                    </span>
                </div>
                <span :is="form.errors.message" v-bind:error-message="form.errors.get('date_filed')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('remarks') }">
				<label>Remarks:</label>
				<textarea style="height: 65px;" class="form-control" name="remarks" v-model="form.remarks" :disabled="form.disabled"></textarea>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('remarks')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('farm_in_attachments') }">
			    <label>Attachments:</label>
			    <input type="file" name="farm_in_attachments" class="hidden" @change="form.errors.clear('farm_in_attachments');parseFiles('farm_in_attachments')">
			    <div class="input-group">
			    	<input type="text" class="form-control pull-left" style="width: 85%" disabled="disabled" ref="attachment-input">
			    	<button type="button" class="btn btn-primary attachment-btn pull-left" @click.prevent="$event.target.parentElement.previousElementSibling.click()" :disabled="form.disabled">
			    	Browse
			   		</button>
			    </div>
			    <span :is="form.errors.message" v-bind:error-message="form.errors.get('farm_in_attachments')"></span>
			    <small class="help-block" v-if="!form.errors.has('farm_in_attachments')"><em class="pull-right">(Maximum size of 3MB per file)</em></small>
			</div>
			<div class="attachments">
				<ol>
					<li v-for="(file, i) in form.farm_in_attachments">
						<i class="fa fa-fw" :class="mimeType(file.type)" :title="file.type"></i>
						<a :href="file.uri" class="attachment-item" :download="file.name" :title="file.name"> @{{ file.name }} </a>
						<a class="delete-file" v-if="!form.disabled" @click.prevent="form.farm_in_attachments.splice(i, 1)">[delete]</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	</form>
</div>

@endsection