@extends('base', ['search' => 'true'])
@section('content')
@setvar('view', 'open-records')
<modal ref="modal" size="90" title="Assets">
	<dataset api="{{ route('open-records.index') }}" :narrow-search="true" :hide-primary="false" primary="asset_id" 
	:utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :excludes="['active', 'vat_paid', 'maintenance_vendor', 
	'maintenance_start_date', 'maintenance_end_date', 'maintenance_remark', 'maintenance_email_notify', 
	'contract_type', 'contract_start_date', 'contract_end_date', 'contract_remarks', 'financial_treatment', 'local_cost_center',
	'cost_center', 'asset_life', 'monthly_depreciation', 'loa_no', 'loa_valid_until_date', 'consumed_asset_life', 'netbook_value', 
	'remaining_asset_life', 'date_fully_depreciated', 'purchase_price_dollar', 'purchase_price_php', 'pre_asset', 'farm_in']" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('open-records.export') }}"></dataset>
</modal>

<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">
		<div class="row">
			<div>
				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('description') }">
					<label>Description:</label>
					<input type="text" class="form-control" name="description" v-model="form.description" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('description')"></span>
				</div>

				<div class="form-group horizontal" :class="{'disabled':  true, 'has-error' : form.errors.has('category') }">
					<label>Category:</label>
					<multiselect v-model="categories" placeholder="" name="category" :show-labels="false" :searchable="true" label="name" track-by="name" :searchable="true" :options="{{ $categories }}" @select="form.subcategory = ''" ref="categories"></multiselect>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('category')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('subcategory'), 'disabled' : (categories ? false : true) || form.disabled }">
					<label>Subcategory:</label>
					<multiselect v-model="form.subcategory" placeholder="" name="subcategory" :show-labels="false" :searchable="false" :options="subcategories"></multiselect>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('subcategory')"></span>
				</div>

				<div class="form-group horizontal" :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('lob_owner') }">
					<label>LOB Owner:</label>
					<multiselect v-model="form.lob_owner" :options="@arr2str($lobs)" placeholder="" name="lob_owner" :show-labels="false" :searchable="true" @input="form.errors.clear('lob_owner')"></multiselect>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('lob_owner')"></span>
				</div>

				<div class="form-group horizontal" :class="{'disabled':  true, 'has-error' : form.errors.has('bldg') }">
					<label>Site:</label>
					<multiselect v-model="form.bldg" placeholder="" name="bldg" :show-labels="false" :searchable="true" :searchable="true" :options="@arr2str($sites)" ref="sites"></multiselect>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('bldg')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('floor') }">
					<label>Floor:</label>
					<input type="text" class="form-control" name="floor" v-model="form.floor" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('floor')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('cube') }">
					<label>Area / Workstation:</label>
					<input type="text" class="form-control" name="cube" v-model="form.cube" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('cube')"></span>
				</div>

				<div class="form-group horizontal" :class="{'disabled': form.disabled, 'has-error' : form.errors.has('employee') }">
					<label>Employee:</label>
					<multiselect v-if="!form.disabled" v-model="form.employee" id="ajax" placeholder="" :show-labels="false" name="employee" :options="employees" :searchable="true" :loading="isLoading" :internal-search="false" @search-change="asyncFindEmployee" :disabled="form.disabled">
					<span slot="noResult">No records found.</span></multiselect>
					<p v-else class="form-control" style="border: 0; box-shadow: none;"><a :href="link_to_employee('{{ route('employees') }}')">@{{ form.employee }}</a></p>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('employee')"></span>
				</div>

				<div class="form-group horizontal" :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('supplier') }">
					<label>Vendor Name:</label>
					<multiselect :options="@arr2str($vendors)" v-model="form.supplier" placeholder="" name="supplier" :show-labels="false" :searchable="true" @input="form.errors.clear('supplier')"></multiselect>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('supplier')"></span>
				</div>

				<div class="form-group horizontal" :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('brand') }">
					<label>Manufacturer:</label>
					<multiselect v-model="form.brand" :options="@arr2str($brands)" placeholder="" name="brand" :show-labels="false" :searchable="true" @input="form.errors.clear('brand')"></multiselect>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('brand')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('model') }">
					<label>Model:</label>
					<input type="text" class="form-control" name="model" v-model="form.model" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('model')"></span>
				</div>
			</div>

			<div>
				<div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('asset_id') }">
						<label>Asset ID:</label>
						<input type="text" class="form-control" name="asset_id" v-model="form.asset_id" :disabled="true">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('asset_id')"></span>
					</div>

					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('amr_no') }">
						<label>AMR ID:</label>
						<input type="text" class="form-control" name="amr_no" v-model="form.amr_no" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('amr_no')"></span>
					</div>

					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('serial_no') }">
						<label>Serial No:</label>
						<input type="text" class="form-control" name="serial_no" v-model="form.serial_no" :disabled="form.disabled || serialDisabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('serial_no')"></span>
					</div>
					<div class="attachments" v-if="delivery_attachments.length > 0">
						<label>Delivery Attachments</label>
						<ol>
							<li v-for="(file, i) in delivery_attachments">
								<i class="fa fa-fw" :class="mimeType(file.type)" :title="file.type"></i>
								<a :href="file.uri" class="attachment-item" :download="file.name" :title="file.name"> @{{ file.name }} </a>
							</li>
						</ol>
					</div>
				</div>
				<div>
					<div class="btn-group-vertical" role="group">
						<a class="btn btn-default btn-md" :class="{ 'disabled' : !identifier || form.status != 'In Storage' }" :disabled="!identifier" :href="transaction_url('{{ route('location-transfer') }}')">Transfer</a>
						<a class="btn btn-default btn-md" :class="{ 'disabled' : !identifier || form.status != 'In Storage' }" :disabled="!identifier" :href="transaction_url('{{ route('asset-replacement') }}')">Replace</a>
						<a class="btn btn-default btn-md" :class="{ 'disabled' : !identifier || form.status == 'Disposed' }" :href="transaction_url('{{ route('asset-disposal') }}')">Dispose</a>
						<a class="btn btn-default btn-md" :class="{ 'disabled' : !identifier || form.status != 'In Storage' }" :disabled="!identifier" :href="transaction_url('{{ route('loan-assets') }}')">Loan</a>
					</div>
					<div class="img-frame" :class="{active : true}">
						<div class="img-wrapper">
							<img id="dp" alt="Asset Image" :src="image_src" onload="window.URL.revokeObjectURL(this.src);" />
							<input type="file" name="asset_image" class="hidden" @change="assignImage($event)" accept="image/*">
							<span class="uploader" v-if="!form.disabled" @click="$event.target.previousElementSibling.click()"><i class="fa fa-camera fa-fw"></i> Upload Image</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab">
			<ul>
				<li v-for="(item, key) in togglables" class="items" :class="{active : selected == key, 'hidden': key == 2}" @click="selected = key">@{{ item }}</li>
			</ul>
			<div class="tab-content" id="general" v-if="selected == 0 || selected == ''">
				<div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('warranty_start_date') }">
					    <label>Warranty Start Date:</label>
					    <div class="input-group">
						    <datepicker v-model="form.warranty_start_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="warranty_start_date"></datepicker> 
						    <span class="input-group-addon">
		                        <i class="fa fa-fw fa-calendar"></i>
		                    </span>
	                    </div>
	                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('warranty_start_date')"></span>
					</div>
					<div class="form-group horizontal"  :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('financial_treatment') }">
						<label>Status:</label>
						<multiselect :options="@arr2str($status)" :searchable="false" :show-labels="false" name="status" placeholder = "" v-model="form.status"></multiselect>
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('status')"></span>
					</div>
				</div>
				<div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('warranty_end_date') }">
					    <label>Warranty End Date:</label>
					    <div class="input-group">
						    <datepicker v-model="form.warranty_end_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="warranty_end_date"></datepicker> 
						    <span class="input-group-addon">
		                        <i class="fa fa-fw fa-calendar"></i>
		                    </span>
	                    </div>
	                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('warranty_end_date')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('notes') }">
						<label>Notes:</label>
						<textarea style="height: 65px;" class="form-control" name="notes" v-model="form.notes" :disabled="form.disabled"></textarea>
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('notes')"></span>
					</div>
				</div>
			</div>
			<div class="tab-content" id="financial" v-if="selected == 1">
				<div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('delivery_receipt_date') }">
					    <label>Delivery Date:</label>
					    <div class="input-group">
						    <datepicker v-model="form.delivery_receipt_date" :disabled="true" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="delivery_receipt_date"></datepicker> 
						    <span class="input-group-addon">
		                        <i class="fa fa-fw fa-calendar"></i>
		                    </span>
	                    </div>
	                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('delivery_receipt_date')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('delivery_receipt_no') }">
						<label>Delivery Receipt No:</label>
						<input type="text" class="form-control" name="delivery_receipt_no" v-model="form.delivery_receipt_no" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('delivery_receipt_no')"></span>
					</div>	
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('purchase_order_date') }">
						<label>Purchase Date:</label>
						<div class="input-group">
						<datepicker v-model="form.purchase_order_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="purchase_order_date"></datepicker> 
						<span class="input-group-addon">
		                        <i class="fa fa-fw fa-calendar"></i>
		                    </span>
	                    </div>
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('purchase_order_no')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('purchase_order_no') }">
						<label>Purchase Order No.:</label>
						<input type="text" class="form-control" name="purchase_order_no" v-model="form.purchase_order_no" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('purchase_order_no')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('sales_invoice_no') }">
						<label>Sales Invoice No.:</label>
						<input type="text" class="form-control" name="sales_invoice_no" v-model="form.sales_invoice_no" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('sales_invoice_no')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('service_date') }">
						<label>Service Date:</label>
						<div class="input-group">
						<datepicker v-model="form.service_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="service_date"></datepicker> 
						<span class="input-group-addon">
		                        <i class="fa fa-fw fa-calendar"></i>
		                    </span>
	                    </div>
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('service_date')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('purchase_price_dollar') }">
						<label>Purchase Price (USD):</label>
						<input type="text" class="form-control" name="purchase_price_dollar" v-model="form.purchase_price_dollar" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('purchase_price_dollar')"></span>
					</div>	
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('purchase_price_php') }">
						<label>Purchase Price (PHP):</label>
						<input type="text" class="form-control" name="purchase_price_dollar" v-model="form.purchase_price_php" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('purchase_price_php')"></span>
					</div>
				</div>
				<div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('tag_date') }">
					    <label>Tag Date:</label>
					    <div class="input-group">
						    <datepicker v-model="form.tag_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="tag_date"></datepicker> 
						    <span class="input-group-addon">
		                        <i class="fa fa-fw fa-calendar"></i>
		                    </span>
	                    </div>
	                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('tag_date')"></span>
					</div>
					<div class="form-group horizontal"  :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('financial_treatment') }">
						<label>Financial Treatment:</label>
						<multiselect id="financial_treatment" :options="{{ json_encode(['CAPITALIZED', 'EXPENSED']) }}" 
						:searchable="false" :show-labels="false" name="financial_treatment" placeholder = "" v-model="form.financial_treatment"></multiselect>
							<span :is="form.errors.message" v-bind:error-message="form.errors.get('financial_treatment')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('local_cost_center') }">
						<label>Local Cost Center:</label>
						<input type="text" class="form-control" name="local_cost_center" v-model="form.local_cost_center" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('local_cost_center')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('date_fully_depreciated') }">
					    <label>Date Fully Depreciated:</label>
					    <div class="input-group">
						    <datepicker v-model="form.date_fully_depreciated" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="date_fully_depreciated"></datepicker> 
						    <span class="input-group-addon">
		                        <i class="fa fa-fw fa-calendar"></i>
		                    </span>
	                    </div>
	                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('date_fully_depreciated')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('asset_life') }">
						<label>Asset Life (months):</label>
						<input type="text" class="form-control" name="asset_life" v-model="form.asset_life" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('asset_life')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('remaining_asset_life') }">
						<label>Rmng Asset Life (months):</label>
						<input type="text" class="form-control" name="remaining_asset_life" v-model="form.remaining_asset_life" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('remaining_asset_life')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('monthly_depreciation') }">
						<label>Monthly Depreciation:</label>
						<input type="text" class="form-control" name="monthly_depreciation" v-model="form.monthly_depreciation" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('monthly_depreciation')"></span>
					</div>
				</div>
			</div>
			<div class="tab-content" id="peza" v-if="selected == 2">
				<div class="row">
				</div>
			</div>
			<div class="tab-content" id="maintenance_agreement" v-if="selected == 3">
				<div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('maintenance_vendor') }">
						<label>Maintenance Vendor:</label>
						<input type="text" class="form-control" name="maintenance_vendor" v-model="form.maintenance_vendor" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('maintenance_vendor')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('maintenance_email_notify') }">
						<label>E-mail address to Notify:</label>
						<textarea style="height: 65px;" class="form-control" name="maintenance_email_notify" v-model="form.maintenance_email_notify" :disabled="form.disabled"></textarea>
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('maintenance_email_notify')"></span>
					</div>	
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('maintenance_remark') }">
						<label>Remarks:</label>
						<textarea style="height: 65px;" class="form-control" name="remarks" v-model="form.maintenance_remark" :disabled="form.disabled"></textarea>
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('maintenance_remark')"></span>
					</div>
				</div>
				<div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('maintenance_start_date') }">
					    <label>Maintenance Start Date:</label>
					    <div class="input-group">
						    <datepicker v-model="form.maintenance_start_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="maintenance_start_date"></datepicker> 
						    <span class="input-group-addon">
		                        <i class="fa fa-fw fa-calendar"></i>
		                    </span>
	                    </div>
	                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('maintenance_start_date')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('maintenance_end_date') }">
					    <label>Maintenance End Date:</label>
					    <div class="input-group">
						    <datepicker v-model="form.maintenance_end_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="maintenance_end_date"></datepicker> 
						    <span class="input-group-addon">
		                        <i class="fa fa-fw fa-calendar"></i>
		                    </span>
	                    </div>
	                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('maintenance_end_date')"></span>
					</div>
				</div>
			</div>
			<div class="tab-content" id="contract_archive" v-if="selected == 4">
				<div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('contract_type') }">
						<label>Contract Type:</label>
						<input type="text" class="form-control" name="contract_type" v-model="form.contract_type" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('contract_type')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('contract_remarks') }">
						<label>Remarks:</label>
						<textarea style="height: 65px;" class="form-control" name="contract_remarks" v-model="form.contract_remarks" :disabled="form.disabled"></textarea>
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('remarks')"></span>
					</div>
				</div>
				<div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('contract_start_date') }">
					    <label>Contract Start Date:</label>
					    <div class="input-group">
						    <datepicker v-model="form.contract_start_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="contract_start_date"></datepicker> 
						    <span class="input-group-addon">
		                        <i class="fa fa-fw fa-calendar"></i>
		                    </span>
	                    </div>
	                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('contract_start_date')"></span>
					</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('contract_end_date') }">
					    <label>Contract End Date:</label>
					    <div class="input-group">
						    <datepicker v-model="form.contract_end_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="contract_end_date"></datepicker> 
						    <span class="input-group-addon">
		                        <i class="fa fa-fw fa-calendar"></i>
		                    </span>
	                    </div>
	                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('contract_end_date')"></span>
					</div>
				</div>
			</div>
			<div class="tab-content" id="history" v-if="selected == 5">
				<ul>
					<li v-for="item in history">
						<span class="form-control-static" v-if="item.action == 'CREATED'">
							<strong> @{{ item.action }} </strong> with @{{ item.column }} <strong> @{{ item.new_val }} </strong> on @{{ item.responsible_date }} by <strong> @{{ item.responsible_user }} </strong>
						</span>
						<span class="form-control-static" v-else>
							<strong> @{{ item.action }} </strong> @{{ item.column }} from <strong> @{{ notEmpty(item.old_val) }} </strong> to <strong> @{{ notEmpty(item.new_val) }} </strong> on @{{ item.responsible_date }} by <strong>@{{ item.responsible_user }}</strong>
						</span>
					</li>
				</ul>
			</div>
		</div>
	</form>
</div>
@endsection
