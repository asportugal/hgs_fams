@extends('base', ['search' => 'true'])
@section('content')
@setvar('view', 'pre-asset')
<modal ref="modal" size="90" title="Pre-Assets">
	<dataset api="{{ route('pre-asset.index') }}" :narrow-search="true" primary="delivery_id" :excludes="['tagged_qty', 'tag_date', 'farm_in']" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('pre-asset.export') }}"></dataset>
</modal>
<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">
		<div class="row">
			<div>
				<div class="form-group horizontal" :class="{ 'disabled':  form.disabled, 'has-error' : form.errors.has('notification') }">
				    <label>Email Notification:</label>
				    <combobox :options="@arr2str($notifications)" name="notification" :searchable="true" v-model="form.notification"></combobox>
			    	<span :is="form.errors.message" v-bind:error-message="form.errors.get('notification')"></span>
				</div>
				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('airway_delivery_date') }">
				    <label>Delivery Date:</label>
				    <div class="input-group">
					    <datepicker v-model="form.airway_delivery_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="airway_delivery_date"></datepicker>
					    <span class="input-group-addon">
			                <i class="fa fa-fw fa-calendar"></i>
			            </span>
			        </div> 
			        <span :is="form.errors.message" v-bind:error-message="form.errors.get('airway_delivery_date')"></span>
				</div>
			    <div class="form-group horizontal" :class="{'has-error' : form.errors.has('cargo') }">
				    <label>Cargo:</label>
				    <input type="text" class="form-control" name="cargo" v-model="form.cargo" :disabled="form.disabled">
				    <span :is="form.errors.message" v-bind:error-message="form.errors.get('cargo')"></span>
				</div>
			    <div class="form-group horizontal" :class="{'has-error' : form.errors.has('airway_delivery_no') }">
				    <label>DR No.:</label>
				    <input type="text" class="form-control" name="airway_delivery_no" v-model="form.airway_delivery_no" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('airway_delivery_no')"></span>    	
				</div>
			    <div class="form-group horizontal" :class="{'has-error' : form.errors.has('consignee_name') }">
				    <label>Consignee's Name:</label>
				    <input type="text" class="form-control" name="consignee_name" v-model="form.consignee_name" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('consignee_name')"></span>
				</div>
			    <div class="form-group horizontal" :class="{'has-error' : form.errors.has('shipper') }">
				    <label>Shippers Name:</label>
				    <input type="text" class="form-control" name="shipper" v-model="form.shipper" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('shipper')"></span>
				</div>
			</div>
			<div>
				<div class="form-group horizontal" :class="{'disabled': noEdit, 'has-error' : form.errors.has('vendor_purchase_order_no') }">
					<label>Purchase Order No.:</label>
					<combobox ref="pos" :options="@arr2str($pos)" @selected="getLineItems($event)" name="vendor_purchase_order_no" :async="true" :internal-search="false" :searchable="true" v-model="form.vendor_purchase_order_no" @ajax-search="getPurchaseOrders($event)"></combobox>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('vendor_purchase_order_no')"></span>
				</div>
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('vendor_delivery_no') }">
					<label>Delivery Receipt No.:</label>
					<input type="text" class="form-control" name="vendor_delivery_no" v-model="form.vendor_delivery_no" :disabled="noEdit">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('vendor_delivery_no')"></span>
				</div>
				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('vendor_delivery_date') }">
					<label>Delivery Date:</label>
					<div class="input-group">
						<datepicker v-model="form.vendor_delivery_date" :disabled="noEdit" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="vendor_delivery_date"></datepicker> 
						<span class="input-group-addon"><i class="fa fa-fw fa-calendar"></i></span>
					</div>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('vendor_delivery_date')"></span>
				</div>
				<div class="form-group horizontal"  :class="{ 'disabled': noEdit, 'has-error' : form.errors.has('description') }">
						<label>Line Items:</label>
					<combobox ref="line_items" :options="description_items" :hide-selected="true" :multiple="true" :auto-close="false" name="description" :searchable="true" @input="setDescription($event)"></combobox>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('description')"></span>
				</div>
				<div class="form-group horizontal" :class="{ 'disabled':  noEdit || from_pending, 'has-error' : form.errors.has('vendor_name')  }">
					<label>Vendor:</label>
					<combobox :options="@arr2str($vendors)" name="vendor_name" :searchable="true" v-model="form.vendor_name"></combobox>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('vendor_name')"></span>
				</div>
					<div class="form-group horizontal" :class="{ 'disabled':  noEdit || from_pending, 'has-error' : form.errors.has('bldg') }">
					<label>Site / Bldg:</label>
					<combobox :options="@arr2str($sites)" name="bldg" :searchable="true" v-model="form.bldg"></combobox>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('bldg')"></span>
				</div>
				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('serial_no') }">
					<label>Serial No.:</label>
					<input type="text" class="form-control" name="serial_no" v-model="form.serial_no" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('serial_no')"></span>
				</div>
				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('received_by') }">
					<label>Received By:</label>
					<input type="text" class="form-control" name="received_by" v-model="form.received_by" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('received_by')"></span>
				</div>
				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('delivery_attachments') }">
					<label>Attachments:</label>
					<input type="file" name="delivery_attachments" class="hidden" @change="form.errors.clear('delivery_attachments');parseFiles('delivery_attachments')">
					<div class="input-group">
						<input type="text" class="form-control pull-left" style="width: 87%" disabled="disabled" ref="attachment-input">
						<button type="button" class="btn btn-primary attachment-btn pull-left" @click.prevent="$event.target.parentElement.previousElementSibling.click()" :disabled="form.disabled">
							Browse
						</button>
					</div>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('delivery_attachments')"></span>
					<small class="help-block" v-if="!form.errors.has('delivery_attachments')"><em class="pull-right">(Maximum size of 3MB per file)</em></small>
				</div>
				<div class="attachments">
					<ol>
						<li v-for="(file, i) in form.delivery_attachments">
							<i class="fa fa-fw" :class="mimeType(file.type)" :title="file.type"></i>
							<a :href="file.uri" class="attachment-item" :download="file.name" :title="file.name"> @{{ file.name }} </a>
							<a class="delete-file" v-if="!form.disabled" @click.prevent="form.delivery_attachments.splice(i, 1)">[delete]</a>
						</li>
					</ol>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
