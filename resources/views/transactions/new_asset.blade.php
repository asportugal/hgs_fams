@extends('base', ['search' => 'true'])
@section('custom-tags')
<div class="pull-left" style="padding: 4.5px 10px; margin-left: 150px;" v-if="quantity">
	<label>Tag Assets: <span v-text="tagged_asset"></span></label>
</div>
@endsection
@section('content')
@setvar('view', 'new-asset')

<modal ref="modal" size="90" title="Pre-Assets">
	<dataset api="{{ route('pre_asset.pending') }}" :narrow-search="true" primary="delivery_id" :excludes="['delivery_reference', 'tag_date', 'farm_in']" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('new-asset.export') }}"></dataset>
</modal>

<div :class="component">
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)">
		<div class="row">
			<div>
				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('description') }">
					<label>Description:</label>
					<input type="text" class="form-control" name="description" v-model="form.description" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('description')"></span>
				</div>

				<div class="form-group horizontal" :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('category') }">
					<label>Category:</label>
					<combobox ref="categories" :options="{{ $categories }}" name="category" :searchable="true" label="name" track-by="name" v-model="form.category"></combobox>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('category')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('subcategory'), 'disabled' : form.disabled }">
					<label>Subcategory:</label>
					<combobox :options="subcategories" name="subcategory" v-model="form.subcategory"></combobox>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('subcategory')"></span>
				</div>

				<!-- <div class="form-group horizontal" :class="{'has-error' : mass_upload != '' ||  form.errors.has('bulk_serial')}">
					<label>Serial No:</label>
					<div class="input-group">
						<input type="text" class="form-control" v-model="file_name" disabled>
						<span class="input-group-addon" style="padding: 0">
						<input type="file" name="assets" class="hidden" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" @change="parseCSV($event)">
						<button style="height: 24px;" type="button" @click.prevent="$event.target.previousElementSibling.click()" class="btn btn-primary">Browse</button></span>
					</div>
					<span class="error" v-if="mass_upload != ''">@{{ mass_upload }}</span>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('bulk_serial')"></span>
				</div> -->

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('serial_no') }">
					<label>Serial No:</label>
					<input type="text" class="form-control" ref="serial_no" name="serial_no" v-model="form.serial_no" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('serial_no')"></span>
				</div>

				<div class="form-group horizontal" :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('bldg') }">
					<label>Site:</label>
					<combobox ref="sites" :options="{{ $sites }}" name="bldg" :searchable="true" label="name" track-by="name" v-model="form.bldg"></combobox>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('bldg')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('floor') }">
					<label>Floor:</label>
					<input type="text" class="form-control" name="floor" v-model="form.floor" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('floor')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('cube') }">
					<label>Area / Workstation:</label>
					<input type="text" class="form-control" name="cube" v-model="form.cube" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('cube')"></span>
				</div>

				<div class="form-group horizontal" :class="{'disabled': form.disabled, 'has-error' : form.errors.has('employee') }">
					<label>Employee:</label>
					<combobox :options="@arr2str($employees)" name="employee" :async="true" :internal-search="false" :searchable="true" v-model="form.employee" api="search-employees"></combobox>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('employee')"></span>
				</div>

				<div class="form-group horizontal" :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('lob_owner') }">
					<label>LOB Owner:</label>
					<combobox :options="@arr2str($lobs)" name="lob_owner" :searchable="true" v-model="form.lob_owner"></combobox>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('lob_owner')"></span>
				</div>

				<div class="form-group horizontal" :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('brand') }">
					<label>Manufacturer:</label>
					<combobox :options="@arr2str($brands)" name="brand" :searchable="true" v-model="form.brand"></combobox>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('brand')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('model') }">
					<label>Model:</label>
					<input type="text" class="form-control" name="model" v-model="form.model" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('model')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('peza_transaction_type') }">
					<label>Peza Transaction Type:</label>
					<input type="text" class="form-control" name="peza_transaction_type" v-model="form.peza_transaction_type" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('peza_transaction_type')"></span>
				</div>

				<div class="form-group horizontal">
					<label>Peza Farm In</label>
				</div>
				<div class="inner">
					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('farm_in_date') }">
						<label>Farm-in Date:</label>
						<div class="input-group">
						<datepicker v-model="form.farm_in_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="farm_in_date"></datepicker> 
						<span class="input-group-addon">
	                        <i class="fa fa-fw fa-calendar"></i>
	                    </span>
                    </div>
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('farm_in_date')"></span>
					</div>

					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('peza_in_form_no') }">
						<label>8105 Form No.:</label>
						<input type="text" class="form-control" name="peza_in_form_no" v-model="form.peza_in_form_no" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('peza_in_form_no')"></span>
					</div>

					<div class="form-group horizontal" :class="{'has-error' : form.errors.has('peza_in_permit_no') }">
						<label>8105 Permit No.:</label>
						<input type="text" class="form-control" name="peza_in_permit_no" v-model="form.peza_in_permit_no" :disabled="form.disabled">
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('peza_in_permit_no')"></span>
					</div>

					<div class="form-group horizontal"  :class="{'has-error' : form.errors.has('vat_paid') }">
						<label>VAT TD / Paid:</label>
						<div class="form-radio">
							<label class="radio-inline"><input type="radio" name="vat_paid" @click="form.vat_paid = '1'" :checked="form.vat_paid">Yes</label>
							<label class="radio-inline"><input type="radio" name="vat_paid" @click="form.vat_paid = '0'" :checked="!form.vat_paid">No</label>
						</div>
						<span :is="form.errors.message" v-bind:error-message="form.errors.get('vat_paid')"></span>
					</div>
				</div>

				<div class="form-group horizontal"  :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('financial_treatment') }">
					<label>Financial Treatment:</label>
					<combobox :options="@arr2str($financial_treatment)" name="financial_treatment" v-model="form.financial_treatment"></combobox>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('financial_treatment')"></span>
				</div>
			</div>
			<div>
				<div class="inner">
					<div>
						<div class="form-group horizontal" :class="{'has-error' : form.errors.has('asset_id') }">
							<label>Asset ID:</label>
							<input type="text" class="form-control" name="asset_id" v-model="form.asset_id" :disabled="true">
							<span :is="form.errors.message" v-bind:error-message="form.errors.get('asset_id')"></span>
						</div>

						<div class="form-group horizontal" :class="{'has-error' : form.errors.has('gr_no') }">
							<label>GR No.:</label>
							<input type="text" class="form-control" name="gr_no" v-model="form.gr_no" :disabled="true">
							<span :is="form.errors.message" v-bind:error-message="form.errors.get('gr_no')"></span>
						</div>

						<div class="form-group horizontal">
							<label class="form-static">Created Date: <span class="text-muted">@date('m/d/Y')</span></label>
						</div>
						<div class="form-group horizontal">
							<label class="form-static">Created User: <span class="text-muted">{{ strtoupper(Auth::user()->username) }}</span></label>
						</div>
					</div>

					<div>
						<div class="img-frame" :class="{active : !form.disabled}">
							<div class="img-wrapper">
								<img id="dp" alt="Asset Image" :src="image_src" onload="window.URL.revokeObjectURL(this.src);" />
								<input type="file" name="asset_image" class="hidden" @change="assignImage($event)" accept="image/*">
								<span class="uploader" @click="$event.target.previousElementSibling.click()"><i class="fa fa-camera fa-fw"></i> Upload Image</span>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('delivery_receipt_date') }">
					<label>Delivery Date:</label>
					<div class="input-group">
					<datepicker v-model="form.delivery_receipt_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="delivery_receipt_date"></datepicker> 
						<span class="input-group-addon">
	                        <i class="fa fa-fw fa-calendar"></i>
	                    </span>
                    </div>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('delivery_receipt_date')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('delivery_receipt_no') }">
					<label>Delivery Receipt No:</label>
					<input type="text" class="form-control" name="delivery_receipt_no" v-model="form.delivery_receipt_no" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('delivery_receipt_no')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('tag_date') }">
					<label>Tag Date:</label>
					<div class="input-group">
						<datepicker v-model="form.tag_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="tag_date"></datepicker> 
						<span class="input-group-addon">
	                        <i class="fa fa-fw fa-calendar"></i>
	                    </span>
                    </div>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('tag_date')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('sales_invoice_no') }">
					<label>Sales Invoice No.:</label>
					<input type="text" class="form-control" name="sales_invoice_no" v-model="form.sales_invoice_no" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('sales_invoice_no')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('purchase_order_no') }">
					<label>Purchase Order No.:</label>
					<input type="text" class="form-control" name="purchase_order_no" v-model="form.purchase_order_no" :disabled="form.disabled">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('purchase_order_no')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('purchase_order_date') }">
					<label>Purchase Date:</label>
					<div class="input-group">
					<datepicker v-model="form.purchase_order_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="purchase_order_date"></datepicker> 
					<span class="input-group-addon">
	                        <i class="fa fa-fw fa-calendar"></i>
	                    </span>
                    </div>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('purchase_order_no')"></span>
				</div>

				<div class="form-group horizontal" :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('supplier') }">
					<label>Vendor Name:</label>
					<combobox :options="@arr2str($vendors)" name="supplier" :searchable="true" v-model="form.supplier"></combobox>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('supplier')"></span>
				</div>

				<div class="checkbox form-group horizontal" :class="{'has-error' : form.errors.has('imported') }">
					<label><input v-model="form.imported" type="checkbox"><strong>Imported Constructive Importation</strong></label>
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('imported')"></span>
				</div>    				

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('import_permit_no') }">
					<label>Permit No.:</label>
					<input type="text" class="form-control" name="import_permit_no" v-model="form.import_permit_no" :disabled="form.disabled || !form.imported">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('import_permit_no')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('pbo_no') }">
					<label>PBO No:</label>
					<input type="text" class="form-control" name="pbo_no" v-model="form.pbo_no" :disabled="form.disabled || !form.imported">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('pbo_no')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('import_cewe_no') }">
					<label>CEWE No.:</label>
					<input type="text" class="form-control" name="import_cewe_no" v-model="form.import_cewe_no" :disabled="form.disabled || !form.imported">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('import_cewe_no')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('import_boat_note') }">
					<label>Boat Note No.:</label>
					<input type="text" class="form-control" name="import_boat_note" v-model="form.import_boat_note" :disabled="form.disabled || !form.imported">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('import_boat_note')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('import_boat_note') }">
					<label>BOC Certificate:</label>
					<input type="text" class="form-control" name="import_boc_cert" v-model="form.import_boc_cert" :disabled="form.disabled || !form.imported">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('import_boc_cert')"></span>
				</div>

				<div class="form-group horizontal" :class="{'has-error' : form.errors.has('import_boat_note') }">
					<label>NTC Permit Certificate No:</label>
					<input type="text" class="form-control" name="import_ntc_permit_no" v-model="form.import_ntc_permit_no" :disabled="form.disabled || !form.imported">
					<span :is="form.errors.message" v-bind:error-message="form.errors.get('import_ntc_permit_no')"></span>
				</div>
			</div>
		</div>
	</form>
</div>
@endsection
