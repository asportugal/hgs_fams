@extends('base', ['search' => 'true'])
@section('content')
@setvar('view', 'farm-out')

<modal ref="modal" size="90" title="PEZA Farm Out (8105)">
<dataset api="{{ route('farm-out.index') }}" :hide-primary="true" primary="farm_out_id" :excludes="['']" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('farm-out.export') }}"></dataset>
</modal>


<div :class="component">	
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)" ref="{{ $view }}">
	<div class="row">	
		<div>
			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('control_no') }">
				<label>Control No:</label>
				<input type="text" class="form-control" name="control_no" v-model="form.control_no" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('control_no')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('location') }">
				<label>Location:</label>
				<input type="text" class="form-control" name="location" v-model="form.location" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('location')"></span>
			</div>

			<div class="form-group horizontal" :class="{ 'has-error' : form.errors.has('vendor') }">
				<label>Vendor:</label>
				<input type="text" class="form-control" name="vendor" v-model="form.vendor" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('vendor')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('proforma_invoice_no') }">
				<label>Proforma Invoice No:</label>
				<input type="text" class="form-control" name="proforma_invoice_no" v-model="form.proforma_invoice_no" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('proforma_invoice_no')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('proforma_invoice_date') }">
				    <label>Proforma Invoice Date:</label>
				    <div class="input-group">
					    <datepicker v-model="form.proforma_invoice_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="proforma_invoice_date"></datepicker> 
					    <span class="input-group-addon">
	                        <i class="fa fa-fw fa-calendar"></i>
	                    </span>
                    </div>
                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('proforma_invoice_date')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('final_invoice_no') }">
				<label>Final Invoice No:</label>
				<input type="text" class="form-control" name="final_invoice_no" v-model="form.final_invoice_no" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('final_invoice_no')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('final_invoice_date') }">
				    <label>Final Invoice Date:</label>
				    <div class="input-group">
					    <datepicker v-model="form.final_invoice_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="final_invoice_date"></datepicker> 
					    <span class="input-group-addon">
	                        <i class="fa fa-fw fa-calendar"></i>
	                    </span>
                    </div>
                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('final_invoice_date')"></span>
			</div>
		</div>
		<div>
			<div class="form-group horizontal" :class="{'disabled' : form.disabled, 'has-error' : form.errors.has('peza_permit_status') }">
				<label>Peza Permit Status:</label>
				<combobox :options="@arr2str(['YES', 'NO', 'Not Applicable'])" name="peza_permit_status" v-model="form.peza_permit_status"></combobox>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('peza_permit_status')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('peza_form_no') }">
				<label>Peza Form No:</label>
				<input type="text" class="form-control" name="peza_form_no" v-model="form.peza_form_no" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('peza_form_no')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('peza_approval_no') }">
				<label>Peza Approval No:</label>
				<input type="text" class="form-control" name="peza_approval_no" v-model="form.peza_approval_no" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('peza_approval_no')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('request_date') }">
			    <label>Request Date:</label>
			    <div class="input-group">
				    <datepicker v-model="form.request_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="request_date"></datepicker> 
				    <span class="input-group-addon">
                        <i class="fa fa-fw fa-calendar"></i>
                    </span>
                </div>
                <span :is="form.errors.message" v-bind:error-message="form.errors.get('request_date')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('date_filed') }">
			    <label>Date Filed:</label>
			    <div class="input-group">
				    <datepicker v-model="form.date_filed" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="date_filed"></datepicker> 
				    <span class="input-group-addon">
                        <i class="fa fa-fw fa-calendar"></i>
                    </span>
                </div>
                <span :is="form.errors.message" v-bind:error-message="form.errors.get('date_filed')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('remarks') }">
				<label>Remarks:</label>
				<textarea style="height: 65px;" class="form-control" name="remarks" v-model="form.remarks" :disabled="form.disabled"></textarea>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('remarks')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('farm_out_attachments') }">
			    <label>Attachments:</label>
			    <input type="file" name="farm_out_attachments" class="hidden" @change="form.errors.clear('farm_out_attachments');parseFiles('farm_out_attachments')">
			    <div class="input-group">
			    	<input type="text" class="form-control pull-left" style="width: 85%" disabled="disabled" ref="attachment-input">
			    	<button type="button" class="btn btn-primary attachment-btn pull-left" @click.prevent="$event.target.parentElement.previousElementSibling.click()" :disabled="form.disabled">
			    	Browse
			   		</button>
			    </div>
			    <span :is="form.errors.message" v-bind:error-message="form.errors.get('farm_out_attachments')"></span>
			    <small class="help-block" v-if="!form.errors.has('farm_out_attachments')"><em class="pull-right">(Maximum size of 3MB per file)</em></small>
			</div>
			<div class="attachments">
				<ol>
					<li v-for="(file, i) in form.farm_out_attachments">
						<i class="fa fa-fw" :class="mimeType(file.type)" :title="file.type"></i>
						<a :href="file.uri" class="attachment-item" :download="file.name" :title="file.name"> @{{ file.name }} </a>
						<a class="delete-file" v-if="!form.disabled" @click.prevent="form.farm_out_attachments.splice(i, 1)">[delete]</a>
					</li>
				</ol>
			</div>
		</div>
	</div>
	</form>
</div>

@endsection