@extends('base', ['search' => 'true'])
@section('content')
@setvar('view', 'loan-assets')

<modal ref="modal" size="90" title="Loan Assets">
	<dataset api="{{ route('loan-assets.index') }}" :narrow-search="true" primary="loan_id" :excludes="['delivery_reference']" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('loan-assets.export') }}"></dataset>
</modal>

<div :class="component">
	<div class="asset-info">
		<h4>Asset Information</h4>
		<div class="img-frame">
			<div class="img-wrapper">
				<img id="dp" alt="Asset Image" :src="image_src" onload="window.URL.revokeObjectURL(this.src);" />
			</div>
		</div>
		<dl class="dl-horizontal" v-for="(val, key) in asset_info">
		  	<dt class="text-capitalize" v-text="key.replace(/_/g, ' ')"></dt>
			<dd v-if="val">@{{ val }}</dd><dd v-else> NONE </dd>
		</dl>
	</div>
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)" ref="{{ $view }}">
		<div v-if="status == 'PENDING' && isApprover('{{ Auth::user()->username }}')" class="text-center" style="width: 100%; padding: 5px 0px 20px;">
			<div class="btn-group-horizontal" role="group">
				<a class="btn btn-primary btn-md"  @click.prevent="actionRequest('APPROVED')" >
				<i v-if="actionLoader == 'APPROVED'" class="fa fa-spinner fa-spin"></i> <i v-else class="fa fa-fw fa-check"></i> Approve</a>
				<a class="btn btn-danger btn-md"   @click.prevent="actionRequest('DECLINED')" >
				<i v-if="actionLoader == 'DECLINED'" class="fa fa-spinner fa-spin"></i>
				<i v-else class="fa fa-fw fa-times"></i> Decline</a>
			</div>
		</div>
		<div class="first">
			
		</div>

		<div>
			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('asset_id') }">
				<label>Asset ID:</label>
				<input type="text" class="form-control" name="asset_id" v-model="form.asset_id" :disabled="{{ Auth::user()->is_admin() }}">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('asset_id')"></span>
			</div>
			<div class="form-group horizontal">
					<dl class="dl-horizontal static">
					  	<dt>Approved By:</dt><dd v-if="approved_by">@{{ approved_by }}</dd><dd v-else> NONE </dd>
					</dl>
					<dl class="dl-horizontal static" style="margin-bottom:0">
					  	<dt>Approved Date:</dt><dd v-if="approved_date">@{{ approved_date }}</dd><dd v-else> NONE </dd>
					</dl>
				</div>
			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('loan_status') }">
				<label>Loan Status:</label>
				<input type="text" class="form-control" name="loan_status" v-model="form.loan_status" :disabled="true">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('loan_status')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('itaf_no') }">
				<label>ITAF No:</label>
				<input type="text" class="form-control" name="itaf_no" v-model="form.itaf_no" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('itaf_no')"></span>
			</div>

			<div class="form-group horizontal" :class="{'disabled': form.disabled, 'has-error' : form.errors.has('requestor') }">
				<label>Requestor:</label>
				<multiselect v-model="form.requestor" id="ajax" placeholder="" name="requestor" :options="employees" :searchable="true" :loading="isLoading" :show-labels="false" :internal-search="false" @search-change="asyncFindEmployee" @open="form.errors.clear('requestor')">
				<span slot="noResult">No records found.</span></multiselect>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('requestor')"></span>
			</div>

			<div class="form-group horizontal"  :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('approver') }">
				<label>Approver:</label>
				<multiselect :options="{{ $loan_approver }}" :searchable="false" :show-labels="false" name="approver" placeholder="" v-model="approvers" @open="form.errors.clear('approver')" label="group_name" track-by="approver" ref="approvers"></multiselect>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('approver')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('start_date') }">
				    <label>Loan Start Date:</label>
				    <div class="input-group">
					    <datepicker v-model="form.start_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="start_date"></datepicker> 
					    <span class="input-group-addon">
	                        <i class="fa fa-fw fa-calendar"></i>
	                    </span>
                    </div>
                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('start_date')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('end_date') }">
				    <label>Loan End Date:</label>
				    <div class="input-group">
					    <datepicker v-model="form.end_date" :disabled="form.disabled" :options="{allowInput: true, dateFormat: 'm/d/Y' }" name="end_date"></datepicker> 
					    <span class="input-group-addon">
	                        <i class="fa fa-fw fa-calendar"></i>
	                    </span>
                    </div>
                    <span :is="form.errors.message" v-bind:error-message="form.errors.get('end_date')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('checked_out_by') }">
				<label>Checked Out By:</label>
				<input type="text" class="form-control" name="checked_out_by" v-model="form.checked_out_by" :disabled="isCheckOut">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('checked_out_by')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('checked_in_by') }">
				<label>Checked In By:</label>
				<input type="text" class="form-control" name="checked_in_by" v-model="form.checked_in_by" :disabled="isCheckIn">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('checked_in_by')"></span>
			</div>

			<div v-if="form.checked_out_date" class="form-group horizontal" :class="{'has-error' : form.errors.has('checked_in_remarks') }">
				<label>Checked In Remarks:</label>
				<textarea style="height: 50px;" class="form-control" name="checked_in_remarks" v-model="form.checked_in_remarks" :disabled="isCheckIn"></textarea>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('checked_in_remarks')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('remarks') }">
				<label>Remarks:</label>
				<textarea style="height: 80px;" class="form-control" name="remarks" v-model="form.remarks" :disabled="form.disabled"></textarea>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('remarks')"></span>
			</div>
		</div>
	</form>
</div>
@endsection