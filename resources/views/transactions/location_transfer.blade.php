@extends('base', ['search' => 'true'])
@section('custom-buttons')
<button v-if="status == 'APPROVED'" class="btn btn-sm" @click="printForm('{{ route('location-transfer.print') }}')">
	<i class="fa fa-fw fa-print blue"></i> Print
</button>
@endsection
@section('content')
@setvar('view', 'location-transfer')

<modal ref="modal" size="90" title="Location Transfer">
	<dataset api="{{ route('location-transfer.index') }}" :narrow-search="true" primary="location_transfer_id" :excludes="['delivery_reference']" :utils="{{ json_encode(['search', 'export']) }}" :paginate="true" :async="true" :info="true" @selected-row="getSelectedRow($event)" export="{{ route('location-transfer.export') }}"></dataset>
</modal>

<div :class="component">
	<div class="asset-info">
		<h4>Asset Information</h4>
		<div class="img-frame">
			<div class="img-wrapper">
				<img id="dp" alt="Asset Image" :src="image_src" onload="window.URL.revokeObjectURL(this.src);" />
			</div>
		</div>
		<dl class="dl-horizontal" v-for="(val, key) in asset_info">
		  	<dt class="text-capitalize" v-text="key.replace(/_/g, ' ')"></dt>
			<dd v-if="val">@{{ val }}</dd><dd v-else> NONE </dd>
		</dl>
	</div>
	<form method="POST" ref="{{ $view }}" @submit.prevent @keydown="form.errors.clear($event.target.name)" @change="form.errors.clear($event.target.name)" ref="{{ $view }}">
		<div v-if="status == 'PENDING' && isApprover('{{ Auth::user()->username }}')" class="text-center" style="width: 100%; padding: 5px 0px 20px;">
			<div class="btn-group-horizontal" role="group">
				<a class="btn btn-primary btn-md"  @click.prevent="actionRequest('APPROVED')" >
				<i v-if="actionLoader == 'APPROVED'" class="fa fa-spinner fa-spin"></i> <i v-else class="fa fa-fw fa-check"></i> Approve</a>
				<a class="btn btn-danger btn-md"   @click.prevent="actionRequest('DECLINED')" >
				<i v-if="actionLoader == 'DECLINED'" class="fa fa-spinner fa-spin"></i>
				<i v-else class="fa fa-fw fa-times"></i> Decline</a>
			</div>
		</div>
		<div>
			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('asset_id') }">
				<label>Asset ID:</label>
				<input type="text" class="form-control" name="asset_id" v-model="form.asset_id" :disabled="{{ Auth::user()->is_admin() }}">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('asset_id')"></span>
			</div>
			<div class="form-group horizontal">
					<dl class="dl-horizontal static">
					  	<dt>Approved By:</dt><dd v-if="approved_by">@{{ approved_by }}</dd><dd v-else> NONE </dd>
					</dl>
					<dl class="dl-horizontal static" style="margin-bottom:0">
					  	<dt>Approved Date:</dt><dd v-if="approved_date">@{{ approved_date }}</dd><dd v-else> NONE </dd>
					</dl>
				</div>
			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('transfer_status') }">
				<label>Transfer Status:</label>
				<input type="text" class="form-control" name="transfer_status" v-model="form.transfer_status" :disabled="true">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('transfer_status')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('itaf_no') }">
				<label>ITAF No:</label>
				<input type="text" class="form-control" name="itaf_no" v-model="form.itaf_no" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('itaf_no')"></span>
			</div>

			<div class="form-group horizontal"  :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('approver') }">
				<label>Approver:</label>
				<multiselect :options="{{ $loan_approver }}" :searchable="false" :show-labels="false" name="approver" placeholder="" v-model="approvers" @open="form.errors.clear('approver')" label="group_name" track-by="approver" ref="approvers"></multiselect>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('approver')"></span>
			</div>		

			<div class="form-group horizontal" :class="{'disabled':  form.disabled, 'has-error' : form.errors.has('to_site') }">
				<label>Site:</label>
				<combobox :options="@arr2str($sites)" name="to_site" :searchable="true" v-model="form.to_site"></combobox>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('to_site')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('to_floor') }">
				<label>Floor:</label>
				<input type="text" class="form-control" name="to_floor" v-model="form.to_floor" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('to_floor')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('to_cube') }">
				<label>Area / Workstation:</label>
				<input type="text" class="form-control" name="to_cube" v-model="form.to_cube" :disabled="form.disabled">
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('to_cube')"></span>
			</div>

			<div class="form-group horizontal" :class="{'has-error' : form.errors.has('transfer_remarks') }">
				<label>Transfer Remarks:</label>
				<textarea style="height: 80px;" class="form-control" name="transfer_remarks" v-model="form.transfer_remarks" :disabled="form.disabled"></textarea>
				<span :is="form.errors.message" v-bind:error-message="form.errors.get('transfer_remarks')"></span>
			</div>
		</div>
	</form>
</div>
@endsection