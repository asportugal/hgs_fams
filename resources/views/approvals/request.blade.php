@extends('base')
@section('content')
@include('partials.buttons', ['search' => 'false', 'operations' => []])
@setvar('view', 'request')
<div :class="component">
<form ref="{{ $view }}">
	<div>
		<div>
			<label>Requested By:</label>
			<span class="form-control-static">{{ $request->requested_by }} for <strong>{{ $request->operation }}</strong> on <strong>{{ strtoupper($request->entity) }}</strong> record </span>
		</div>
	</div>
	@if(!in_array($request->status, ['APPROVED', 'DECLINED']))
	<div style="padding: 10px 0">
		<div class="btn-group-horizontal" role="group">
			<a class="btn btn-primary btn-md" @click.prevent="action('approve', '{{ $request->approval_id }}', '{{ Auth::user()->username }}')">Approve</a>
			<a class="btn btn-danger btn-md" @click.prevent="action('decline', '{{ $request->approval_id }}', '{{ Auth::user()->username }}')">Decline</a>
		</div>
	</div>
	@endif
	<div style="padding-top: 20px">
	@foreach(json_decode($request->content) as $key => $content)
		<div>
		<label>{{ title_case($key) }}: </label>
		@if(!is_array($content))
			@if($content == "" || empty($content))
				<span class="form-control-static">none</span>
			@else
				<span class="form-control-static">{{ $content }}</span>
			@endif
		@else
			@foreach($content as $c)
				<div class="attachments"><ol>
					<li>
						<i class="fa fa-fw" :class="mimeType('{{ preg_replace('/[^a-zA-Z0-9\']/', '', $c->mime_type) }}')"></i>
						<a href="{{ 'data:'.$c->mime_type.';base64,'.$c->data }}" class="attachment-item" download="{{ $c->file_name }}" title="name">{{ $c->file_name }}</a>
					</li></ol>
				</div>
			@endforeach
		@endif
		</div>
	@endforeach
	</div>
</form>
</div>
@endsection