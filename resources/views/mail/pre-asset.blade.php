<body style="font-family: Arial; font-size: 12px; ">
<img src="{{ $message->embed('img/logo.png') }}" style="padding: 30px 0; width: 230px;" />
<table cellspacing="2" width="70%">
<tbody>
	<tr>
		<th colspan="2" style="border: 1px solid #000">Airway Details</th>
	</tr>
	<tr>
		<td><font-size="11">Airway Cargo: <strong>{{ $delivery['cargo'] or 'N/A' }}</strong></font></td>
		<td><font-size="11">Airway Consignee Name: <strong>{{ $delivery['consignee_name'] or 'N/A' }}</strong></font></td>
	</tr>
	<tr>
		<td><font-size="11">Airway Delivery Date: <strong>{{ $delivery['airway_delivery_date'] or 'N/A' }}</strong></font></td>
		<td><font-size="11">Airway Shipper Name: <strong>{{ $delivery['shipper'] or 'N/A' }}</strong></font></td>
	</tr>
	<tr>
		<td><font-size="11">Airway Delivery No: <strong>{{ $delivery['airway_delivery_no'] or 'N/A' }}</strong></font></td>
		<td><font-size="11">&nbsp;</font></td>
	</tr>
	<tr>
		<td colspan="2" style="padding: 5px;">&nbsp;</td>
	</tr>
	<tr>
		<th colspan="2" style="border: 1px solid #000">Vendor Details</th>
	</tr>
	<tr>
		<td><font-size="11">Site: <strong>{{ $delivery['bldg'] }}</strong></font></td>
		<td><font-size="11">Vendor Delivery Date: <strong>{{ $delivery['vendor_delivery_date'] }}</strong></font></td>
	</tr>
	<tr>
		<td><font-size="11">Vendor:  <strong>{{ $delivery['vendor_name'] }}</strong></font></td>
		<td><font-size="11">Delivery Receipt No: <strong>{{ $delivery['vendor_delivery_no'] }}</strong></font></td>
	</tr>
	<tr>
		<td><font-size="11">Purchase No:  <strong>{{ $delivery['vendor_purchase_order_no'] }}</strong></font></td>
		<td><font-size="11">Description: </font></td>
	</tr>
	<tr>
		<td><font-size="11">Received By: <strong>{{ $delivery['received_by'] or 'N/A'}}</strong></font></td>
		<td rowspan="2" valign="top">
			@foreach($delivery['line_items'] as $item)
				<font-size="11"><strong>{{ $item['description'] }} x {{ $item['quantity'] }}</strong></font><br>
			@endforeach
		</td>
	</tr>
	<tr>
		<td><font-size="11">Serial:  <strong>{{ $delivery['serial_no'] or 'NONE' }}</strong></font></td>
		<td><font-size="11">&nbsp;</font></td>
	</tr>
	<tr>
		<td colspan="2"><hr style="margin: 20px 0"></td>
	</tr>
	<tr>
		<td colspan="2">
			<a href="{{ route('home') }}"><h3 style="margin: 5px 0">Fixed Asset Management System</h3></a>
		</td>
	</tr>
	<tr>
		<td colspan="2"><font-size="9">This is a system generated message, do not reply.</font></td>
	</tr>
</tbody>
</table>
<body>