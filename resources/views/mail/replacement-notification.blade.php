<body style="font-family: Arial; font-size: 12px; ">
<img src="{{ $message->embed('img/logo.png') }}" style="padding: 30px 0; width: 230px;" /><br>

Your approval is required for the requested asset replacement below: <br><br>
<strong>ITAF No: </strong> {{ $replacement->itaf_no }} <br>
<strong>Requestor: </strong> {{ $replacement->requested_by }} <br>
<strong>Replacement Serial No: </strong> {{ $replacement->serial_no }} <br>
<strong>Remarks :</strong> {{ $replacement->replacement_remarks }} <br>

<!-- To approve, reply to this email with APPROVE message. <br>
To decline, reply to this email with DECLINE message. <br><br> -->

Click this <a href="{{ route('asset-replacement') . '?id=' . base64_encode($replacement->replacement_id) }}">link</a> to approve/decline asset replacement request. <br>

<hr style="margin: 20px 0;">
<a href="{{ route('home') }}"><h3 style="margin: 5px 0">Fixed Asset Management System</h3></a>
<font-size="9">This is a system generated message, do not reply.</font>
</body>