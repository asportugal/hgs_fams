<body style="font-family: Arial; font-size: 12px; ">
<img src="{{ $message->embed('img/logo.png') }}" style="padding: 30px 0; width: 230px;" /><br>

Your approval is required for the requested asset disposal below: <br><br>
<strong>ITAF No: </strong> {{ $disposal->itaf_no }} <br>
<strong>Requestor: </strong> {{ $disposal->requested_by }} <br>
<strong>Disposal Method: </strong> {{ $disposal->disposal_method }} <br>
<strong>Disposal Vendor: </strong> {{ $disposal->disposal_vendor }} <br>
<strong>Remarks :</strong> {{ $disposal->disposal_remarks }} <br>

<!-- To approve, reply to this email with APPROVE message. <br>
To decline, reply to this email with DECLINE message. <br><br> -->

Click this <a href="{{ route('asset-disposal') . '?id=' . base64_encode($disposal->disposal_id) }}">link</a> to approve/decline asset disposal request. <br>

<hr style="margin: 20px 0;">
<a href="{{ route('home') }}"><h3 style="margin: 5px 0">Fixed Asset Management System</h3></a>
<font-size="9">This is a system generated message, do not reply.</font>
</body>