<body style="font-family: Arial; font-size: 12px; ">
<img src="{{ $message->embed('img/logo.png') }}" style="padding: 30px 0; width: 230px;" /><br>

Your {{ strtolower($type) }} request with ITAF No. 
<strong>{{ $request['itaf_no'] }}</strong> has been {{ $request['status'] }}.<br>

Click this <a href="{{ $url }}">link</a> to view {{ strtolower($type) }} request details. <br>

<hr style="margin: 20px 0;">
<a href="{{ route('home') }}"><h3 style="margin: 5px 0">Fixed Asset Management System</h3></a>
<font-size="9">This is a system generated message, do not reply.</font>
</body>