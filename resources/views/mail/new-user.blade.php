<body style="font-family: Arial; font-size: 12px; ">
<img src="{{ $message->embed('img/logo.png') }}" style="padding: 30px 0; width: 230px;" /><br>

Your FAMS account has been succesfully created!<br><br>

Login to <a href="{{ route('home') }}">FAMS</a> using this credentials below: <br><br>

<strong>Username: </strong> {{ $user->username }} <br>
<strong>Password: </strong> {{ $password }} <br>

<hr style="margin: 20px 0;">
<a href="{{ route('home') }}"><h3 style="margin: 5px 0">Fixed Asset Management System</h3></a>
<font-size="9">This is a system generated message, do not reply.</font>
</body>