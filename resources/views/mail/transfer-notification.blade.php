<body style="font-family: Arial; font-size: 12px; ">
<img src="{{ $message->embed('img/logo.png') }}" style="padding: 30px 0; width: 230px;" /><br>

Your approval is required for the requested asset transfer below: <br><br>
<strong>ITAF No: </strong> {{ $transfer->itaf_no }} <br>
<strong>Requestor: </strong> {{ $transfer->requested_by }} <br>
<strong>Site: </strong> <strong> from </strong> {{ $transfer->from_site }} <strong> to </strong> {{ $transfer->to_site }} <br>
<strong>Floor: </strong> {{ $transfer->to_floor or 'N/A' }} <br>
<strong>Cube :</strong> {{ $transfer->to_cube or 'N/A' }} <br>
<strong>Remarks :</strong> {{ $transfer->transfer_remarks }} <br>

<!-- To approve, reply to this email with APPROVE message. <br>
To decline, reply to this email with DECLINE message. <br><br> -->

Click this <a href="{{ route('location-transfer') . '?id=' . base64_encode($transfer->location_transfer_id) }}">link</a> to approve/decline asset transfer request. <br>

<hr style="margin: 20px 0;">
<a href="{{ route('home') }}"><h3 style="margin: 5px 0">Fixed Asset Management System</h3></a>
<font-size="9">This is a system generated message, do not reply.</font>
</body>