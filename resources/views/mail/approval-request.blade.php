<body style="font-family: Arial; font-size: 12px; ">
<img src="{{ $message->embed('img/logo.png') }}" style="padding: 30px 0; width: 230px;" /><br>

Dear {{ $request->approver }},<br><br>

Your approval is required to reflect changes made by {{ $request->requested_by }} <br>
Please, click on this <a href="{{ route('request', $request->approval_id) }}">link</a> to approve <br>

<hr style="margin: 20px 0;">
<a href="{{ route('home') }}"><h3 style="margin: 5px 0">Fixed Asset Management System</h3></a>
<font-size="9">This is a system generated message, do not reply.</font>
</body>