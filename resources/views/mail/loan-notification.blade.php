<body style="font-family: Arial; font-size: 12px; ">
<img src="{{ $message->embed('img/logo.png') }}" style="padding: 30px 0; width: 230px;" /><br>

Your approval is required for the requested loan below: <br><br>
<strong>ITAF No: </strong> {{ $loan->itaf_no }} <br>
<strong>Requestor: </strong> {{ $loan->loan_to }} <br>
<strong>Item Description:</strong> {{ $loan->loan_remarks }} <br>
<strong>Start Date:</strong> {{ $loan->loan_start_date }} <br>
<strong>End Date:</strong> {{ $loan->loan_end_date }} <br><br>

<!-- To approve, reply to this email with APPROVE message. <br>
To decline, reply to this email with DECLINE message. <br><br> -->

Click this <a href="{{ route('loan-assets') . '?id=' . base64_encode($loan->loan_id) }}">link</a> to approve/decline loan request.<br>

<hr style="margin: 20px 0;">
<a href="{{ route('home') }}"><h3 style="margin: 5px 0">Fixed Asset Management System</h3></a>
<font-size="9">This is a system generated message, do not reply.</font>
</body>