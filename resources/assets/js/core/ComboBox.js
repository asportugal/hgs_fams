import Multiselect from 'vue-multiselect';

var Combobox = {
	components: {
		Multiselect
	},
	props: {
		name: String,
		value: { String, Number },
		options: Array,
		trackBy: String,
		removeSelected: {
			type: Boolean,
			default: false
		},
		multiple: {
			type: Boolean,
			default: false
		},
		hideSelected: {
			type: Boolean,
			default: false
		},
		autoClose: {
			type: Boolean,
			default: true
		},
		internalSearch: {
			type: Boolean,
			default: true
		},
		label: String,
		searchable: {
			type: Boolean,
			default: false
		},
		async: {
			type: Boolean,
			default: false
		},
		limit: {
			type: Number,
			default: 10
		},
	},
	template: '<multiselect :hide-selected="hideSelected" :multiple="multiple"'+
			':close-on-select="autoClose" @open="clearErrors" @input="returnValues" @remove="removeValue" @select="returnValue" @search-change="asyncFind" :async="async"'+ 
			':internal-search="internalSearch" :loading="isLoading" :name="name" :searchable="searchable" :options="multiSelectOptions"'+ 
			':labels="selectLabels" v-model="internalValue" placeholder="" :show-labels="false"></multiselect>',
	data() {
		return {
			isLoading: false,
			externalValue: '',
			internalValue: '',
			selectLabels: [],
			selectValues: [],
			originalOptions: this.options.slice(0),
			selectOpts: this.options,
		}
	},
	methods: {
		clearInput() {
			this.externalValue = '';
			this.internalValue = '';
		},
		returnValues(value) {
			this.$emit('input', value);
		},
		removeValue(value) {
			this.$emit('delete', value);
		},
		returnValue(value) {
			this.$emit('selected', value)
		},
		asyncFind(query) {
			if(this.async) {
				this.$emit('ajax-search', query);
			}
		},
		getExternalValue(value) {
			return this.selectOpts[this.selectValues.indexOf(value)];
		},
		setSelectOptions(obj) {
			this.selectOpts = obj.options;
			this.createSelectList();
			if(typeof obj.default === 'function') {
				obj.default(this, obj.options[0]);
			}
		},
		createSelectList() {
			this.selectLabels = [];
			this.selectValues = [];
			for(let x in this.selectOpts) {
				if(this.label && this.trackBy) { 
					this.selectLabels.push(this.selectOpts[x][this.trackBy]);
					this.selectValues.push(this.selectOpts[x][this.label]);
				} else {
					this.selectLabels.push(this.selectOpts[x]);
					this.selectValues.push(this.selectOpts[x]);
				}
			}
		},
		clearErrors() {
			this.$root.form.errors.clear(this.name);
		},
		renderOptions(value) {
			this.$emit('render', value)
		}
	},
	watch: {
		options: {
			handler(val) {
				this.selectOpts = val;
				this.createSelectList();
			},
        	deep: true
		},
		externalValue(value) {
			if(!this.multiple) {
				this.$emit('input', value);
			}
		},
		internalValue(value) {
			if(!this.multiple) {
				this.externalValue = this.selectLabels[this.selectValues.indexOf(value)];
			}
		},
		value: {
			handler() {
				this.externalValue = this.value;
				this.internalValue = this.selectValues[this.selectLabels.indexOf(this.value)];
			},
          	deep: true
		}
	},
	created() {
		this.createSelectList();
	},
	computed: {
		multiSelectOptions() {
			return this.selectValues;
		},
		selectOptions() {
			var options = [];
			for(let x in this.selectLabels) {
				var object = {name: this.selectLabels[x], val: this.selectValues[x]}
				options.push(object);
			}
			return options;
		},
	}
}
export default Combobox;