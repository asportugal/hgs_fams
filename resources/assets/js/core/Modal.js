var Modal = {
	name: 'modal',
	props: ['title', 'size'],
	template: '<div @keyup.esc="close()" class="modal" v-bind:style="styleObj" v-if="show" tabindex="-1" role="dialog">'+
  				'<div class="modal-dialog" v-bind:style="width" role="document">'+
    			'<div class="modal-content"><div class="modal-header">'+
		        '<button type="button" class="close" aria-label="Close" @click="show = false"><span aria-hidden="true">&times;</span></button>'+
		        '<h4 class="modal-title">{{ title }}</h4></div><div class="modal-body">'+
		        '<slot></slot></div></div></div></div>',
	data() {
		return {
			show: false,
			styleObj: {}
		}
	},
	methods: {
		close() {
			this.show = false;
		}
	},
	watch: {
		show(val) {
			if(val) { this.styleObj = { display: 'block', opacity: '1' } }
		}
	},
	computed: {
		width() {
			var width = 'auto';
			if(this.size) {
				 width = { width: this.size + '%' };
			}
			return width;
		}
	}
}

export default Modal;