var Notification = {
	name: 'notification',
	props: {
		display: {
			type: Boolean,
			default: false
		},
		alertType: {
			type: String,
			default: ''
		},
	},
	template: '<div id="alert" v-if="show" class="push-notif alert alert-dismissable" :class="type" :style="opacity">'+
        '<a class="close" data-dismiss="alert" aria-label="close" @click="show = false">×</a>'+
        '<slot>{{ message }}</slot></div>',
	data() {
		return {
			show: this.display,
			type: this.alertType,
			message: '',
			opacity: ''
		}
	},
	watch: {
		show(val) {
			if(val) {
				setTimeout(() => { this.opacity = 'opacity: 0'; this.show = false }, 5000);
			}
		}
	}
}

export default Notification;