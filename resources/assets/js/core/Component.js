import Promise from 'promise';
class Component {

	static load(element) {
		let name = window.location.pathname.toLowerCase().replace('/', '').split('/');
		let v = this.instance(element);
		__webpack_public_path__ = window.location.origin;
		
		return new Promise((resolve, reject) => {
			require.ensure([], () => {
				this.options = require('../component/' + name[1].replace('-', '_')).default;
				if(this.options) {
					let component = Object.assign(v, this.options);
					resolve(component);
				} else {
				 	reject('Error');
				}
			});
		});
	}

	static instance(element) {
		return {
			el: element,
			mixins: [this.mixins()],
			mounted: this.mounted
		}
	}

	static mixins() {
		return {
			data() {
				return {
					save: false,
					create: false,
					update: false,
					delete: false
				}
			},
			watch: {
				'form.isComponentEvent'(value) {
					if(!value) {
						this.save = false;
						this.create = false;
						this.update = false;
						this.delete = false;
					}
				}
			},
			methods: {
				printForm(url) {
					window.open(url+'/'+btoa(this.identifier), '_parent');
				},
				componentEvent(name) {
					return this[name];
				},
				isEmpty(obj) {
				    if (obj == null) return true;
					if (obj.length > 0)    return false;
				    if (obj.length === 0)  return true;
				    if (typeof obj !== 'object') return true;

				    for (var key in obj) {
				        if (hasOwnProperty.call(obj, key)) return false;
				    }

				    return true;
				},
				cancelTransaction() {
					location.replace(location.pathname);
				},
				toggleClass() {
		            var tbody = document.querySelectorAll('tr.active');
		            if(tbody && tbody.length > 0) {
		                tbody[0].classList.remove('active');
		            }
		        },
		        retrieveSessionVals(model, context) {
		        	context = (typeof context === 'undefined') ? this : context; 
		        	for(let x in model) {
		        		if(x == 'asset_image') {
		        			var mimestring, byteString;
		        			var dataURI = sessionStorage.getItem(x);
		        			if(typeof dataURI !== 'undefined' || dataURI !== '') {
			        			var byteString = atob(dataURI.split(',')[1]);
		        				mimestring = dataURI.split(',')[0].split(':')[1].split(';')[0];
							    var content = new Uint8Array(byteString.length);
							    for (var i = 0; i < byteString.length; i++) {
							        content[i] = byteString.charCodeAt(i)
							    }
							    context[x] = new Blob([content], {type: mimestring});
						    }
		        		} else {
			        		var sessionItem = sessionStorage.getItem(x);
			        		context[x]  = sessionItem;
			        		if(sessionItem != "" && (typeof model[x] == 'object' || typeof model[x] == 'array')) {
			        			context[x]  = JSON.parse(sessionItem);
			        		}
		        		}
		        	}
		        },
		        toSessionVals(modelValues, context) {
		        	context = (typeof context === 'undefined') ? this : context; 
					for(let x in modelValues) {
						sessionStorage.setItem(x, context[x]);
						if(context[x] === null) {
							sessionStorage.setItem(x, '');
						}
						if(context[x] instanceof File || context[x] instanceof Blob) {
							var reader = new FileReader();
			                reader.onload = function() {
			                	sessionStorage.setItem(x, reader.result);
					        }
				            reader.readAsDataURL(context[x]);
						}
						if(context[x] instanceof Object || context[x] instanceof Array) {
							sessionStorage.setItem(x, JSON.stringify(context[x]));
						}
					}
		        },
		        submitForm(method) {
		            if (method == 'create') {
		            	event.target.disabled = true;
		            	this.create = true;
		                this.form.post('/'+this.component+'/').then(
		                   response => { location.replace(location.pathname) }
		                )
		            } else if (method == 'save') {
		            	event.target.disabled = true;
		            	this.save = true;
		            	this.storeVals();
		            	this.form.post('/'+this.component+'/').then(
		                    response => {
		                    	sessionStorage.setItem('tagged_qty', parseInt(this.tagged_qty) + 1);
		                    	sessionStorage.setItem('loaded', true);
		                    	location.reload();
		                    }
		                )
		            } else if (method == 'update'){
		            	event.target.disabled = true;
		            	this.update = true;
		                this.form.put('/'+this.component+'/' + this.identifier).then(
		                     response => { location.replace(location.pathname) }
						)
		            } else if (method == 'delete') {
		            	event.target.disabled = true;
		            	this.delete = true;
		                this.form.delete('/'+this.component+'/' + this.identifier).then(
		                     response => { location = location.pathname; }
						)
		            }
		        },
		        showData(id) {
		            this.toggleClass();
		            this.identifier = id;
		            var row = event.target.parentNode;
		            for(let x in this.form.originalData) {
		                this.form[x] = row.children[x].innerText;
		            }
		            row.classList.add('active');
		            this.form.errors.clear();
		            this.form.disabled = true;
		        },
		        assignImage(event) {
		        	var input = event.target;
		        	this.form[input['name']] = input.files[0];
		        }, 
		        parseImage(event) {
		            var container = event.target.previousElementSibling;
		            var input = event.target;
		            if (input.files && input.files[0]) {
		                var file = input.files[0];
		                var reader = new FileReader();
		                reader.onload = function (e) {
		                    container.src = e.target.result;
		                }
		            reader.readAsDataURL(file);
		            }
		            this.form[input['name']] = input.files[0];
		        },
		        parseFiles(field) {
					let input = event.target;
					if (input.files || input.files[0]) {
						let file = input.files[0];
						this.$refs['attachment-input'].value = file.name;
						if(file.size <= 3000000) {
				    		if(file.size > 0) {
				        		file['uri'] = window.URL.createObjectURL(file);
				    			this.form[field].push(file);
							}
						} else {
							var obj = new Object();
							obj[field] = ['File exceeds maximum size required'];
							this.form.errors.errors = Object.assign({}, obj);
						}
					}
				},
		        massUpload(event) {
		            let input = event.target.files;
		            if(input && input[0]) {
		                if(input[0].type === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
		                    let byte2Mb = Math.ceil(input[0].size * Math.pow(10,-6));
		                    if(byte2Mb > 2) {
		                    	this.process_result = {template: '<div><p class="text-danger"> File size too big. </p></div>'};
		                    } else {
		                        this.process_result = {template: '<div class="mass-upload-btn"><button class="btn btn-primary" type="submit">Click here to upload</button></div>'};
		                    }
		                } else {
		                    this.process_result = {template: '<div><p class="text-danger"> Unsupported file type, please use the downloaded template. </p></div>'};
		                }
		            }
		        },
		        mimeType(mime) {
		        	let fileTypeIcon = 'fa-file-o';
		        	if(mime.indexOf('ms-excel') > 1 || mime.indexOf('spreadsheet') > 1) {
		        		fileTypeIcon = 'fa-file-excel-o';
		        	}
		        	if(mime.indexOf('ms-word') > 1 || mime.indexOf('wordprocessing') > 1) {
		        		fileTypeIcon = 'fa-file-word-o';
		        	}
		        	if(mime.indexOf('ms-powerpoint') > 1 || mime.indexOf('presentation') > 1) {
		        		fileTypeIcon = 'fa-file-powerpoint-o';
		        	}
		        	if(mime.indexOf('pdf') > 1) {
		        		fileTypeIcon = 'fa-file-pdf-o';
		        	}
		        	if(mime.indexOf('image') >= 0) {
		        		fileTypeIcon = 'fa-file-image-o';
		        	}
		        	if(mime.indexOf('video') >= 0) {
		        		fileTypeIcon = 'fa-file-video-o';
		        	}
		        	if(mime.indexOf('audio') >= 0) {
		        		fileTypeIcon = 'fa-file-audio-o';
		        	}
		        	return fileTypeIcon;
		        },
		        showModal() {
		        	this.$refs['table-modal'].style.display = 'block';
		        	this.$refs['table-modal'].style.opacity = '1';
		        },
		        lengthen() {
		        	let search = event.target.lastElementChild;
		        	search.style.width = '300px',
		        	search.style.visibility = 'visible',
		        	search.focus();
		        },
		        search() {
		        	location.search = '?search=' + event.target.value;
		        },
		        fileBuilder(encodedFile) {
		        	var byteCharacters = atob(encodedFile.data);
					var byteNumbers = new Array(byteCharacters.length);
					for (var i = 0; i < byteCharacters.length; i++) {
					    byteNumbers[i] = byteCharacters.charCodeAt(i);
					}
					var byteArray = new Uint8Array(byteNumbers);
					var file = new File([byteArray], encodedFile.file_name, {type: encodedFile.mime_type});
					file['uri'] = window.URL.createObjectURL(file);
					return file;
		        },
		        getParameterByName(name, url) {
				if (!url) {
				      url = window.location.href;
				    }
				    name = name.replace(/[\[\]]/g, "\\$&");
				    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
				        results = regex.exec(url);
				    if (!results) return null;
				    if (!results[2]) return '';
				    return decodeURIComponent(results[2].replace(/\+/g, " "));
				},
			}
		}
	}

	static mounted() {
		this.$refs[this.component].parentNode.style['height'] = window.innerHeight - this.$el.offsetTop + 'px';
		this.$el.style = { visibility: 'visible' };
		var id = this.getParameterByName('rid');
		if(id) {
			this.getSelectedRow(id);
		}
	}
}
export default Component;