import Errors from './Errors';
import axios from 'axios';
class Form {

    constructor(data) {
        this.originalData = data;

        for (let field in data) {
            this[field] = data[field];
        }
        this.isComponentEvent = false;
        this.disabled = false;
        this.alert = false;
        this.isLoading = false;
        this.errors = new Errors();
    }

    reset() {
        for (let field in this.originalData) {
            this[field] = '';
        }
        this.errors.clear();
    }

    get(url) {
        return this.submit('get', url, null, null) ;
    }

    post(url) {
        const config = {headers: {'Content-Type': 'multipart/form-data'}};
        let formData = new FormData();
        for (let property in this.originalData) {
            if(this[property] instanceof Array) {
                for(let x in this[property]) {
                    formData.append(property+'[]', this[property][x]);
                }
            } else {
                formData.append(property, this[property]);
            }
        }
        return this.submit('post', url, formData, config) ;
    }

    put(url) {
        let formData = new FormData();
        formData.append('_method', 'PUT');
        for (let property in this.originalData) {
            if(this[property] instanceof Array) {
                for(let x in this[property]) {
                    formData.append(property+'[]', this[property][x]);
                }
            } else {
                var value = (this[property] === null) ? '' : this[property];
                formData.append(property, value);
            }
        }
        return this.submit('post', url, formData, null);
    }

    delete(url) {
        return this.submit('delete', url, null, null);
    }

    submit(requestType, url, data, config) {
        this.isComponentEvent = true;
        return new Promise((resolve, reject) => {
            axios[requestType](url, data, config)
                .then(response => {
                    resolve(response.data);
                })
                .catch(error => {
                    this.onFail(error.response.data);
                    reject(error.response.data);
                });
        });
    }
    
    onFail(errors) {
        this.isComponentEvent = false;
        this.errors.record(errors);
    }
}

export default Form;