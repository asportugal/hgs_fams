var Multiselect = {
	name: 'multiselect',
	props : {
		trackBy: {
			type: String,
			default: 'id'
		},
		label: {
			type: String,
			default: 'name'
		},
		name: {
			type: String
		},
		searchable: {
			type: Boolean,
			default: false
		},
		options: {
			type: Array,
		},
		async: {
			type: Boolean,
			default: false
		},
		multiple: {
			type: Boolean,
			default: false
		},
		tabindex: {
	        type: Number,
	        default: 0
	    },
	    optionHeight: {
	    	type: Number,
	    	default: 20
	    },
	    loading: {
	    	type: Boolean,
	    	default: false
	    },
	    disabled: {
	    	type: Boolean,
	    	default: false
	    },
	    hideTags: {
	    	type: Boolean,
	    	default: false
	    },
	    inputValue: {
	    	type: String,
	    	default: null
	    },
	    closeOnSelect: {
	    	type: Boolean,
	    	default: null
	    },
		value: {
			type: null,
			default: null
		}
	},
	template: '<div :tabindex="tabindex" class="multiselect"'+
				'v-bind:class="{ \'multiselect-active\': isOpen, \'disabled\': disabled }"'+
				'@focus="disabled ? false : activate()"'+
				'@blur="searchable ? false : deactivate()"'+
				'@keydown.self.down.prevent="pointerForward()"'+
				'@keydown.self.up.prevent="pointerBackward()"'+
				'@keydown.enter.tab.stop.self="addPointer($event)"'+
				'@keyup.esc="deactivate()">'+
				'<i v-if="loading" class="multiselect-loader fa fa-spinner fa-spin"></i>'+
				'<div v-if="!loading" @mousedown.prevent.stop="toggle()" class="multiselect-select"></div>'+
				'<div class="multiselect-tags" v-bind:style="maxHeight">'+
				'<span class="multiselect-tag" ref="tags" v-if="multiple && internalValue.length >= 1" v-for="item, i in internalValue"'+
				':style="{ \'display\': hideElement(item) ? \'inline-block\' : \'none\'}">'+
				'<span v-text="item[label]"></span>'+
				'<i v-show="!disabled" aria-hidden="true" tabindex="1" @keydown.enter.prevent="removeElement(item)"'+ 
				'@mousedown.prevent="removeElement(item)" class="multiselect-tag-icon"></i>'+
				'</span>'+
				'<input v-if="searchable" type="text" ref="search"'+
				'class="multiselect-input"'+
				':value="isOpen ? search : currentOptionLabel"'+
				'@input="updateSearch($event.target.value)"'+
				'@keydown.enter.tab="addPointer($event)"'+
				'@keydown.down="pointerForward()"'+
				'@keydown.up="pointerBackward()"'+
				'@focus.prevent="activate()"'+
				'@blur.prevent="deactivate()">'+
				'<span v-else class="multiselect-single"'+
				'v-text="currentOptionLabel"></span>'+
				'</div>'+
				'<ul class="multiselect-elem-holder" v-show="isOpen" v-bind:style="maxHeight" @mousedown.prevent>'+
				'<li class="multiselect-list" v-for="item, i in filteredOptions" ref="list"'+
				'v-bind:class="{ \'selected\':isSelected(item), \'highlighted\': pointer == i}"'+
				'@mouseenter.self="setPointer(i)"'+
				'@click.stop="select(item)">'+
				'{{ item.name }}</li>'+
				'<li v-if="!search && async && options.length == 0">type to search..</li>'+
				'<li v-if="!async && searchable && options.length == 0" style="padding: 4px; background: #fff;">No records found.</li>'+
				'</ul>'+
			'</div>',
	data() {
		return {
	        isOpen: false,
	        internalValue: this.value || this.value === 0 ? this.value : this.multiple ? [] : null,
	        search: '',
	        pointer: 0
		}
	},
	watch: {
		internalValue() {
			this.adjustSearch();
	    },
		search() {
			if(this.search === this.currentOptionLabel) return;
			if(this.async) {
  				this.$emit('async-get', this.search);
  			}
		},
		value() {
			if(this.hideTags) {
				this.ignoreWatch();
			} else {
				var _this = this;
				if(this.multiple) {
					var _temp = [];
					for(var x in _this.value) {
						_temp.push(this.options.find(function(item) {
							return item[_this.trackBy] == _this.value[x];
						}));
					}
					this.internalValue = _temp;
				} else {
					this.internalValue = this.options.find(function(item) {
						return item[_this.trackBy] == _this.value;
					});
				}
			}
		}
	},
	methods: {
		ignoreWatch() {
			if(this.value.length > 1 
				&& this.internalValue.length == 0) {
				var _this = this;
				var _temp = [];
				for(let x in _this.value) {
					_temp.push(this.options.find(function(item){
						return _this.value[x] == item[_this.trackBy];		
					}));
				}
				this.internalValue = _temp;
			}
		},
		select(value) {
			if(this.multiple) {
	          if(this.isSelected(value)) {
	            this.removeElement(value);
	            return;
	          } else {
	            this.internalValue.push(value);
	          }
	        } else {
				this.internalValue = this.isSelected(value) ? null : value;
	        }

	        if(!this.hideTags) {
	        	this.$emit('input', this.valueKeys);	
	        }
	        if(!this.multiple || this.closeOnSelect) {
				this.deactivate();
			}
			this.$emit('select', this.currentValue);
		},
		pointerForward() {
			if(this.pointer < this.filteredOptions.length -1) {
				var pointer = this.pointer+1;
				this.setPointer(pointer)
			}
		},
		pointerBackward() {
			if(this.pointer > 0) {
				var pointer = this.pointer-1;
				this.setPointer(pointer)
			}
		},
		isSelected(option) {
			if (!this.internalValue) return false;
			var opt = option[this.trackBy];
			if (this.multiple) {
				return this.inputValue == opt;
			} else {
				return this.value === opt;
			}
		},
		addPointer(event) {
			var val = this.filteredOptions[this.pointer];
			this.select(val);
		},
		setHighlight(key) {
			if(Object.keys(this.internalValue).length >= 1 
				&& this.internalValue[this.trackBy] == this.filteredOptions[key][this.trackBy]) {
				return { 'selected': true };
			}
		},
		setPointer(key) {
			this.pointer = key;
		},
		activate() {
			if(this.isOpen) return;
			if(this.disabled) return;

			this.isOpen = true;
			
			if(this.searchable) {
				this.search = '';
				this.$refs.search.focus();
				if(!this.hideTags) return;
				var _this = this; 
				this.internalValue = this.internalValue.filter(function(item){
					return _this.value.indexOf(item[_this.trackBy].toString()) > -1 || item[_this.trackBy] == _this.inputValue;
				});
			} else {
				this.$el.focus();
			}
		},
		deactivate() {
			if(!this.isOpen) return;
			this.isOpen = false;
			if(this.searchable) {
				this.$refs.search.blur();
				this.adjustSearch();
			} else {
				this.$el.blur();
			}
			this.$emit('close', this.internalValue);
		},
		removeElement(option) {
	      	var index = this.valueKeys.indexOf(option[this.trackBy]);
	      	this.internalValue.splice(index, 1);
	      	this.$emit('remove', option, index);
	      	if(!this.hideTags) return;
	      	this.deactivate();
		},
		adjustSearch() {
			if (!this.searchable) return;
      		this.search = this.multiple ? '' : this.currentOptionLabel;
	    },
	    updateSearch(query) {
	    	this.search = query.toString();
	    },
	    getOptionLabel(option) {
			if (!option && option !== 0) return '';
			return option[this.label] || '';
	    },
	    hideElement(item) {
	    	if(!this.hideTags) return true;
	    	if(this.isOpen) return false;
	    	return this.inputValue == item[this.trackBy];
	    },
	    toggle() {
			this.isOpen ? this.deactivate() : this.activate();
	    }
	},
	computed: {
		valueKeys() {
			var _this = this;
			if(!this.internalValue) return;
			return this.multiple ? this.internalValue.map(function (item) {
				return item[_this.trackBy];
			}) : this.internalValue[_this.trackBy];
		},
		maxHeight() {
			return { maxHeight: (this.optionHeight * 10) + 'px' }
		},
		filteredOptions() {
			var _this = this;
			var query = this.search.toLowerCase() || '';

			var options = this.searchable ? this.options.filter(function(item) {
				var text = item[_this.label].toLowerCase();
				return text.indexOf(query) > -1;
			}) : this.options;

			if(this.multiple) {
				options = options.filter(function(item) {
					return _this.valueKeys.indexOf(item[_this.trackBy]) == -1
				});
			}
			if(this.hideTags) {
				if(_this.inputValue) {
					var append = options.find(function(item) {
						return _this.inputValue == item[_this.trackBy];
					});

					if(typeof append == 'undefined') {
						options.unshift(this.options.find(function(item){
							return _this.inputValue == item[_this.trackBy];
						}));
					}
				}
			}

			return options.slice(0);
		},
		optionKeys() {
			var _this = this;
			var options = this.options.map(function(item) {
				return item[_this.trackBy];
			});
			return options.slice(0);
	    },
		currentValue() {
			if(!this.multiple) return false;
			var values = this.internalValue.slice(0);
			return values.length > 0 ? values.pop() : false;
		},
		currentOptionLabel() {
			return this.getOptionLabel(this.internalValue) + '';
	    }
	},
	created() {
		if(this.searchable) this.adjustSearch();
	}
}

export default Multiselect;