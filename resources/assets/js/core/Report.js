import ECharts from 'vue-echarts/components/ECharts.vue';
import 'echarts/lib/chart/pie';
import 'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/title';

var Report = {
	components: {
		'echart': ECharts
	},
	props: {
		dataSet: Object
	},
	template: '<div ref="chart"><echart v-if="chartType == \'PIE\'" :style="dimension" :options="pieChart"></echart><echart v-else :style="dimension" :options="barChart"></echart></div>',
	data() {
		return {
			isLoading: false,
			chartType: 'PIE',
			filterBy: 0,
			pieChart: null,
			barChart: null,
			dimension: null 
		}
	},
	methods: {
		generatePieChart() {
			let data = [];
		    let pie_content = [];
		    let labels = [];
			for(let x in this.dataSet[this.dataKey[this.filterBy]]) {
				var d = this.dataSet[this.dataKey[this.filterBy]][x];
				labels.push(d.label);
				pie_content.push({
					value: d.asset_count,
					name: d.label
				});
			}

		    for (let i = 0; i <= 360; i++) {
		        let t = i / 180 * Math.PI
		        let r = Math.sin(2 * t) * Math.cos(2 * t)
		        data.push([r, i])
		    }

		    this.pieChart = {
		    	title : {
			        text: 'ASSET WARRANTY REPORT BY ' + this.dataKey[this.filterBy].toUpperCase(),
			        x:'center'
			    },
			    legend: {
			        orient: 'vertical',
			        left: '7%',
			        top: '10%',
			        data: labels,
			        textStyle: {
			        	fontSize: 15
			        }
			    },
				tooltip: {
					trigger: 'item',
						formatter: '{a} <br/>{b} : {c} ({d}%)'
					},
					series: [{
					name: this.dataKey[this.filterBy].toUpperCase(),
					type: 'pie',
					radius: '80%',
					center: ['50%', '55%'],
					data: pie_content,
					itemStyle: {
						emphasis: {
							shadowBlur: 10,
							shadowOffsetX: 0,
							shadowColor: 'rgba(0, 0, 0, 0.5)'
						}
					}
				}]
		    }
		},
		generateBarChart() {
			var labels = [];
			var values = [];

			for(let x in this.dataSet[this.dataKey[this.filterBy]]) {
				var d = this.dataSet[this.dataKey[this.filterBy]][x];
				labels.push(d.label);
				values.push(d.asset_count);
			}

			this.barChart = {
			    color: ['#3398DB'],
			    title : {
			        text: 'ASSET WARRANTY REPORT BY ' + this.dataKey[this.filterBy].toUpperCase(),
			        x:'center'
			    },
			    tooltip : {
			        trigger: 'axis',
			        axisPointer : {
			            type : 'shadow'
			        }
			    },
			    grid: {
			        left: '3%',
			        right: '4%',
			        bottom: '3%',
			        containLabel: true
			    },
			    yAxis : [
			        {
			            type : 'category',
			            data : labels,
			            axisTick: {
			                alignWithLabel: true
			            }
			        }
			    ],
			    xAxis : [
			        {
			            type : 'value'
			        }
			    ],
			    series : [
			        {
			            name: this.dataKey[this.filterBy].toUpperCase(),
			            type:'bar',
			            barWidth: '60%',
			            data: values
			        }
			    ]
			};
		}
	},
	watch: {
		chartType(val) {
			if(val == 'PIE') {
				this.generatePieChart();
			} else {
				this.generateBarChart();
			}
		},
		filterBy(val) {
			if(this.chartType == 'PIE') {
				this.generatePieChart();
			} else {
				this.generateBarChart();
			}
		}
	},
	computed: {
		dataKey() {
			return Object.keys(this.dataSet);
		}
	},
	created() {
		let context = this.$root.$el;
		this.dimension = {
			height: window.innerHeight - (context.lastElementChild.lastElementChild.offsetTop + 50) +'px' ,
		}
		this.generatePieChart();
	},
}
export default Report;