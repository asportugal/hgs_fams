var Attachment = {
	name: 'attachment',
	props: ['file', 'size', 'name', 'type'],
	template: '<li><i class="fa fa-fw" :class="mimeType(type)" :title="type"></i>'+
				'<a :click="parseFile(file, name, type)" class="attachment-item" :download="name" :title="name">{{ name }}</a>'+
				'</li>',
	method: {
		parseFile(data, name, type) {
        	var byteCharacters = atob(encodedFile.data);
			var byteNumbers = new Array(byteCharacters.length);
			for (var i = 0; i < byteCharacters.length; i++) {
			    byteNumbers[i] = byteCharacters.charCodeAt(i);
			}
			var byteArray = new Uint8Array(byteNumbers);
			var file = new File([byteArray], encodedFile.file_name, {type: encodedFile.mime_type});
			file['uri'] = window.URL.createObjectURL(file);
			return file;
        }
	},
}

export default Attachment;