var Dataset = {
	name: 'data-set',
	props: {
		api: String,
		export: String,
		defaultSearch: {
			type: String,
			default: ''
		},
		narrowSearch: {
			type: Boolean,
			default: false
		},
		marker: {
			type: Boolean,
			default: false
		},
		linkTo: {
			type: String,
			default: ''
		},
		hidePrimary: {
			type: Boolean,
			default: true
		},
		excludes: {
			type: Array,
			default: function() {
				return [];
			}
		},
		utils: {
			type: Array,
			default: function() {
				return [];
			}
		},
		customHeader: {
			type: Object,
			default: function() {
				return {};
			}
		},
		async: Boolean,
		info: Boolean,
		paginate: Boolean,
		primary: String
	},
	template: '<div class="dataset"><div class="utils" v-if="utils.length"><div v-if="narrowSearch">'+
			'<button class="btn btn-primary" @click="clearHeaderFields()">Clear Search</button>'+
			'<i v-show="searchLoader" class="fa fa-spinner fa-spin"></i><label v-if="searchVals.length > 0">'+
			'<br><br>Search: {{ searchVals }}</label></div><div v-if="!narrowSearch && utils.indexOf(\'search\') != -1">'+
			'<label>Search: </label><input type="search" @keyup.enter="search($event)" class="form-control">'+
			'<i v-show="searchLoader" class="fa fa-spinner fa-spin"></i></div>'+
			'<div class="pull-right" v-if="utils.indexOf(\'export\') != -1"><label>Export</label>'+
			'<div class="btn-group btn-group-sm">'+
			'<a :href="export_csv" class="btn btn-default">CSV</a>'+
			'<a :href="export_excel" class="btn btn-default">EXCEL</a>'+
			'</div></div></div>'+
			'<div class="table" v-if="!loading"><table class="table table-hover" style="overflow: auto">'+
			'<thead><tr><th v-if="marker" width="1">'+
			'<input id="checkBox" :checked="allSelected" type="checkbox" @click="massCheck($event)"></th>'+
			'<th v-else width="1">#</th><th v-for="header in headers">'+
			'<input v-if="narrowSearch" type="text" class="form-control search-field" v-model="headerFields[header]" @input="asynchNarrowSearch($event)">'+
			'<i class="fa fa-fw fa-search"></i>{{ hasDef(header) }}</th></tr></thead>'+
			'<tbody><tr v-for="(row, key, index) in rows" :key="key" :class="{active : selected == key}" v-on:click="selected = key " v-on:dblclick="rowSelected(row)">'+
			'<td v-if="marker" width="1">'+
			'<input class="chk" ref="chks" type="checkbox" v-model="checkedRows" @click="unSelectMass($event)" :value="row[primary]"></td>'+
			'<td v-else>{{ index + 1 }}</td>'+
			'<td v-for="(col, index) in headers"><a v-if="hasPrimaryLink(index)" :href="linkToUrl(row[col])">{{ row[col] }}</a>'+
			'<span v-else>{{ row[col] }}</span></td></tr>'+
			'<tr v-if="pagination.total === 0"><td :colspan="headers.length + 1" style="background: #fff">'+
			'<span>No records found</span></td></tr></tbody></table></div>'+
			'<div class="text-center" v-else><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>'+
			'<div class="footer"><ul class="pagination" v-if="paginate && (pagination.total > pagination.per_page)">'+
			'<li v-bind:class="{ \'disabled\' : pagination.current_page < 10}"><span v-if="pagination.current_page <= pagination.per_page">'+
			'&laquo;</span><span v-else @click.prevent="link(prev_list)" rel="prev">&laquo;</span></li>'+
			'<li v-bind:class="{ \'disabled\' : pagination.current_page == 1}"><span v-if="pagination.current_page == 1" rel="prev">'+
			'Previous</span><span v-else @click.prevent="link(prev)" rel="prev">Previous</span></li>'+
			'<li v-for="n in total_pages" v-bind:class="{ \'active\' : paginationLinks(n) == pagination.current_page }">'+
			'<span @click.prevent="link(\'page=\'+paginationLinks(n))">{{ paginationLinks(n) }}</span>'+
			'<li v-bind:class="{ \'disabled\' : pagination.current_page >= pagination.last_page}">'+
			'<span v-if="pagination.current_page >= pagination.last_page" rel="next">Next</span>'+
			'<span v-else @click.prevent="link(next)" rel="next">Next</span></li>'+
			'<li v-bind:class="{ \'disabled\' : total_pages < pagination.per_page}">'+
			'<span v-if="total_pages < pagination.per_page">&raquo;</span>'+
			'<span v-else @click.prevent="link(next_list)" rel="next">&raquo;</span></li></ul>'+
			'<label v-if="info && (pagination.total >= 0) " class="pull-right">Total Result: {{ pagination.total }} rows'+
			'</label></div>',
	data() {
		return {
			checkedRows: [],
			allSelected: false,
			searchLoader: false,
			loading: true,
			headerDefition: this.customHeader,
			columns: [],
			rows: {},
			exportNarrow: '',
			selected: null,
			appendSearch: '?', 
			searchValue: false,
			headerFields: {},
			searchVals: [],
			pagination: {
				total: null,
				per_page: null,
				current_page: null,
				last_page: null,
				next_page_url: null,
				prev_page_url: null,
				from: null,
				to: null
			}
		}
	},
	watch: {
        defaultSearch: {
          handler: function() {
            this.asyncRecordsFetch(this.api + '?search=' + this.defaultSearch + '&page=1');
          },
          deep: true
        },
    },
	methods: {
		clearHeaderFields() {
			this.headerFields = {};
			this.searchVals = [];
		},
		asynchNarrowSearch(event) {
			var paramsArray = [];
			this.searchVals = [];
			this.searchLoader = true;
			var searchValue = this.searchValue ? this.searchValue : '';
			for(let x in this.headerFields) {
				if(this.headerFields[x] != '') {
					paramsArray.push(x+'='+this.headerFields[x]);
					this.searchVals.push(x + ' => ' + this.headerFields[x]);
				}
			}
			this.appendSearch = '?narrow=true'+ searchValue +'&'+paramsArray.join('&');
			this.asyncRecordsFetch(this.api + this.appendSearch + '&page=1');
		},
		unSelectMass(event) {
			if(!event.target.checked) {
				this.allSelected = false;
			}
		},
		externalSearch(val) {
			this.appendSearch = '?' + val + '&';
			this.asyncRecordsFetch(this.api + this.appendSearch + 'page=1');
		},
		massCheck(val) {
			if(val.target.checked) {
				for(let x in this.$refs['chks']) {
					if(this.checkedRows.indexOf(this.$refs['chks'][x].value) == -1) {
						this.checkedRows.push(this.$refs['chks'][x].value);
					}
				}
				this.allSelected = true;
			} else {
				this.allSelected = false;
				this.checkedRows = [];
			}
		},
		paginationLinks(page) {
			var mod = (Math.floor(this.pagination.current_page) / 10) % 10;
			if(mod % 1 === 0) {
				var p = Math.floor(this.pagination.current_page) / 10;
				var offset = (p - 1) * 10;
			} else {
				var offset = Math.floor(this.pagination.current_page / 10) * 10;
			}
			return parseInt(offset) + page;
		},
		search(event) {
			this.searchValue = event.target.value;
			this.searchLoader = true;
			this.appendSearch = '?search=' + this.searchValue +'&';
			this.asyncRecordsFetch(this.api + this.appendSearch + 'page=1');
		},
		link(query) {
			if(query) {
				if(this.defaultSearch) {
					this.appendSearch = '?search=' + this.defaultSearch + '&';
				}
				if(!this.async){ 
					location.search = query 
				}
				this.asyncRecordsFetch(this.api + this.appendSearch + query)
			}
		},
		rowSelected(value) {
			if(!this.marker) {
				var url = window.location.pathname + '?rid=' + value[this.primary];
				history.replaceState({}, '', url);
				this.$emit('selected-row', value[this.primary]);
			}
		},
		asyncRecordsFetch(url) {
			this.$root.form.get(url).then(
				response => {
					if(this.$root.isEmpty(this.columns) && response.data.length > 0) {
						this.columns = Object.keys(response.data[0]);
					}
					this.rows = Object.assign({}, response.data)
					for(let x in this.pagination) {
						this.pagination[x] = response[x];
					}
					this.searchLoader = false;
				}
			)
		},
		hasDef(header) {
			if(typeof this.headerDefition[header] !== 'undefined') {
				header = this.headerDefition[header];
			}
			return header.replace(/_/g, ' ');
		},
		hasPrimaryLink(index) {
			if(index == 0) {
				if(this.linkTo != '') { return true; }
			}
		},
		linkToUrl(id) {
            return this.linkTo + '?rid=' + btoa(id);
        }
	},
	beforeMount() {
		if(this.defaultSearch) {
			this.asyncRecordsFetch(this.api + '?search=' + this.defaultSearch + '&page=1');
		} else if(this.api) {
			this.asyncRecordsFetch(this.api + location.search);
		}
	},
	updated() {
		this.loading = false;
		if(this.allSelected) {
			for(let x in this.$refs['chks']) {
				if(this.checkedRows.indexOf(this.$refs['chks'][x].value) == -1) {
					this.checkedRows.push(this.$refs['chks'][x].value);
				}
			}
		}
	},
	computed: {
		headers() {
			var headerCols = this.columns.slice(0);
			if(this.hiddenCols.length > 0 && headerCols.length > 0) {
				for(let x in this.hiddenCols) {
					var index = headerCols.indexOf(this.hiddenCols[x]);
					if(index >= 0) {
						headerCols.splice(index, 1);
					}
				}
			}
			return headerCols;
		},
		prev_list() {
			var page = (parseInt(Math.floor(this.pagination.current_page / 10) -1) * 10) + 1;
			return 'page='+ page;
		},
		next_list() {
			var page = parseInt(Math.ceil(this.pagination.current_page / 10) * 10) + 1;
			return 'page='+ page
		},
		prev() {
			var page = parseInt(this.pagination.current_page) - 1;
			return 'page='+ page;
		},
		next() {
			var page = parseInt(this.pagination.current_page) + 1;
			return 'page='+ page
		},
		total_pages() {
			var offset = Math.ceil(this.pagination.current_page / 10) * 10;
			if(offset > parseInt(this.pagination.last_page)) {
				return parseInt(this.pagination.last_page) % parseInt(this.pagination.per_page);
			}
			return this.pagination.per_page;
		},
		export_csv() {
			return this.export + '/csv' + this.withSearch;
		},
		export_excel() {
			return this.export + '/excel' + this.withSearch;
		},
		withSearch() {
			var search = '';
			if(this.defaultSearch) {
				search = '?search=' + this.defaultSearch;
			} else if(this.narrowSearch) {
				search = this.appendSearch == '?' ? '' : this.appendSearch;
			} else {
				if(this.searchValue) {
					search = '?search=' + this.searchValue;
				}
			}
			return search;
		},
		hiddenCols() {
			var columns = this.excludes; 
			if(this.hidePrimary) {
				columns.push(this.primary);
			}
			return columns;
		},
	}
}

export default Dataset;