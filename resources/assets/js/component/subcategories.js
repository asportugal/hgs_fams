import Form from '../core/Form';
import Dataset from '../core/Dataset';
import ComboBox from '../core/ComboBox';

var subcategories = {
	components: { 
		'dataset': Dataset,
		'combobox': ComboBox
	},
	data: {
        component: 'subcategories',
        headerDefs: {'name': 'Subcategory Name', 'code': 'Category Code'},
        identifier: false,
		form: new Form({
			subcategory_name: '',
			category: ''
		}),
	},
	methods: {
		getSelectedRow(val) {
			this.identifier = val.category_id;
			this.form.subcategory_name = val.name;
			this.form.category = val.code;
			this.form.disabled = true;
		}
	}
}
export default subcategories;