import Form from '../core/Form';
import ComboBox from '../core/ComboBox';
import Dataset from '../core/Dataset';
import DatePicker from '../core/DatePicker';

var print_assets = {
	components: {
		'combobox': ComboBox,
		'dataset': Dataset,
		'datepicker': DatePicker
	},
	data: {
        component: 'print-assets',
        identifier: false,
        toggle: false,
        toFront: true,
        printer_name: '',
		form: new Form({
			printer_name: '',
			asset_id: '',
			amr_no: '',
			gr_no: '',
			serial_no: '',
			description: '',
			category: '',
			subcategory: '',
			lob_owner: '',
			bldg: '',
			floor: '',
			cube: '',
			employee: '',
			brand: '',
			model: '',
			group: '',
			invoice_no: '',
			delivery_no: '',
			delivery_receipt_date: '',
			supplier: '',
			notes: '',
			status: '',
		}),
	},
	methods: {	
		print() { 
			var allSelected = this.$refs['records'].allSelected;
			if(allSelected) {
				var query = ['all=true'].concat(this.formValues());
				this.form.get('/print-assets/initialize/'+btoa(this.printer_name)+'/?'+query.join('&'));
			} else {
				var parameters = [];
				var assets = this.$refs['records'].checkedRows;
				for(let x in assets) {
					parameters.push('asset_id[]='+assets[x]);
				}
				this.form.get('/print-assets/initialize/'+btoa(this.printer_name)+'?'+parameters.join('&'));
			}
		},
		formValues() {
			var query = [];
			for(let x in this.form.originalData) {
				if(this.form[x] != '') {
					query.push(x+'='+this.form[x]);
				}				
			}
			return query;
		},
		searchAssets() {
			var query = this.formValues();
			if(query.length > 0) {
				this.$refs['records'].externalSearch(query.join('&'));
			}
			this.toggleForm();
		},
		toggleForm() {
			var timer;
			this.toggle = this.toggle ? false : true;
			if(this.toggle === false) {
				this.$refs['form'].style.top = '-63%';
				this.$refs['form'].style.zIndex = '-1';
				var show = vm => {
					vm.toFront = true;
					vm.$refs['records'].$el.style.visibility = 'visible';
					clearTimeout(timer);
				}
				timer = setTimeout(show, 400, this);
			} else {
				this.toFront = false;
				this.$refs['form'].style.top = '0';
				this.$refs['records'].$el.style.visibility = 'hidden';
				var show = vm => {
					vm.$refs['form'].style.zIndex = '1';
					clearTimeout(timer);
				}
				timer = setTimeout(show, 300, this);
			}
		},
		getSelectedRow(val) {
			this.form.printer_name = val.printer_name;
			this.form.disabled = true;
		}
	},
	computed: {
		printerStatus() {
			if(this.printer_status == 'FAILED') {
				return '#a94442'; 
			} else if (this.printer_status == 'SUCCESS') {
				return '#3c763d';
			}
		},
		testMsg() {
			if(this.printer_status == 'FAILED') {
				return 'Couldn\'t connect to printer ['+this.printer_name+']';
			} else if (this.printer_status == 'SUCCESS') {
				return 'Connection succesful!'
			}
		},
		printDisabled() {
			return (this.printer_name == '')
 		}
	}
}
export default print_assets;