import Form from '../core/Form';
import Modal from '../core/Modal';
import Dataset from '../core/Dataset';
import ComboBox from '../core/ComboBox';
import DatePicker from '../core/DatePicker';
import Multiselect from 'vue-multiselect';

var location_transfer = {
	components: {
		Multiselect,
		'combobox': ComboBox,
		'modal': Modal,
		'dataset': Dataset,
		'datepicker': DatePicker,
	},
	data: {
		component: 'location-transfer',
		identifier: false,
		approvers: '',
		actionLoader: false,
		isLoading: false,
		status: '',
		asset_info: {},
		approved_date: '',
		approved_by: '',
		form: new Form({
			transfer_status: 'NEW',
			to_site: '',
			to_floor: '',
			to_cube: '',
			asset_image: '',
			transfer_approver: '',
			transfer_approver_group: '',
			transfer_remarks: '',
			itaf_no: '',
			asset_id: ''
		}),
	},
	watch: {
		approvers() {
			if(this.approvers) {
				this.form.transfer_approver = this.approvers.approver;
				this.form.transfer_approver_group = this.approvers.group_name;
			}
		},
	},
    methods: {
    	isApprover(username) {
    		return (this.form.transfer_approver == username)
    	},
    	actionRequest(status) {
    		this.actionLoader = status;
    		this.form.transfer_status = status;
    		this.form.put('/transfer-asset/'+this.identifier).then(
    			response => {
    				location.reload();
    			}
    		)
    	},
		getSelectedRow(val) {
			this.form.errors.errors = {};
			this.form.reset();
			this.getAssetInfo(val.asset_id);
			this.getAssetImage(val.asset_id);
			this.identifier = val.location_transfer_id;
			this.setSelectValue('approvers', val.transfer_approver_group);
			this.form.transfer_status = val.transfer_status;
			this.form.to_site = val.to_site;
			this.form.to_floor = val.to_floor;
			this.form.to_cube = val.to_cube;
			this.form.approver = val.approver;
			this.form.asset_image = val.asset_image;
			this.form.transfer_approver_group = val.transfer_approver_group;
			this.status = val.transfer_status;
			this.form.itaf_no = val.itaf_no;
			this.form.asset_id = val.asset_id;			
			this.approved_date = val.approved_date;
			this.approved_by = val.approved_by;
			this.form.transfer_remarks = val.transfer_remarks;
			this.form.disabled = true;	
			this.$refs.modal.show = false;
		},
		getAssetImage(asset_id) {
            this.form.asset_image = '';
            this.form.get('/attachments/asset_image/'+asset_id).then(
                response => {
                	if(response[0]) {
                    	this.form.asset_image = this.fileBuilder(response[0]);
                    }
                }
            )
        },
    	setSelectValue(select, value) {
			var key = this.$refs[select].optionKeys.indexOf(value.toLowerCase())
			this[select] = this.$refs[select].options[key];
		},
		showAssetInfo(response) {
			var assets = ['asset_id', 'serial_no', 'amr_no', 'gr_no', 'status', 'brand', 'model', 
			'description', 'category', 'subcategory', 'bldg', 'supplier', 'lob_owner', 'purchase_order_no', 
			'delivery_receipt_no', 'sales_invoice_no'];
			this.asset_info = {};
			for(let x in assets) {
				var key = assets[x];
				var value = response ? response[key] : '';
				this.asset_info[key] = value;
			}
		},
		getAssetInfo(id) {
			this.form.get('/open-records/'+btoa(id)).then(
				response => { 
					this.form.asset_id = response['asset_id']; 
					this.showAssetInfo(response);
				}
			)
		},
		getLocationTransferInfo(id) {
			this.form.get('/location-transfer/'+id).then(
				response => {
					this.getAssetInfo(response.asset_id); 
					this.getAssetImage(response.asset_id);
					this.identifier = response.location_transfer_id;
					this.setSelectValue('approvers', response.transfer_approver_group);
					this.form.transfer_status = response.transfer_status;
					this.form.to_site = response.to_site;
					this.form.to_floor = response.to_floor;
					this.form.to_cube = response.to_cube;
					this.form.transfer_remarks = response.transfer_remarks;
					this.form.itaf_no = response.itaf_no;
					this.form.asset_id = response.asset_id;
					this.status = response.transfer_status;
					this.form.disabled = true;	
				}
			)
		}
    },
    computed: {
		image_src() {
			var image = '/img/new-img.png';
			if(this.form.asset_image) {
				image = URL.createObjectURL(this.form.asset_image);
			}
			return image;
		}
    },
    beforeMount() {
    	this.showAssetInfo();
    	var ref_id = this.getParameterByName('ref');
    	var transfer_id = this.getParameterByName('id');
    	if(ref_id) { this.getAssetInfo(atob(ref_id)); this.getAssetImage(atob(ref_id)); } 
    	if (transfer_id) { this.getLocationTransferInfo(transfer_id) }
    }
}
export default location_transfer; 