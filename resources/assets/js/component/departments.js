import Form from '../core/Form';
import Dataset from '../core/Dataset';
var departments = {
	components: {
		'dataset': Dataset
	},
	data: {
        component: 'departments',
        identifier: false,
		form: new Form({
				department_name: ''
			}),
	},
	methods: {
		getSelectedRow(val) {
			this.identifier = val.department_id;
			this.form.department_name = val.name;
			this.form.disabled = true;
		}
	}
}
export default departments;

