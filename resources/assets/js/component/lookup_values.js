import Form from '../core/Form';
import Dataset from '../core/Dataset';
import Multiselect from 'vue-multiselect';

var lookup_values = {
	components: {
		Multiselect,
		'dataset': Dataset
	},
	data: {
        component: 'lookup-values',
        identifier: false,
		form: new Form({
			tag: '',
			name: '',
		}),
	},
	methods: {
		getSelectedRow(val) {
			this.identifier = val.lookup_id;
			this.form.tag = val.tag;
			this.form.name = val.name;
			this.form.disabled = true;
		}
	}
}
export default lookup_values;