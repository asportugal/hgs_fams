import Form from '../core/Form';
import Modal from '../core/Modal';
import DatePicker from '../core/DatePicker';
import Dataset from '../core/Dataset';
import ComboBox from '../core/ComboBox';

var employees = {
    components: { 
        'combobox': ComboBox,
        'modal': Modal,
        'dataset': Dataset,
        'datepicker': DatePicker
    },
    data: {
        component: 'employees',
        identifier: false,
        process_result: '',
        selected: '',
        upload_mass: false,
        togglables: ['Accountable Assets', 'Loaned Assets'],
        form: new Form({
            employee_no: '',
            first_name: '',
            middle_name: '',
            last_name: '',
            work_phone: '',
            email: '',
            employee_status: '',
            department: '',
            department_head: '',
            supervisor: '',
            date_hired: '',
            employee_image: ''
        }),
    },
    methods: {
        toggleTab() {
            var list = document.querySelectorAll('li.active');
            if(list && list.length > 0) {
                list[0].classList.remove('active');
            }
        },
        getSelectedRow(val) {
            this.form.errors.errors = {};
            this.identifier = val.employee_id;
            this.form.disabled = true;
            this.$refs.modal.show = false;
            this.getEmpImage();

            for(let x in val) {
                this.form[x] = val[x];
            }
        },
        getEmpImage(callback) {
            this.form.employee_image = '';
            this.form.get('/attachments/employee_image/'+this.identifier).then(
                response => {
                    if(response[0]) {
                        this.form.employee_image = this.fileBuilder(response[0]);
                    }
                }
            )
            this.imageLoading = false;
        },
    },
    computed: {
        empImage() {
            var image = '/img/default-user.png';
            if(this.form.employee_image != '') {
                image = window.URL.createObjectURL(this.form.employee_image);
            }
            return image;
        },
    },
    beforeMount() {
        var emp_id = this.getParameterByName('eid')
        if(emp_id) {
            this.form.get('/search-employee?eid='+emp_id).then(
                response => { this.getSelectedRow(response) }
            )
        } else {
            this.upload_mass = true;
        }
    }
}
export default employees;