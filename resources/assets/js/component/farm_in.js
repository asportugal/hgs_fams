import Form from '../core/Form';
import Dataset from '../core/Dataset';
import ComboBox from '../core/ComboBox';
import DatePicker from '../core/DatePicker';
import Modal from '../core/Modal';

var farm_in = {
	components: {
		'combobox': ComboBox,
		'dataset': Dataset,
		'modal': Modal,
		'datepicker': DatePicker,
	},
	data: {
        component: 'farm-in',
        identifier: false,
        line_items: 1,
        line_items_status: [],
		form: new Form({
			purchase_order: '',
			peza_permit_status: '',
			peza_form_no: '',
			peza_approval_no: '',
			date_filed: '',
			location: '',
			vendor: '',
			description: [''],
			quantity: [''],
			proforma_invoice_no: '',
			proforma_invoice_date: '',
			final_invoice_no: '',
			final_invoice_date: '',
			currency: 'PHP',
			total_amount: '',
			request_date: '',
			remarks: '',
			farm_in_attachments: []
		}),
	},
	methods: {
		matchQty(id) {
			console.log(id);
			if(typeof this.form.quantity[id] === 'undefined') {
				this.form.quantity[id] = '';
			}
		},
		addLineItem() {
			this.form.description.push('');
			this.form.quantity.push('');
			this.line_items++;
		},
		removeLineItem(index) {
			this.form.description.splice(index, 1);
			this.form.quantity.splice(index, 1);
			this.line_items_status.splice(index, 1);
			this.line_items--;
		},
		getSelectedRow(val) {
			this.form.errors.errors = {};
			this.$refs.modal.show = false;
			this.form.reset();
			this.identifier = val.farm_in_id;
			this.getFileAttachments()
			this.form.purchase_order = val.purchase_order;
			this.form.peza_permit_status = val.peza_permit_status;
			this.form.peza_form_no = val.peza_form_no;
			this.form.peza_approval_no = val.peza_approval_no;
			this.form.date_filed = val.date_filed;
			this.form.location = val.location;
			this.form.vendor = val.vendor;
			this.form.proforma_invoice_no = val.proforma_invoice_no;
			this.form.proforma_invoice_date = val.proforma_invoice_date;
			this.form.final_invoice_no = val.final_invoice_no;
			this.form.final_invoice_date = val.final_invoice_date;
			this.form.currency = val.currency;
			this.form.total_amount = val.total_amount;
			this.form.request_date = val.request_date;
			this.form.remarks = val.remarks;
			this.line_items = val.line_items.length;
			this.assignLineItems(val.line_items);
			this.form.disabled = true;
		},
		assignLineItems(val) {
			this.form.description = [];
			this.form.quantity = [];
			for(var i=0; i<this.line_items; i++) {
				this.line_items_status.push(val[i].done);
				this.form.description.push(val[i].description);
				this.form.quantity.push(val[i].quantity);
			}
		},
		getFileAttachments() {
			this.form.farm_in_attachments = [];
			this.form.get('/attachments/farm_in_attachments/'+this.identifier).then(
				response => {
					for(let x in response){
						this.form.farm_in_attachments.push(this.fileBuilder(response[x]));
					}
				}
			)
		},
	}
}
export default farm_in;