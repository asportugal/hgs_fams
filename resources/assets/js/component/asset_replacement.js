import Form from '../core/Form';
import Modal from '../core/Modal';
import Dataset from '../core/Dataset';
import Multiselect from 'vue-multiselect';
import DatePicker from '../core/DatePicker';

var asset_replacement = {
	components: {
		Multiselect,
		'modal': Modal,
		'dataset': Dataset,
		'datepicker': DatePicker,
	},
	data: {
		component: 'asset-replacement',
		identifier: false,
		approvers: '',
		actionLoader: false,
		isLoading: false,
		status: '',
		asset_info: {},
		approved_date: '',
		approved_by: '',
		form: new Form({
			replacement_status: 'NEW',
			replacement_approver: '',
			replacement_remarks: '',
			asset_image: '',
			serial_no: '',
			replacement_approver_group: '',
			itaf_no: '',
			asset_id: ''
		}),
	},
	watch: {
		approvers() {
			if(this.approvers) {
				this.form.replacement_approver = this.approvers.approver;
				this.form.replacement_approver_group = this.approvers.group_name;
			}
		},
	},
    methods: {
    	isApprover(username) {
    		return (this.form.replacement_approver == username)
    	},
    	actionRequest(status) {
    		this.actionLoader = status;
    		this.form.replacement_status = status;
    		this.form.put('/replace-asset/'+this.identifier).then(
    			response => {
    				location.reload();
    			}
    		)
    	},
		getSelectedRow(val) {
			this.form.errors.errors = {};
			this.form.reset();
			this.getAssetInfo(val.asset_id);
			this.getAssetImage(val.asset_id);
			this.identifier = val.replacement_id;
			this.setSelectValue('approvers', val.replacement_approver_group);
			this.form.replacement_status = val.replacement_status;
			this.form.serial_no = val.serial_no;
			this.form.replacement_approver = val.replacement_approver;
			this.form.replacement_remarks = val.replacement_remarks;
			this.form.asset_image = val.asset_image;
			this.status = val.replacement_status;
			this.form.replacement_approver_group = val.replacement_approver_group;
			this.form.itaf_no = val.itaf_no;
			this.form.asset_id = val.asset_id;			
			this.approved_date = val.approved_date;
			this.approved_by = val.approved_by;
			this.form.disabled = true;	
			this.$refs.modal.show = false;
		},
		getAssetImage(asset_id) {
            this.form.asset_image = '';
            this.form.get('/attachments/asset_image/'+asset_id).then(
                response => {
                	if(response[0]) {
                    	this.form.asset_image = this.fileBuilder(response[0]);
                    }
                }
            )
        },
    	setSelectValue(select, value) {
			var key = this.$refs[select].optionKeys.indexOf(value.toLowerCase())
			this[select] = this.$refs[select].options[key];
		},
		showAssetInfo(response) {
			var assets = ['asset_id', 'serial_no', 'amr_no', 'gr_no', 'status', 'brand', 'model', 
			'description', 'category', 'subcategory', 'bldg', 'supplier', 'lob_owner', 'purchase_order_no', 
			'delivery_receipt_no', 'sales_invoice_no'];
			this.asset_info = {};
			for(let x in assets) {
				var key = assets[x];
				var value = response ? response[key] : '';
				this.asset_info[key] = value;
			}
		},
		getAssetInfo(id) {
			this.form.get('/open-records/'+btoa(id)).then(
				response => { 
					this.form.asset_id = response['asset_id']; 
					this.showAssetInfo(response);
				}
			)
		},
		getAssetReplacementInfo(id) {
			this.form.get('/asset-replacement/'+id).then(
				response => {
					this.getAssetInfo(response.asset_id); 
					this.getAssetImage(response.asset_id);
					this.identifier = response.replacement_id;
					this.setSelectValue('approvers', response.replacement_approver_group);
					this.form.replacement_status = response.replacement_status;
					this.form.serial_no = response.serial_no;
					this.form.replacement_remarks = response.replacement_remarks;
					this.form.itaf_no = response.itaf_no;
					this.form.asset_id = response.asset_id;
					this.status = response.replacement_status;
					this.form.disabled = true;	
				}
			)
		}
    },
    computed: {
    	isSerialNo() {
    		if(!this.form.disabled) {
    			if(this.form.replacement_status != 'APPROVED') {
    				return false;
    			}
    		}
    		return true;
    	},
		image_src() {
			var image = '/img/new-img.png';
			if(this.form.asset_image) {
				image = URL.createObjectURL(this.form.asset_image);
			}
			return image;
		}
    },
    beforeMount() {
    	this.showAssetInfo();
    	var ref_id = this.getParameterByName('ref');
    	var replacement_id = this.getParameterByName('id');
    	if(ref_id) { this.getAssetInfo(atob(ref_id)); this.getAssetImage(atob(ref_id)); } 
    	if (replacement_id) { this.getAssetReplacementInfo(replacement_id) }
    }
}
export default asset_replacement; 