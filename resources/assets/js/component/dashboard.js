import Form from '../core/Form';
import Dataset from '../core/Dataset';
var dashboard = {
	components: {
		'dataset': Dataset
	},
	data: {
        component: 'dashboard',
		form: new Form({
			brand_name: '',
		}),
	},
	methods: {
		getSelectedRow(val) {
			this.identifier = val.brand_id;
			this.form.brand_name = val.name;
			this.form.disabled = true;
		}
	}
}
export default dashboard;