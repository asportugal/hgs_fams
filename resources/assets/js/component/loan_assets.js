import Form from '../core/Form';
import Modal from '../core/Modal';
import Dataset from '../core/Dataset';
import Multiselect from 'vue-multiselect';
import DatePicker from '../core/DatePicker';

var loan_assets = {
	components: {
		Multiselect,
		'modal': Modal,
		'dataset': Dataset,
		'datepicker': DatePicker,
	},
	data: {
		component: 'loan-assets',
		identifier: false,
		employees: [],
		categories: '',
		subcategories: [],
		approvers: '',
		actionLoader: false,
		isLoading: false,
		status: '',
		asset_info: {},
		approved_date: '',
		approved_by: '',
		form: new Form({
			loan_status: 'NEW',
			approver: '',
			requestor: '',
			start_date: '',
			end_date: '',
			remarks: '',
			checked_out_by: '',
			checked_out_date: '',
			checked_in_by: '',
			checked_in_date: '',
			checked_in_remarks: '',
			asset_image: '',
			approver_group: '',
			itaf_no: '',
			asset_id: ''
		}),
	},
	watch: {
		categories() {
			if(this.categories) {
				this.form.category = '';
				this.form.category = this.categories.name;
				this.form.get('/subcategories/name/'+this.categories.code).then(
					response => {
						this.subcategories = [];
						for(let x in response) {
							this.subcategories.push(response[x].name)
						}
					}
				)
			}
		},
		employee() {
			if(this.employee) {
				this.form.requestor = this.employee.first_name + ' ' + this.employee.middle_name + ' ' + this.employee.last_name;
			}
		},
		approvers() {
			if(this.approvers) {
				this.form.approver = this.approvers.approver;
				this.form.approver_group = this.approvers.group_name;
			}
		},
	},
    methods: {
    	isApprover(username) {
    		return (this.form.approver == username)
    	},
    	actionRequest(status) {
    		this.actionLoader = status;
    		this.form.loan_status = status;
    		this.form.put('/asset-loan/'+this.identifier).then(
    			response => {
    				location.reload();
    			}
    		)
    	},
    	asyncFindEmployee(query) {
			this.isLoading = true;
			this.form.get('/search-employees?q='+query)
				.then(
					response => { 
						this.employees = response
						this.isLoading = false
					}
				)
		},
		getSelectedRow(val) {
			this.form.errors.errors = {};
			this.form.reset();
			this.getAssetInfo(val.asset_id);
			this.getAssetImage(val.asset_id);
			this.identifier = val.loan_id;
			this.setSelectValue('approvers', val.loan_approver_group);
			this.form.loan_status = val.loan_status;
			this.form.approver = val.loan_approver;
			this.form.requestor = val.loan_to;
			this.approved_date = val.approved_date;
			this.approved_by = val.approved_by;
			this.form.checked_out_by = val.checked_out_by;
			this.form.checked_out_date = val.checked_out_date;
			this.form.checked_in_by = val.checked_in_by;
			this.form.checked_in_date = val.checked_in_date;
			this.form.checked_in_remarks = val.checked_in_remarks;
			this.form.start_date = val.loan_start_date;
			this.form.end_date = val.loan_end_date;
			this.form.remarks = val.loan_remarks;
			this.form.loan_status = val.loan_status;
			this.form.itaf_no = val.itaf_no;
			this.form.disabled = true;	
			this.$refs.modal.show = false;
			this.status = val.loan_status;
		},
		getAssetImage(asset_id) {
            this.form.asset_image = '';
            this.form.get('/attachments/asset_image/'+asset_id).then(
                response => {
                	if(response[0]) {
                    	this.form.asset_image = this.fileBuilder(response[0]);
                    }
                }
            )
        },
    	setSelectValue(select, value) {
			var key = this.$refs[select].optionKeys.indexOf(value.toLowerCase())
			this[select] = this.$refs[select].options[key];
		},
		showAssetInfo(response) {
			var assets = ['asset_id', 'serial_no', 'amr_no', 'gr_no', 'status', 'brand', 'model', 
			'description', 'category', 'subcategory', 'bldg', 'supplier', 'lob_owner', 'purchase_order_no', 
			'delivery_receipt_no', 'sales_invoice_no'];
			this.asset_info = {};
			for(let x in assets) {
				var key = assets[x];
				var value = response ? response[key] : '';
				this.asset_info[key] = value;
			}
		},
		getAssetInfo(id) {
			this.form.get('/open-records/'+btoa(id)).then(
				response => { 
					this.form.asset_id = response['asset_id']; 
					this.showAssetInfo(response);
				}
			)
		},
		getAssetLoanInfo(id) {
			this.form.get('/loan-assets/'+id).then(
				response => {
					this.getAssetInfo(response.asset_id); 
					this.getAssetImage(response.asset_id);
					this.identifier = response.loan_id;
					this.setSelectValue('approvers', response.loan_approver_group);
					this.form.requestor = response.loan_to;
					this.form.start_date = response.loan_start_date;
					this.form.end_date = response.loan_end_date;
					this.form.remarks = response.loan_remarks;
					this.form.checked_out_by = response.checked_out_by;
					this.form.checked_out_date = response.checked_out_date;
					this.form.checked_in_by = response.checked_in_by;
					this.form.checked_in_date = response.checked_in_date;
					this.form.checked_in_remarks = response.checked_in_remarks;
					this.form.itaf_no = response.itaf_no;
					this.form.loan_status = response.loan_status;
					this.status = response.loan_status;
					this.form.disabled = true;	
				}
			)
		}
    },
    computed: {
    	isCheckOut() {
    		if(!this.form.disabled) {
    			if(this.form.loan_status != 'APPROVED' || this.form.checked_out_date) {
    				return true;
    			} else {
    				return false;
    			}
    		}
    		return true;
    	},
    	isCheckIn() {
    		if(!this.form.disabled) {
    			if(this.form.checked_out_date && !this.form.checked_in_date) {
    				return false;
    			}
    		}
    		return true;
    	},
		image_src() {
			var image = '/img/new-img.png';
			if(this.form.asset_image) {
				image = URL.createObjectURL(this.form.asset_image);
			}
			return image;
		}
    },
    beforeMount() {
    	this.showAssetInfo();
    	var ref_id = this.getParameterByName('ref');
    	var loan_id = this.getParameterByName('id');
    	if(ref_id) { this.getAssetInfo(atob(ref_id)); this.getAssetImage(atob(ref_id)); } 
    	if (loan_id) { this.getAssetLoanInfo(loan_id); }
    }
}

export default loan_assets; 