import Form from '../core/Form';
import Dataset from '../core/Dataset';
import ComboBox from '../core/ComboBox';
import DatePicker from '../core/DatePicker';
import Modal from '../core/Modal';

var farm_out = {
	components: {
		'combobox': ComboBox,
		'dataset': Dataset,
		'modal': Modal,
		'datepicker': DatePicker,
	},
	data: {
        component: 'farm-out',
        identifier: false,
		form: new Form({
			control_no: '',
			peza_permit_status: '',
			peza_form_no: '',
			peza_approval_no: '',
			date_filed: '',
			location: '',
			vendor: '',
			proforma_invoice_no: '',
			proforma_invoice_date: '',
			final_invoice_no: '',
			final_invoice_date: '',
			request_date: '',
			remarks: '',
			farm_out_attachments: []
		}),
	},
	watch: {
	},
	methods: {
		getSelectedRow(val) {
			this.form.errors.errors = {};
			this.$refs.modal.show = false;
			this.form.reset();
			this.identifier = val.farm_out_id;
			this.getFileAttachments()
			this.form.control_no = val.control_no;
			this.form.peza_permit_status = val.peza_permit_status;
			this.form.peza_form_no = val.peza_form_no;
			this.form.peza_approval_no = val.peza_approval_no;
			this.form.date_filed = val.date_filed;
			this.form.location = val.location;
			this.form.vendor = val.vendor;
			this.form.proforma_invoice_no = val.proforma_invoice_no;
			this.form.proforma_invoice_date = val.proforma_invoice_date;
			this.form.final_invoice_no = val.final_invoice_no;
			this.form.final_invoice_date = val.final_invoice_date;
			this.form.request_date = val.request_date;
			this.form.remarks = val.remarks;
			this.form.disabled = true;
		},
		getFileAttachments() {
			this.form.farm_out_attachments = [];
			this.form.get('/attachments/farm_out_attachments/'+this.identifier).then(
				response => {
					for(let x in response){
						this.form.farm_out_attachments.push(this.fileBuilder(response[x]));
					}
				}
			)
		},
	}
}
export default farm_out;