import Form from '../core/Form';
import Dataset from '../core/Dataset';
var notifications = {
	components: {
		'dataset': Dataset
	},
	data: {
        component: 'notifications',
        identifier: false,
        to: '',
        cc: '',
		form: new Form({
				group_name: '',
				to_address: [],
				cc_address: []
			}),
	},
	watch: {
		to(val) {
			if(val.split(';').length > 9) {
				this.form.errors.errors['to_address.0'] = ['Only maximum of 9 emails is allowed'];
			} else {
				this.del_error('to_address');
				this.form.to_address = val.split(';');
			}
		},
		cc(val) {
			if(val.split(';').length > 9) {
				this.form.errors.errors['cc_address.0'] = ['Only maximum of 9 emails is allowed'];
			} else {
				this.del_error('cc_address');
				this.form.cc_address = val.split(';');
			}
		}
	},
	methods: {
		del_error(field) {
			console.log(this.form[field].length);
			for(var i=0; i<this.form[field].length; i++) {
				var address = field+'.'+i;
				console.log(address);
				if(this.form.errors.has(address)) {
					delete this.form.errors.errors[address];
				}
			}
		},
		has_error(field) {
			for(let x in this.form[field]) {
				var address = field+'.'+x;
				if(this.form.errors.has(address)) {
					return true;
					break;
				}
			}
			return false;
		},
		get_error(field) {
			var errors = '';
			for(let x in this.form[field]) {
				var address = field+'.'+x;
				if(this.form.errors.get(address)) {
					errors += this.form.errors.get(address) + ' ';
				}
			}
			return errors;
		},
		splitValues(field) {
			this.form[field] = [];
			if(this[field].indexOf(';') > 0) {
				if(this[field].split(';') > 9) {
				} else {
					var email_array = this[field].split(';');
					for(var i=0; i<email_array.length; i++) {
						if(email_array[i] !== '') {
							this.form[field].push(email_array[i]);	
						}	
					}
				}
			} else {
				this.form[field].push(this.carbon);
			}
		},
		getSelectedRow(val) {
			this.identifier = val.notification_id;
			this.form.group_name = val.group_name;			
			this.to = val.to_address;
			this.cc = val.cc_address;
			this.form.disabled = true;
		}
	}
}
export default notifications;