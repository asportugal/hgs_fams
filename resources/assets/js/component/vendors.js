import Form from '../core/Form';
import Dataset from '../core/Dataset';
var vendors = {
	components: {
		'dataset': Dataset
	},
	data: {
        component: 'vendors',
        headerDefs: {'name': 'Supplier Name', 'address': 'Supplier Address'},
        identifier: false,
		form: new Form({
			supplier_name: '',
			contact_person: '',
			contact_number: '',
			supplier_address: ''
		}),
	},
	methods: {
		getSelectedRow(val) {
			this.identifier = val.vendor_id;
			this.form.supplier_name = val.name;
			this.form.contact_person = val.contact_person;
			this.form.contact_number = val.contact_no;
			this.form.supplier_address = val.address;
			this.form.disabled = true;
		}
	}
}
export default vendors;