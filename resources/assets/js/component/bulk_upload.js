import Form from '../core/Form';
import Multiselect from 'vue-multiselect';
import XLSX from 'xlsx';

var bulk_upload = {
	components: { 
		Multiselect 
	},
	data: {
		component: 'bulk-upload',
		upload_type: '',
		file: '',
		loader: false,
		form: new Form({
			process: '',
			file: '',
			comment: ''
		})
	},
	methods: {
		bulk() {
			this.$refs['bulk-upload'].submit();
		},
		getUploadType() {
			return this.upload_type ? this.upload_type.type : '';
		},
		removeContents() {
			var table = this.$refs['exceltable'];
			while(table.firstChild) {
				table.removeChild(table.firstChild);
			}
		},
		parseExcel() {
			this.removeContents();
			this.loader = true;
			var input = event.target.files;
			if(input[0].type !== 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
				this.form.errors.record({file: ['Invalid file selected']});
			} else {
				this.file = input[0]
				var reader = new FileReader();
	    		var status, workbook, sheetName;
	    		var vm = this;
				reader.onload = function (e) {
		    		var data = e.target.result;
		    		workbook = XLSX.read(data, {type: 'binary'});
		    		sheetName = workbook.SheetNames[0];
			    }
			    reader.onloadend = function (e) {
			    	var excel = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName], {defval: ""});
			    	var table = vm.$refs['exceltable'];
			    	var header = table.createTHead();
			    	var body = document.createElement('tbody');
					table.appendChild(vm.generateHeaders(header, Object.keys(excel[0])));
					table.appendChild(vm.generateBody(body, excel));
					vm.loader = false;
			    }
			    reader.readAsBinaryString(this.file);
			}
		},
		generateHeaders(header, cols) {
			var row = header.insertRow(0);
			for(let x in cols) {
				if(x == 0) { row.appendChild(this.generateCell('&nbsp;&nbsp;&nbsp;', 'th')); }
				row.appendChild(this.generateCell(cols[x], 'th'));
			}
			return row;
		},
		generateBody(body, excel) {
			for(let x in excel) {
				var row = body.insertRow(x);
				row.appendChild(this.generateCell(parseInt(x) + 1, 'td'));
				for(let y in excel[x]) {
					row.appendChild(this.generateCell(excel[x][y], 'td'));
				}
			}
			return body;
		},
		generateCell(val, cell) {
			var tableCell = document.createElement(cell);
			tableCell.innerHTML = val;
			return tableCell;
		}
	},
	computed: {
		template() {
			var template = '';
			if(this.upload_type.type == 'create') {
				template = 'ASSETS_UPLOADER_TEMPLATE (NEW).xlsx'; 
			} else if(this.upload_type.type == 'update') {
				template = 'ASSETS_UPLOADER_TEMPLATE (UPDATE).xlsx';
			}
			return template;
		},
		bulkDisable() {
			return (!this.loader && this.file) && this.upload_type ? false : true;
		}
	}
}

export default bulk_upload;