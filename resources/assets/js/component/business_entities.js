import Form from '../core/Form';
import Dataset from '../core/Dataset';

var business_entities = {
	components: {
		'dataset': Dataset
	},
	data: {
        component: 'business-entities',
        identifier: false,
		form: new Form({
			lob: '',
		}),
	},
	methods: {
		getSelectedRow(val) {
			this.identifier = val.business_entity_id;
			this.form.lob = val.name;
			this.form.disabled = true;
		}
	}
}
export default business_entities;