import Form from '../core/Form';
import Dataset from '../core/Dataset';
var brands = {
	components: {
		'dataset': Dataset
	},
	data: {
        component: 'brands',
        identifier: false,
		form: new Form({
			brand_name: '',
		}),
	},
	methods: {
		getSelectedRow(val) {
			this.identifier = val.brand_id;
			this.form.brand_name = val.name;
			this.form.disabled = true;
		}
	}
}
export default brands;