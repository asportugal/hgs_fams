import Form from '../core/Form';
import Modal from '../core/Modal';
import Dataset from '../core/Dataset';
import Multiselect from 'vue-multiselect';
import DatePicker from '../core/DatePicker';

var open_records = {
	components: { 
		Multiselect,
		'modal': Modal,
		'dataset': Dataset,
		'datepicker': DatePicker,
		'notification': Notification
	},
	data: {
		component: 'open-records',
		identifier: false,
		categories: '',
		employee: '',
		employees: [],
		subcategories: [],
		quantity: '',
		tagged_qty: '',
		isLoading: false,
		serialDisabled: true,
		selected: '',
		history: '',
		togglables: ['General', 'Financial', 'Peza', 'Maintenance Agreement', 'Contract Archive', 'History'],
		delivery_attachments: [],
		form: new Form({
			description: '',
			category: '',
			subcategory: '',
			lob_owner: '',
			bldg: '',
			floor: '',
			cube: '',
			employee: '',
			supplier: '',
			brand: '',
			model: '',
			asset_id: '',
			amr_no: '',
			serial_no: '',
			status: '',
			delivery_receipt_date: '',
			tag_date: '',
			warranty_start_date: '',
			warranty_end_date: '',
			notes: '',
			delivery_receipt_no: '',
			purchase_order_date: '',
			purchase_order_no: '',
			service_date: '',
			sales_invoice_no: '',
			purchase_price_dollar: '',
			purchase_price_php: '',
			financial_treatment: '',
			local_cost_center: '',
			date_fully_depreciated: '',
			exchange_rate_php_dollar: '',
			asset_life: '',
			remaining_asset_life: '',
			monthly_depreciation: '',
			maintenance_vendor: '',
			maintenance_start_date: '',
			maintenance_end_date: '',
			maintenance_remark: '',
			maintenance_email_notify: '',
			contract_type: '',
			contract_start_date: '',
			contract_end_date: '',
			contract_remarks: '',
			asset_image: ''
		})
	},
	watch: {
		categories() {
			if(this.categories) {
				this.form.category = '';
				this.form.category = this.categories.name;
				this.form.get('/subcategories/name/'+this.categories.code).then(
					response => {
						this.subcategories = [];
						for(let x in response) {
							this.subcategories.push(response[x].name)
						}
					}
				)
			}
		},
	},
	methods: {
		notEmpty(val) {
			if(val == '' || !val) {
				return 'null'; 
			}
			return val;
		},
		toggleTab() {
            var list = document.querySelectorAll('li.active');
            if(list && list.length > 0) {
                list[0].classList.remove('active');
            }
        },
		asyncFindEmployee(query) {
			this.isLoading = true;
			this.form.get('/search-employees?q='+query)
				.then(
					response => { 
						this.employees = response;
						this.isLoading = false;
					}
				)
		},
		getSelectedRow(val) {
			this.form.errors.errors = {};
			this.$refs.modal.show = false;
			this.form.disabled = true;
			this.serialDisabled = true;
			this.form.reset();
			this.identifier = val.asset_id;
			this.getHistory(val.asset_id);
			this.setSelectValue('categories', val.category);
			this.getAssetImage();
			this.delivery_attachments = [];
			if(val.pre_asset) {
				this.getDeliveryAttachments(val.pre_asset.delivery_id);
			}
			if(val.serial_no == 'NONE') {
				this.serialDisabled = false;
			}
			this.form.serial_no = val.serial_no;
			this.form.description = val.description;
			this.form.subcategory = val.subcategory;
			this.form.lob_owner = val.lob_owner;
			this.form.bldg = val.bldg;
			this.form.floor = val.floor;
			this.form.cube = val.cube;
			this.form.employee = val.employee;
			this.form.supplier = val.supplier;
			this.form.brand = val.brand;
			this.form.model = val.model;
			this.form.asset_id = val.asset_id;
			this.form.amr_no = val.amr_no;
			this.form.status = val.status;
			this.form.delivery_receipt_date = val.delivery_receipt_date;
			this.form.tag_date = val.tag_date;
			this.form.warranty_start_date = val.warranty_start_date;
			this.form.warranty_end_date = val.warranty_end_date;
			this.form.delivery_receipt_no = val.delivery_receipt_no;
			this.form.purchase_order_date = val.purchase_order_date;
			this.form.purchase_order_no = val.purchase_order_no;
			this.form.service_date = val.service_date;
			this.form.sales_invoice_no = val.sales_invoice_no;
			this.form.purchase_price_dollar = val.purchase_price_dollar;
			this.form.purchase_price_php = val.purchase_price_php;
			this.form.financial_treatment = val.financial_treatment;
			this.form.local_cost_center = val.local_cost_center;
			this.form.date_fully_depreciated = val.date_fully_depreciated;
			this.form.asset_life = val.asset_life;
			this.form.remaining_asset_life = val.remaining_asset_life;
			this.form.monthly_depreciation = val.monthly_depreciation;
			this.form.maintenance_vendor = val.maintenance_vendor;
			this.form.maintenance_start_date = val.maintenance_start_date;
			this.form.maintenance_end_date = val.maintenance_end_date;
			this.form.maintenance_remark = val.maintenance_remark;
			this.form.maintenance_email_notify = val.maintenance_email_notify;
			this.form.contract_type = val.contract_type;
			this.form.contract_start_date = val.contract_start_date;
			this.form.contract_end_date = val.contract_end_date;
			this.form.contract_remarks = val.contract_remarks;
			this.form.notes = val.notes;
		},
		setSelectValue(select, value) {
			var key = this.$refs[select].optionKeys.indexOf(value.toLowerCase())
			this[select] = this.$refs[select].options[key];
		},
		getAssetImage() {
            this.form.asset_image = '';
            this.form.get('/attachments/asset_image/'+this.identifier).then(
                response => {
                	if(response[0]) {
                    	this.form.asset_image = this.fileBuilder(response[0]);
                    }
                }
            )
        },
        getHistory(id) {
        	if(id != '') {
        		this.history = [];
        		this.form.get('/history/'+id).then(
        			response => {
        				this.history = response;
        			}
        		)
        	}
        },
        getDeliveryAttachments(id) {
        	if(id != '') {
	        	this.delivery_attachments = [];
	            this.form.get('/attachments/delivery_attachments/'+id).then(
	                response => {
	                    for(let x in response){
							this.delivery_attachments.push(this.fileBuilder(response[x]));
						}
	                }
	            )
            }
        },
        transaction_url(link) {
        	var url = (!this.identifier) ? '#' : link + '?ref=' + btoa(this.identifier);
			return url;
		},
		link_to_employee(url) { 
			return url +'?eid=' + btoa(this.form.employee); 
		},
	},
	computed: {
		image_src() {
			var image = '/img/new-img.png';
			if(this.form.asset_image) {
				image = URL.createObjectURL(this.form.asset_image)
			}
			return image;
		}
	},
	beforeMount() {
		this.form.disabled = true;
	}
}
export default open_records;