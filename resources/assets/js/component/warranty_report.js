import Form from '../core/Form';
import Report from '../core/Report';
import Multiselect from '../core/Multiselect';

var warranty_report = {
	components: {
		'multiselect': Multiselect,
		'report': Report
	},
	data: {
        component: 'warranty_report',
        identifier: false,
        chart_type: 'PIE',
        filter_by: 0,
        isLoading: false,
        form: new Form()
	},
	watch: {
		chart_type(value) {
			if(value !== null) {
				this.$refs['report'].chartType = value;
			}
		},
		filter_by(value) {
			if(value !== null) {
				this.$refs['report'].filterBy = value;
			}
		}
	}
}
export default warranty_report;