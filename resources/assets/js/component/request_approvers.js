import Form from '../core/Form';
import Dataset from '../core/Dataset';
import Multiselect from 'vue-multiselect';

var request_approvers = {
	components: {
		Multiselect,
		'dataset': Dataset
	},
	data: {
        component: 'request-approvers',
        identifier: false,
        users: [],
        user: '',
        isLoading: false,
		form: new Form({
			group_name: '',
			approver: ''						
		}),
	},
	watch: {
		user() {
			if(this.user) {
				this.form.approver = '';
				this.form.approver = this.user.username;
			}
		}
	},
	methods: {
		asyncFindUsers(query) {
			this.isLoading = true;
			this.form.get('/search-users?q='+query)
				.then(
					response => { 
						this.users = response
						this.isLoading = false
					}
				)
		},
		getSelectedRow(val) {
			this.identifier = val.approver_id;
			this.form.group_name = val.group_name;
			this.form.approver = val.approver;
			this.form.disabled = true;
		},
		customLabel ({ full_name }) {
	      return `${full_name}`
	    },
	}
}
export default request_approvers;