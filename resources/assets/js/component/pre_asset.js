import Form from '../core/Form';
import Modal from '../core/Modal';
import Dataset from '../core/Dataset';
import ComboBox from '../core/ComboBox';
import DatePicker from '../core/DatePicker';

var pre_asset = {
	components: { 
		'combobox': ComboBox,
		'modal': Modal,
		'dataset': Dataset,
		'datepicker': DatePicker
	},
	data: {
        component: 'pre-asset',
		identifier: false,
		from_pending: false,
		description_items: [],
		reset_description: false,
		form: new Form({
			airway_delivery_no: '',
			cargo: '',
			consignee_name: '',
			airway_delivery_date: '',
			shipper: '',
			vendor_name: '',
			vendor_delivery_date: '',
			bldg: '',
			description: [],
			serial_no: '',
			received_by: '',
			vendor_purchase_order_no: '',
			vendor_delivery_no: '',
			delivery_attachments: [],
			tagged_quantity: '',
			notification: ''
		}),
	},
	methods: {
		setDescription(value) {
			this.form.description = value;
		},
		getPurchaseOrders(query) {
			this.$refs['pos'].isLoading = true;
			this.$root.form.get('/search-pos?q='+query)
			.then( response => {
				var respObj = { options: response, default: null };
				if(response.length > 1) {
					console.log('true');
					this.$refs['pos'].setSelectOptions(respObj);	
				} else {
					if(this.identifier) {
						respObj.options = [query];
						respObj.default = function(vm, d) {
							vm.$children[0].internalValue = d; 
						}
					}
					this.$refs['pos'].setSelectOptions(respObj) 
				};
				
				this.$refs['pos'].isLoading = false; 
			});
		},
		getFarmIn(value) {
			this.form.get('/po-details/'+value).then(
				response => {
					this.form.description = [];
					this.from_pending = true;
					this.form.vendor_name = response.vendor;
					this.form.bldg = response.location;
					this.getLineItems(value);
				}
			)
		},
		getLineItems(value) {
			this.form.get('/line-items/'+value).then(
				response => {
					this.description_items = [];
					this.description_items = response
				}
			)				
		},
		getFileAttachments() {
			this.form.delivery_attachments = [];
			this.form.get('/attachments/delivery_attachments/'+this.identifier).then(
				response => {
					for(let x in response){
						this.form.delivery_attachments.push(this.fileBuilder(response[x]));
					}
				}
			)
		},
		getSelectedRow(value) {
			this.$refs.modal.show = false;
			this.form.errors.reset();
			this.form.disabled = true;
			this.identifier = value.delivery_id;
			this.getFileAttachments();
			this.getLineItems(value.vendor_purchase_order_no);
			this.getPurchaseOrders(value.vendor_purchase_order_no);
			this.$refs['line_items'].internalValue = this.form.description;
			this.form.vendor_purchase_order_no = value.vendor_purchase_order_no;
			this.form.airway_delivery_no = value.airway_delivery_no;
			this.form.cargo = value.cargo;
			this.form.consignee_name = value.consignee_name;
			this.form.airway_delivery_date = value.airway_delivery_date;
			this.form.shipper = value.shipper;
			this.form.vendor_name = value.vendor_name;
			this.form.vendor_delivery_date = value.vendor_delivery_date;
			this.form.bldg = value.bldg;
			this.form.serial_no = value.serial_no;
			this.form.received_by = value.received_by;
			this.form.vendor_delivery_no = value.vendor_delivery_no;
			this.form.notification = value.notification;
			for(let x in value.farm_in_items) {
				this.form.description.push(value.farm_in_items[x].description);
			}
		},
	},
	computed: {
		noEdit() {
			var disabled = false;
			if(this.identifier || this.form.disabled) {
				disabled = true;	
			}
			return disabled;
		}
	}
}


export default pre_asset;