import Form from '../core/Form';
var request = {
	data: {
        component: 'request',
        identifier: false,
        form: new Form({})
	},
	methods: {
		action(action, id, user) {
			var q = '?id='+id+'&action='+action+'&approver='+user;
			this.form.get('/request?aid='+btoa(q)).then(
				response => {
					console.log(response)
				}
			);
		},
	}
}
export default request;