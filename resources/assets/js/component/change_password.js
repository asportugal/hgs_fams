import Form from '../core/Form';
var change_password = {
	data: {
        component: 'change-password',
        identifier: false,
		form: new Form({
			brand_name: '',
		}),
	},
	methods: {
		getSelectedRow(val) {
			this.identifier = val.brand_id;
			this.form.brand_name = val.name;
			this.form.disabled = true;
		}
	}
}
export default brands;