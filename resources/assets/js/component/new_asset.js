import Form from '../core/Form';
import Modal from '../core/Modal';
import Dataset from '../core/Dataset';
import ComboBox from '../core/ComboBox';
import DatePicker from '../core/DatePicker';
import XLSX from 'xlsx';

var new_asset = {
	components: { 
		'combobox': ComboBox,
		'modal': Modal,
		'dataset': Dataset,
		'datepicker': DatePicker,
		'notification': Notification
	},
	data: {
		component: 'new-asset',
		identifier: false,
		subcategories: [],
		//file_name: '',
		quantity: '',
		tagged_qty: '',
		asset_id: false,
		ccode: '',
		scode: '',
		//upload_bulk: false,
		//mass_upload: '',
		form: new Form({
			//bulk_serial: '',
			asset_id: '',
			gr_no: '',
			amr_no: '',
			description: '',
			category: '',
			subcategory: '',
			serial_no: '',
			bldg: '',
			floor: '',
			cube: '',
			employee: '',
			lob_owner: '',
			brand: '',
			model: '',
			peza_transaction_type: '',
			farm_in_date: '',
			peza_in_form_no: '',
			peza_in_permit_no: '',
			vat_paid: '',
			financial_treatment: '',
			delivery_receipt_date: '',
			delivery_receipt_no: '',
			tag_date: '',
			sales_invoice_no: '',
			purchase_order_no: '',
			purchase_order_date: '',
			supplier: '',
			imported: true,
			import_permit_no: '',
			pbo_no: '',
			import_cewe_no: '',
			import_boat_note: '',
			import_boc_cert: '',
			import_ntc_permit_no: '',
			asset_image: ''
		})
	},
	watch: {
		/*upload_bulk(value) {
			if(!value) {
				this.form.bulk_serial = '';
				this.mass_upload = '';
				this.file_name = '';
			} else {
				this.form.serial_no = '';
			}
		},
		'form.bulk_serial'(value) {
			this.file_name = value.name;
		},*/
		'form.category'(value) {
			if(value) {
				var categoriesElem = this.$refs['categories'];
				this.ccode = categoriesElem.options[categoriesElem.selectValues.indexOf(value)].code;
				this.assetID();
				this.form.get('/subcategories/name/'+this.ccode).then(
					response => {
						this.subcategories = [];
						for(let x in response) {
							this.subcategories.push(response[x].name)
						}
					}
				)
			}
			
		},
		'form.delivery_receipt_date'() {
			this.assetID();
		},
		'form.bldg'(value) {
			var sitesElem = this.$refs['sites'];
			this.scode = sitesElem.options[sitesElem.selectValues.indexOf(value)].code;
			this.assetID();
		},
		tagged_qty(val) {
			if(val == parseInt(this.quantity)) {
        		location.reload();
        	}
		}
	},
	methods: {
		parseCSV(event) {
			this.form.bulk_serial = event.target.files[0];
			this.form.errors.clear('bulk_serial');
			this.form.errors.clear('serial_no');
			var reader = new FileReader();
    		var status, workbook, sheetName;
    		var vm = this;
			reader.onload = function (e) {
	    		var data = e.target.result;
	    		workbook = XLSX.read(data, {type: 'binary'});
	    		sheetName = workbook.SheetNames[0];
		    }
		    reader.onloadend = function (e) {
		    	var excel = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName], {defval: ""});
		    	if(excel.length > vm.quantity) {
		    		vm.mass_upload = 'Serial No quantity must not exceed current qty';
		    	}
				vm.loader = false;
		    }
		    reader.readAsBinaryString(this.form.bulk_serial);
		},
		assetID() {
			if((this.ccode && this.scode) && this.form.delivery_receipt_date) {
				this.form.errors.clear('asset_id');
				var fragment = this.ccode+'|'+this.scode+'|'+this.form.delivery_receipt_date;
				this.form.get('/asset/'+btoa(fragment))
					.then(
						response => { 
							this.form.asset_id = response;
							this.asset_id = response;
						}
					)
			}
			return this.form.asset_id;
		},
		getSelectedRow(value) {
			history.replaceState({}, '', location.pathname);
			this.form.errors.errors = {};
			this.$refs.modal.show = false;
			this.form.reset();
			this.form.description = value.description;
			this.form.serial_no = value.serial_no;
			this.form.delivery_receipt_date = value.vendor_delivery_date;
			this.form.tag_date = value.tag_date;
			this.form.delivery_receipt_no = value.vendor_delivery_no;
			this.form.purchase_order_no = value.vendor_purchase_order_no;
			this.form.supplier = value.vendor_name;	
			this.form.bldg = value.bldg;
			this.tagged_qty = value.tagged_qty;
			this.quantity = value.quantity;
			if(value.farm_in) {
				this.form.farm_in_date = value.farm_in.date_filed;
				this.form.sales_invoice_no = value.farm_in.final_invoice_no;
				this.form.purchase_order_date = value.farm_in.request_date;
				this.form.peza_in_form_no = value.farm_in.peza_form_no;
				this.form.peza_in_permit_no = value.farm_in.peza_approval_no;
			}
		},
		storeVals() {
			if(this.asset_id) {
		        this.toSessionVals(this.form.originalData, this.form);
		        this.toSessionVals({quantity: '', tagged_qty: '', subcategories: ''});
	        }
	    }
	},
	computed: {
		tagged_asset() {
			return this.tagged_qty + ' / ' + this.quantity;
		},
		image_src() {
			var image = '/img/new-img.png';
			if(this.form.asset_image) {
				image = URL.createObjectURL(this.form.asset_image)
			}
			return image;
		},
		disableSave() {
			// return  this.mass_upload != '' || (this.form.errors.any() || !this.asset_id);
			return !this.asset_id || this.form.errors.any();
		}
	},
	beforeUpdate() {	
		if(sessionStorage.getItem('loaded')) {
			this.retrieveSessionVals(this.form.originalData, this.form);
			this.retrieveSessionVals({quantity: '', tagged_qty: '', subcategories: []});
			this.form['serial_no'] = '';
			sessionStorage.clear();
			this.$refs['serial_no'].focus();
		}
	}
}
export default new_asset;