import Form from '../core/Form';
import Modal from '../core/Modal';
import Dataset from '../core/Dataset';
import Multiselect from 'vue-multiselect';
import DatePicker from '../core/DatePicker';

var asset_disposal = {
	components: {
		Multiselect,
		'modal': Modal,
		'dataset': Dataset,
		'datepicker': DatePicker,
	},
	data: {
		component: 'asset-disposal',
		identifier: false,
		approvers: '',
		actionLoader: false,
		isLoading: false,
		status: '',
		asset_info: {},
		approved_date: '',
		approved_by: '',
		form: new Form({
			disposal_status: 'NEW',
			disposal_method: '',
			disposal_vendor: '',
			disposal_approver: '',
			disposal_remarks: '',
			asset_image: '',
			disposal_approver_group: '',
			itaf_no: '',
			asset_id: ''
		}),
	},
	watch: {
		approvers() {
			if(this.approvers) {
				this.form.disposal_approver = this.approvers.approver;
				this.form.disposal_approver_group = this.approvers.group_name;
			}
		},
	},
    methods: {
    	isApprover(username) {
    		return (this.form.disposal_approver == username)
    	},
    	actionRequest(status) {
    		this.actionLoader = status;
    		this.form.disposal_status = status;
    		this.form.put('/dispose-asset/'+this.identifier).then(
    			response => {
    				location.reload();
    			}
    		)
    	},
		getSelectedRow(val) {
			this.form.errors.errors = {};
			this.form.reset();
			this.getAssetInfo(val.asset_id);
			this.getAssetImage(val.asset_id);
			this.identifier = val.disposal_id;
			this.setSelectValue('approvers', val.disposal_approver_group);
			this.form.disposal_status = val.disposal_status;
			this.form.disposal_method = val.disposal_method;
			this.form.disposal_vendor = val.disposal_vendor;
			this.form.disposal_approver = val.disposal_approver;
			this.form.disposal_remarks = val.disposal_remarks;
			this.form.asset_image = val.asset_image;
			this.form.disposal_approver_group = val.disposal_approver_group;
			this.form.itaf_no = val.itaf_no;
			this.form.asset_id = val.asset_id;			
			this.approved_date = val.approved_date;
			this.approved_by = val.approved_by;
			this.form.disposal_remarks = val.disposal_remarks;
			this.status = val.disposal_status;
			this.form.disabled = true;	
			this.$refs.modal.show = false;
		},
		getAssetImage(asset_id) {
            this.form.asset_image = '';
            this.form.get('/attachments/asset_image/'+asset_id).then(
                response => {
                	if(response[0]) {
                    	this.form.asset_image = this.fileBuilder(response[0]);
                    }
                }
            )
        },
    	setSelectValue(select, value) {
			var key = this.$refs[select].optionKeys.indexOf(value.toLowerCase())
			this[select] = this.$refs[select].options[key];
		},
		showAssetInfo(response) {
			var assets = ['asset_id', 'serial_no', 'amr_no', 'gr_no', 'status', 'brand', 'model', 
			'description', 'category', 'subcategory', 'bldg', 'supplier', 'lob_owner', 'purchase_order_no', 
			'delivery_receipt_no', 'sales_invoice_no'];
			this.asset_info = {};
			for(let x in assets) {
				var key = assets[x];
				var value = response ? response[key] : '';
				this.asset_info[key] = value;
			}
		},
		getAssetInfo(id) {
			this.form.get('/open-records/'+btoa(id)).then(
				response => { 
					this.form.asset_id = response['asset_id']; 
					this.showAssetInfo(response);
				}
			)
		},
		getAssetDisposalInfo(id) {
			this.form.get('/asset-disposal/'+id).then(
				response => {
					this.getAssetInfo(response.asset_id); 
					this.getAssetImage(response.asset_id);
					this.identifier = response.disposal_id;
					this.setSelectValue('approvers', response.disposal_approver_group);
					this.form.disposal_status = response.disposal_status;
					this.form.disposal_method = response.disposal_method;
					this.form.disposal_vendor = response.disposal_vendor;
					this.form.disposal_remarks = response.disposal_remarks;
					this.form.itaf_no = response.itaf_no;
					this.form.asset_id = response.asset_id;
					this.status = response.disposal_status;
					this.form.disabled = true;	
				}
			)
		}
    },
    computed: {
		image_src() {
			var image = '/img/new-img.png';
			if(this.form.asset_image) {
				image = URL.createObjectURL(this.form.asset_image);
			}
			return image;
		}
    },
    beforeMount() {
    	this.showAssetInfo();
    	var ref_id = this.getParameterByName('ref');
    	var disposal_id = this.getParameterByName('id');
    	if(ref_id) { this.getAssetInfo(atob(ref_id)); this.getAssetImage(atob(ref_id)); } 
    	if (disposal_id) { this.getAssetDisposalInfo(disposal_id); }
    }
}
export default asset_disposal; 