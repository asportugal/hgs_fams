import Form from '../core/Form';
import Dataset from '../core/Dataset';
import Modal from '../core/Modal';
import Multiselect from '../core/Multiselect';

var user_management = {
	components: { 
		'multiselect':  Multiselect,
		'dataset': Dataset,
		'modal': Modal
	},
	data: {
		component: 'user-management',
        identifier: false,
        isDisabled: true,
        enabled: false,
        menus: null,
        transactions: null,
        current: null,
		form: new Form({
			role: null,
			group: null,
			scope: [],
			modules: [],
			permission: [],
			first_name: null,
			middle_name: null,
			last_name: null,
			email: null,
			username: null
		})
	},
	methods: {
		parseOption(menus) {
			if(this.menus === null) {
				this.menus = menus;
			}
			return this.menus;
		},
		parsePermission(permission) {
			var tempObject = new Object();
			var transactions = permission.split('|');
			for(let x in transactions) {
				tempObject[transactions[x]] = false;
			}
			return tempObject;
		},
		getSelected(event) {
			if(this.current != event.id) {
				this.current = event.id.toString();
			}
			this.transactions = this.parsePermission(event.transactions);
		},
		addUserPrivilege() {
			var transactions = [];
			for(let x in this.transactions) {
				var z = '';
				if(this.transactions[x]) {
					z = x;
				}
				transactions.push(z);
			}

			this.form.permission.push(transactions.join('|'));
			this.form.modules.push(this.current);
			this.transactions = null;
			this.current = null;
		},
		setActivation(event) {
			event.target.children[0].className = 'fa fa-spinner fa-spin';
			let formData = new FormData();
        	formData.append('_method', 'PUT');
        	formData.append('enabled', event.target.value);
			this.form.submit('post', '/user-management/set-activation/'+this.identifier, formData).then(
					response => { location.reload() }
				);
		},
		resetPassword(event) {
			event.target.children[0].className = 'fa fa-spinner fa-spin';
			let formData = new FormData();
        	formData.append('_method', 'PUT');
			this.form.submit('put', '/user-management/reset-password/'+this.identifier, formData).then(
					response => { location.reload() }
				);
		},
		setGroup(event) {
			var value = event.target.value;
			if(!event.target.checked) {
				if(this.form.group == '0') {
					for(let x in this.$refs.groups.children) {
						var child = this.$refs.groups.children[x];
						if(child.tagName == 'LABEL') {
							 if(child.firstChild.checked) {
							 	this.form.group = child.firstChild.value;
							 }
						}
					}
				} else {
					this.form.group = null;
				}
			} else {
				if(this.form.group == null) {
					this.form.group = value;
				} else {
					this.form.group = '0'
				}
			}
		},
		displaySelected(index) {

			if(this.form.modules.length > this.form.permission.length) {
				this.form.modules.pop();
			}

			var current = this.form.modules.splice(index, 1)[0];
			var transactions = this.form.permission.splice(index, 1)[0];

			var menus = this.menus.find(function(item) {
				return item.id == current;
			});

			var permission = {};
			var a = menus.transactions.split('|');
			var b = transactions.split('|');
			for(let x in a) {
				permission[a[x]] = (b[x]) !== '' ? true : false;
			}

			this.current = current;
			this.transactions = permission;

		},
		defaultApprover(event) {
			console.log('meh');
		},
		getRemoved(event) {
			var index = this.form.modules.indexOf(event.id);
			if(index > - 1) {
				this.form.modules.splice(index, 1);
				this.form.permission.splice(index, 1);
			}
			this.transactions = null;
			this.current = null;
		},
		getSelectedRow(id) {
			this.form.get('/user-management/'+id).then(
				response => {
					this.identifier 		= response.id;
					this.form.role 			= response.role;
					this.form.group 		= response.group;
					this.form.first_name 	= response.full_name.split("|")[0];
					this.form.middle_name 	= response.full_name.split("|")[1];
					this.form.last_name 	= response.full_name.split("|")[2];
					this.form.email 		= response.email;
					this.form.username 		= response.username;
					this.enabled 			= response.enabled;
					this.form.scope = [];
					for(let x in response.site_access) {
						this.form.scope.push(response.site_access[x].site_id);
					}

					this.form.modules = [];
					this.form.permission = [];
					for(let x in response.user_permissions) {
						this.form.modules.push(response.user_permissions[x].menu_id);
						this.form.permission.push(response.user_permissions[x].permission);
					}		
				}
			)

			this.form.disabled = true;
			this.noUpdate = true;
			this.$refs.modal.show = false;
		}
	},
	computed: {
		// filterOptions() {
		// 	var modules = this.form.modules;
		// 	return this.menus;
		// },
		accessPrivilege() {
			var access = [];
			var modules = this.form.modules;
			var menus = this.menus;
			var ctr = 1;

			for(let x in modules) {
				var menu = menus.find(function(item) {
					return item.id == modules[x];
				});

				if(ctr > this.form.permission.length) {
					break;
				}

				ctr++;
				var permission = [];
				var a = menu.transactions.split('|');
				var b = this.form.permission[x].split('|');
				for(let y in a) {
					if(b[y] !== '') {
						permission.push(a[y]);
					}
				}

				var temp_obj = {
					'name': menu.name,
					'permission': permission.join(', ')
				}
				access.push(temp_obj);
			}
			return access;
		}
	}
}
export default user_management;