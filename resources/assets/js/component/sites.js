import Form from '../core/Form';
import Dataset from '../core/Dataset';
var sites = {
	components: {
		'dataset': Dataset
	},
	data: {
        component: 'sites',
        identifier: false,
		headerDefs: {'name': 'Site Name', 'code': 'Site Code', 'address': 'Address'},
		form: new Form({
			site_name: '',
			site_code: '',
			address: '',
		}),
	},
	methods: {
		getSelectedRow(val) {
			this.identifier = val.site_id;
			this.form.site_name = val.name;			
			this.form.site_code = val.code;
			this.form.address = val.address;
			this.form.disabled = true;
		},
	}
}
export default sites;