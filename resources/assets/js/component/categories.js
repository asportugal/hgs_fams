import Form from '../core/Form';
import Dataset from '../core/Dataset';
var categories = {
	components: {
		'dataset': Dataset,
	},
	data: {
        component: 'categories',
        headerDefs: {'name': 'Category Name', 'code': 'Category Code'},
        identifier: false,
		form: new Form({
				category_name: '',
				category_code: '',
			}),
	},
	methods: {
		getSelectedRow(val) {
			this.identifier = val.category_id;
			this.form.category_name = val.name;
			this.form.category_code = val.code;
			this.form.disabled = true;
		}
	}
}
export default categories;