import Vue from 'vue';
import Component from './core/Component';
Component.load('#app').then(
	component => {
		new Vue(component);
	}
)