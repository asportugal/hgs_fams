<?php

namespace App\Traits;
use Auth;
trait Searchable
{
    public function scopeSearch($query, $fullText = null) {
    	$searchableFields = array_merge($this->getFillable(), $this->getGuarded());
        foreach($searchableFields as $property) {
            $query->orWhere($property, 'like', '%'.$fullText.'%');
        }
        return $query;
    }

    public function scopeNarrowSearch($query, $parameters) {
		foreach($parameters as $key => $val) {
            $query->where($key, 'like', '%'.$val.'%');
        }
        return $query;
    }
}