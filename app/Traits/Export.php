<?php

namespace App\Traits;
use Auth;
use Maatwebsite\Excel\Facades\Excel;

trait Export
{
    public static function csv($fileName, $model) {
        ini_set('memory_limit', '-1');
        return Excel::create($fileName, function($excel) use ($model) {
            $excel->sheet('Sheet1', function($sheet) use ($model) {
                $sheet->fromModel($model);
                $sheet->row(1, function($row) {
                    $row->setFontWeight('bold');
                });
            });
        })->download('csv');
	}

    public static function excel($fileName, $model) {
        ini_set('memory_limit', '-1');
        return Excel::create($fileName, function($excel) use ($model) {
            $excel->sheet('Sheet1', function($sheet) use ($model) {
               $sheet->fromModel($model);
               $sheet->row(1, function($row) {
                    $row->setFontWeight('bold');
                });
            });
        })->download('xlsx');
    }
}