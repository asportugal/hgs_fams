<?php

namespace App\Traits;
use Auth;
trait FileHelper
{
	
	public function saveAttachments($ref_id, $attachment_type, $files) {
		foreach($files as $file)
        {
            \App\Attachment::create([
                'reference_id' => $ref_id,
                'attachment_type' => $attachment_type,
                'file_name' => $file->getClientOriginalName(), 
                'file_size' => $file->getSize(),
                'mime_type' => $file->getMimeType(),
                'data' => $file
            ]);
        }		
	}	
}