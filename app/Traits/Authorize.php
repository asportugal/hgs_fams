<?php

namespace App\Traits;
use Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\ApprovalRequest;
trait Authorize
{
	public static function isValidRole() {
        return !in_array(Auth::user()->role, ['ADMINISTRATOR', 'APPROVER']) ? true : false;
	}
    public static function createRequest($properties, $content) {
		$request = \App\Approval::create([
            'content' => json_encode($content),
            'reference_id' => $properties['reference_id'],
            'entity' => $properties['model'],
            'status' => 'PENDING',
            'requested_by' => Auth::user()->full_name,
            'operation' => strtoupper($properties['operation']),
            'approver' => Auth::user()->approver
        ]);
         $approver = \App\User::where('username', Auth::user()->getOriginal('approver'))->first();
         Mail::to($approver->email)
            ->cc(Auth::user()->email)
            ->send(new ApprovalRequest($request));
    }
}