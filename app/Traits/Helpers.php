<?php

namespace App\Traits;
use Auth;
trait Helpers
{
	public static function hasDateValue($value) {
		if(strtotime($value)){
            return date('Y-m-d h:i:s.000', strtotime($value));
        }
	}

    public static function fileParser($files) {
    	if(is_array($files)) {
    		$parsedFile = [];
    		$counter = 0;
    		foreach($files as $file) {
    			$content = $file->openFile()->fread($file->getSize());
    			$parsedFile[$counter] = [
	                'file_name' => $file->getClientOriginalName(), 
	                'file_size' => $file->getSize(),
	                'mime_type' => $file->getMimeType(),
	    			'data' => base64_encode($content)
    			];
    			$counter++;
    		}
    	} else {
    		$parsedFile = '';
			$content = $file->openFile()->fread($file->getSize());
			$parsedFile[$counter] = [
                'file_name' => $file->getClientOriginalName(), 
                'file_size' => $file->getSize(),
                'mime_type' => $file->getMimeType(),
    			'data' => base64_encode($content)
			];
    	}
        
        return $parsedFile;
    }
    
}