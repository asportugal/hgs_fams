<?php
namespace App\Traits;

use App\Models\History;
use Auth;

trait Responsible
{
    public static function bootResponsible()
    {
        if(!empty(Auth::user()))
        {
            $user = strtoupper(Auth::user()->username);
            static::creating(function ($model) use ($user) {
                $model->created_by = $user;
                $model->updated_by = $user;
                static::setNullWhenEmpty($model);
            }, 10);

            static::created(function ($model) use ($user) {
                static::setHistoryRecord('created', $model);
            }, 10);

            static::updated(function ($model) use ($user) {
                static::setHistoryRecord('updated', $model);
            }, 10);

            static::updating(function ($model) use ($user) {
                $model->updated_by = $user;
                static::setNullWhenEmpty($model);
            }, 10);

            static::saving(function ($model) use ($user) {
                $model->created_by = $model->created_by ?: $user;
                $model->updated_by = $user;
                static::setNullWhenEmpty($model);
            }, 10);

            static::deleting(function ($model) use ($user) {
                $model->deleted_by = $user;
                $model->save();
            }, 10);
        }
    }

    private static function setHistoryRecord($event, $model)
    {
        if($model->getTable() != 'attachments') {
            foreach($model->getAttributes() as $column => $value) {
                if($model->isDirty($column)) {
                    if(!in_array($column, ['created_by', 'updated_by', 'deleted_by', 'deleted_at', 'updated_at', 'created_at', $model->getKeyName()])){
                        $history_arr = [];
                        $history_arr['reference_id'] = $model->getKey();
                        $history_arr['entity'] = $model->getTable();
                        $history_arr['column'] = $column;
                        $history_arr['action'] = strtoupper($event);
                        $history_arr['new_val'] = empty($value) ? '' : $value;
                        $history_arr['responsible_user'] = strtoupper(Auth::user()->username);
                        $history_arr['responsible_date'] = date('Y-m-d h:i:s.000');
                        if($event == 'updated') {
                            $old = $model->getOriginal();
                            $history_arr['old_val'] = $old[$column];
                            History::create($history_arr);
                        }
                        if($event == 'created' && !empty($value)) {
                            History::create($history_arr);
                        }
                    }
                }
            }
        }
    }

    private static function setNullWhenEmpty($model)
    {
        $attributes = array_merge($model->getFillable());
        foreach ($attributes as $attribute) {
        	$value = $model->{$attribute};
            if($value === '') {
            	$model->{$attribute} = NULL;
            }
        }
    }
}