<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PreAssetNotification extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $delivery;

    public function __construct($delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this;
        $mail->subject('Delivery:' . $this->delivery['vendor_name'] . ' - PO Nos.:' . $this->delivery['vendor_purchase_order_no'] . 
            ' - Delivery Nos.:' . $this->delivery['vendor_delivery_no']);
        $mail->view('mail.pre-asset');
        if(count($this->delivery['attachments']) > 0) {
            foreach($this->delivery['attachments'] as $file) {
                $mail->attachData(base64_decode($file['data']), $file['file_name'], ['mime' => $file['mime_type']]);
            }
        }
        return $mail;
        
    }
}
