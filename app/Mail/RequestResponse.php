<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequestResponse extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $request;
    public $type;
    public $url;

    public function __construct($request, $type)
    {
        $this->request = $request;
        $this->type = str_replace('_', ' ', $type);
        if($type == 'loan_asset') {
            $this->url = route('loan-assets') . '?id=' . base64_encode($request['id']);
        } else if ($type == 'asset_disposal') {
            $this->url = route('asset-disposal') . '?id=' . base64_encode($request['id']);
        } else if ($type == 'asset_replacement') {
            $this->url = route('asset-replacement') . '?id=' . base64_encode($request['id']);
        } else if ($type == 'location_transfer') {
            $this->url = route('location-transfer') . '?id=' . base64_encode($request['id']);
        }
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Fixed Asset Management - '. title_case($this->type) .' Request')->view('mail.request-response');
    }
}
