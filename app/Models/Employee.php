<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Responsible;
use App\Traits\Searchable;
use Carbon\Carbon;

class Employee extends Model
{
    use SoftDeletes, Responsible, Searchable;
    protected $table = 'employees';
    protected $guarded = [];
    protected $dates = ['deleted_at', 'update_at', 'created_at'];
    protected $fillable = ['employee_no', 'first_name', 'middle_name', 'last_name', 'work_phone', 'email', 'employee_status', 'department', 'department_head', 'supervisor', 'date_hired'];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = ['created_at', 'created_by', 'updated_at', 'updated_by'];

    public function attachment()
    {
    	return $this->hasOne('App\Models\Attachment', 'reference_id', 'attachment_id');
    }

    public function setDateHiredAttribute($value){
        if(strtotime($value)){
            $this->attributes['date_hired'] = date('Y-m-d h:i:s.000', strtotime($value));
        }
    }

    public function getDateHiredAttribute($value){
       return Carbon::parse($value)->format('m/d/Y');
    }

    public function scopefindByName($query, $param){
        $e = explode(' ', $param);
        $last_name = isset($e[2]) ? $e[2] : $e[1];
        return $query->where('first_name', $e[0])
            ->where('last_name', $last_name);
    }

    public function scopeAsyncSearch($query, $param){
        return $query->where('first_name', 'like', '%'.$param.'%')
            ->orWhere('middle_name', 'like', '%'.$param.'%')
            ->orWhere('last_name', 'like', '%'.$param.'%');
    }
}
