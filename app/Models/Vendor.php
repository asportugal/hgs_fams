<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Responsible;
use App\Traits\Searchable;

class Vendor extends Model
{
    use Responsible, Searchable;
    protected $table = 'vendors';
    protected $fillable = ['name', 'address', 'contact_person', 'contact_no'];
    protected $guarded = [];
    protected $dates = ['update_at', 'created_at'];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = ['created_at', 'created_by', 'updated_at', 'updated_by'];

    public function setContactPersonAttribute($value){
        $this->attributes['contact_person'] = title_case(trim($value));
    }
}
