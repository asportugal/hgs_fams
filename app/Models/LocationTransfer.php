<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Responsible;
use App\Traits\Searchable;
use App\Traits\Helpers;
use Carbon\Carbon;

class LocationTransfer extends Model
{
    use SoftDeletes, Responsible, Searchable;
    protected $table = 'location_transfer';
    protected $primaryKey = 'location_transfer_id';
    protected $fillable = ['asset_id', 'transfer_remarks', 'itaf_no', 'from_site', 'from_floor', 'from_cube', 'to_site', 'to_floor', 'to_cube','transfer_approver_group', 'transfer_status', 'transfer_approver', 'requested_by'];
    protected $guarded = [];
    protected $dates = ['deleted_at', 'update_at', 'created_at'];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = ['created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by'];

    public function asset() {
        return $this->hasOne('App\Models\Asset', 'asset_id', 'asset_id');
    }

    public function getApprovedDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function getApprovedByAttribute($value){
        if($value) {
            $user = App\Models\User::where('username', $value)->first();
            return $user->full_name;
        }
    }

    public function getRequestedByAttribute($value)
    {
        if($value) {
            $user = App\Models\User::where('username', $value)->first();
            return $user->full_name;
        }
    }

    public function getTransferApproverAttribute($value)
    {
        if($value) {
            $user = \App\User::where('username', $value)->first();
            return $user->full_name;
        }
    }

    public function setRequestedByAttribute($value)
    {
        return $this->attributes['requested_by'] = strtoupper($value);
    }

    public function setApprovedByAttribute($value)
    {
        return $this->attributes['approved_by'] = strtoupper($value);
    }

    public function setTransferApproverAttribute($value)
    {
        return $this->attributes['transfer_approver'] = strtoupper($value);
    }

    public function setItafNoAttribute($value) {
        return $this->attributes['itaf_no'] = strtoupper($value);
    }

    public function setApprovedDateAttribute($value) {
        return $this->attributes['approved_date'] = Helpers::hasDateValue($value);
    }
}
