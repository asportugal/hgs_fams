<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class UserPermission extends Model
{
    protected $table = 'user_permissions';
    protected $fillable = ['permission', 'user_id', 'menu_id'];
    protected $guarded = [];
    public $timestamps = false;

	public function menus()
    {
        return $this->belongsTo('App\Models\Menu', 'menu_id', 'id');
    }

}
