<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class FarmInItems extends Model
{
    protected $table = 'farm_in_items';
    protected $fillable = ['purchase_order', 'description', 'quantity'];
    protected $guarded = [];
   	public $timestamps = false;
   	
    public function farmIn()
    {
        return $this->belongsTo('App\Models\FarmIn', 'purchase_order');
    }
    
}
