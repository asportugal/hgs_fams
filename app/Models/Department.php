<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Responsible;
use App\Traits\Searchable;
class Department extends Model
{
    use SoftDeletes, Responsible, Searchable;
    protected $table = 'departments';
    protected $fillable = ['name'];
	protected $guarded = [];
    protected $dates = ['deleted_at', 'update_at', 'created_at'];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = ['created_at', 'created_by', 'updated_at', 'updated_by'];

    public function setNameAttribute($value){
        $this->attributes['name'] = trim($value);
    }
}
