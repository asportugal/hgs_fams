<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Traits\Responsible;
use App\Traits\Searchable;
use App\Traits\Helpers;
use Carbon\Carbon;

class Asset extends Model
{
    use SoftDeletes, Responsible, Searchable;
    protected $table = 'assets';
    protected $fillable = ['asset_id','amr_no','gr_no','serial_no','description','category','subcategory','lob_owner',
		'bldg','floor','cube','employee','brand','model','sales_invoice_no','delivery_receipt_no','purchase_order_no',
		'delivery_receipt_date', 'purchase_order_date','tag_date','service_date','warranty_start_date','warranty_end_date',
		'supplier', 'notes', 'vat_paid','financial_treatment','local_cost_center','cost_center','asset_life',
		'monthly_depreciation', 'loa_no', 'loa_valid_until_date','netbook_value','status'];
    protected $guarded = ['active', 'consumed_asset_life', 'remaining_asset_life', 'date_fully_depreciated', 
        'maintenance_vendor', 'maintenance_start_date', 'maintenance_end_date', 'maintenance_remark', 
        'maintenance_email_notify', 'contract_type', 'contract_start_date', 'contract_end_date', 'contract_remarks', 'purchase_price_dollar', 'purchase_price_php'];
    protected $dates = ['deleted_at', 'update_at', 'created_at'];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = ['created_by', 'updated_by', 'deleted_by', 'deleted_at', 'updated_at', 'created_at', 'done'];
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();
        // static::addGlobalScope('bldg', function (Builder $builder) {
        //     $sites = Auth::user()->sites->map(function($value){ return $value->name; });
        //     $builder->whereIn('bldg', $sites->all());
        // });
        // if(Auth::user()->group != 'ALL') {
        //     static::addGlobalScope('group', function (Builder $builder) {
        //         $builder->where('group', Auth::user()->group);       
        //     });
        // }
    }

    public function scopeFilterAssets($query, $parameters){
        foreach($parameters as $key => $value) {
            if(str_contains($key, 'date')) {
                $query->whereDate($key, $value);
            } else {
                $query->where($key, 'like', '%'.$value.'%');
            }
        }
        return $query;
    }

    public function preAsset() {
    	return $this->hasOne('App\Models\PreAsset', 'vendor_delivery_no', 'delivery_receipt_no');
    }

    public function setDeliveryReceiptDateAttribute($value){
    	$this->attributes['delivery_receipt_date'] = Helpers::hasDateValue($value);
    }

    public function getStatusAttribute($value){
        return title_case($value);
    }

    public function setStatusAttribute($value){
        $this->attributes['status'] = strtoupper($value);
    }

    public function getCategoryAttribute($value){
        return title_case($value);
    }

    public function getDeliveryReceiptDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function setPurchaseOrderDateAttribute($value){
    	$this->attributes['purchase_order_date'] = Helpers::hasDateValue($value);
    }

    public function getPurchaseOrderDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function setTagDateAttribute($value){
    	$this->attributes['tag_date'] = Helpers::hasDateValue($value);
    }

    public function setDateFullyDepreciatedAttribute($value){
        $this->attributes['date_fully_depreciated'] = Helpers::hasDateValue($value);
    }

    public function setContractStartDateAttribute($value){
        $this->attributes['contract_start_date'] = Helpers::hasDateValue($value);
    }

    public function setContractEndDateAttribute($value){
        $this->attributes['contract_end_date'] = Helpers::hasDateValue($value);
    }

    public function getMaintenanceStartDateAttribute($value) {
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function getMaintenanceEndDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function setMaintenanceStartDateAttribute($value) {
        $this->attributes['maintenance_start_date'] = Helpers::hasDateValue($value);
    }

    public function setMaintenanceEndDateAttribute($value){
        $this->attributes['maintenance_end_date'] = Helpers::hasDateValue($value);
    }

    public function getTagDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function setServiceDateAttribute($value){
    	$this->attributes['service_date'] = Helpers::hasDateValue($value);
    }

    public function getServiceDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function setWarrantyStartDateAttribute($value){
        $this->attributes['warranty_start_date'] = Helpers::hasDateValue($value);
    }

    public function getWarrantyStartDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function setWarrantyEndDateAttribute($value){
        $this->attributes['warranty_end_date'] = Helpers::hasDateValue($value);
    }

    public function getWarrantyEndDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }
    
    public function setLoaValidUntilDateAttribute($value){
    	$this->attributes['loa_valid_until_date'] = Helpers::hasDateValue($value);
    }

    public function getLoaValidUntilDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function setMonthlyDepreciationAttribute($value){
        if($value != '' || $value) {
    	   $this->attributes['monthly_depreciation'] = floatval($value);
        }
    }

    public function setPurchasePriceDollarAttribute($value){
        if($value != '' || $value) {
    	   $this->attributes['purchase_price_dollar'] = floatval($value);
        }
    }

    public function setExchangeRatePhpDollarAttribute($value){
        if($value != '' || $value) {
           $this->attributes['exchange_rate_php_dollar'] = floatval($value);
        }
    }

    public function setPurchasePricePhpAttribute($value){
        if($value != '' || $value) {
            $this->attributes['purchase_price_php'] = floatval($value);    
        }
    }

    public function setNetbookValueAttribute($value){
        if($value != '' || $value) {
    	   $this->attributes['netbook_value'] = floatval($value);
        }
    }

    public function setVatPaidAttribute($value){
        if($value != '' || $value) {
    	   $this->attributes['vat_paid'] = intval($value);
        }
    }

    public function setImportedAttribute($value){
        if($value != '' || $value) {
    	   $this->attributes['imported'] = intval($value);
        }
    }

    public function setAssetLifeAttribute($value){
        if($value != '' || $value) {
    	   $this->attributes['asset_life'] = intval($value);
        }
    }

    public function setLoaNoAttribute($value){
        if($value != '' || $value) { 
    	   $this->attributes['loa_no'] = intval($value);
        }
    }

    public function setRemainingAssetLifeAttribute($value){
        if($value != '') {
            $this->attributes['remaining_asset_life'] = intval($value);    
        }
    }

    public function setCategoryAttribute($value){
    	$this->attributes['category'] = strtoupper($value);
    }

    public function scopeNoLock($query) {
        return $query->from(DB::raw(self::getTable() . ' with (nolock) '));
    }

    public static function createAssetID($category, $site, $date){
        $d = explode('/', $date);
        $formatted_date = $d[1] . chr(64 + $d[0]) . $d[2];
        $asset_id = implode('-', [\App\Category::findByName($category), \App\Site::findByName($site), $formatted_date]);
        $asset = \App\Asset::noLock()->where('bldg', $site)
            ->where('category', $category)
            ->max(DB::raw('right(asset_id, 5)'));

        if($asset) {
            $id = '-'. sprintf('%05d', intval($asset) + 1);
        }
        
        return  $asset_id . (isset($id) ? $id : '-00001');
    }

    public static function defaultInitialValues($data) {
        $initial_vals = ['status' => 'IN STORAGE'];
        $initial_vals['warranty_start_date'] = $data['delivery_receipt_date'];
        $start_date = Carbon::parse($initial_vals['warranty_start_date']);
        $initial_vals['warranty_end_date'] = $start_date->addYears(3);
        return array_merge($data, $initial_vals);
    }
}
