<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Responsible;
use App\Traits\Searchable;
use App\Traits\Helpers;
use Carbon\Carbon;
class FarmOut extends Model
{
    use SoftDeletes, Responsible, Searchable;
    protected $table = 'farm_out';
    protected $fillable = ['control_no', 'request_date'];
    protected $dates = ['deleted_at', 'update_at', 'created_at'];
    protected $guarded = ['peza_permit_status', 'peza_form_no', 'peza_approval_no', 'location', 'vendor', 
    'proforma_invoice_no', 'proforma_invoice_date', 'final_invoice_no', 'final_invoice_date', 'date_filed', 'remarks'];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = ['created_by', 'updated_by', 'deleted_by', 'deleted_at', 'updated_at', 'created_at'];

    public function setProformaInvoiceDateAttribute($value){
        $this->attributes['proforma_invoice_date'] = Helpers::hasDateValue($value);
    }

    public function getProformaInvoiceDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function setFinalInvoiceDateAttribute($value){
        $this->attributes['final_invoice_date'] = Helpers::hasDateValue($value);
    }

    public function getFinalInvoiceDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function setRequestDateAttribute($value){
        $this->attributes['request_date'] = Helpers::hasDateValue($value);
    }

    public function getRequestDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function setDateFiledAttribute($value){
        $this->attributes['date_filed'] = Helpers::hasDateValue($value);
    }

    public function getDateFiledAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public static function generateCtrlNo(){
        $latest_ctrl_no = App\Models\FarmOut::max('control_no');
        $control_no = 'HGS_OUT-'.date('Y').'-00001';
        if($latest_ctrl_no) {
            $c = explode('-', $latest_ctrl_no);
            if(date('Y') == $c[1]) {
                $control_no = 'HGS_OUT-'.date('Y').'-'.sprintf('%05d', intval(last($c)) + 1);
            }
        }
        return $control_no;
    }
}
