<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $table = 'attachments';
    protected $fillable = ['reference_id', 'attachment_type', 'file_name', 'file_size', 'mime_type', 'data'];
    protected $guarded = [];
    protected $hidden = ['attachment_type', 'reference_id'];
    public $timestamps = false;

    protected $casts = [
            'reference_id' => 'string'
        ];
    
    protected $attachmentType = [
        1 => 'asset_image',
        2 => 'delivery_attachments',
        3 => 'asset_attachments',
        4 => 'employee_image',
        5 => 'user_image',
        6 => 'farm_in_attachments',
        7 => 'farm_out_attachments'
    ];

    public function scopeTypeOf($query, $type)
    {
        return $query->where('attachment_type', array_search($type, $this->attachmentType));
    }

    public function scopeEmployeeImage($query, $id)
    {
        return $query->where('attachment_type', '4')->where('reference_id', $id);
    }

    public function scopeFarmInAttachment($query, $id)
    {
        return $query->where('attachment_type', '6')->where('reference_id', $id);
    }

    public function scopeFarmOutAttachment($query, $id)
    {
        return $query->where('attachment_type', '7')->where('reference_id', $id);
    }

    public function scopeDeliveryAttachment($query, $id)
    {
        return $query->where('attachment_type', '2')->where('reference_id', $id);
    }

    public function employee()
    {
    	return $this->belongsTo('App\Models\Employee', 'employee_id');
    }

    public function setDataAttribute($file)
    {
    	$content = $file->openFile()->fread($file->getSize());
    	$this->attributes['data'] = base64_encode($content);
    }
}
