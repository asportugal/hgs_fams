<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Responsible;
use App\Traits\Searchable;

class Notification extends Model
{
    use SoftDeletes, Responsible, Searchable;
    protected $table = 'notifications';
    protected $fillable = ['group_name', 'to_address', 'cc_address'];
    protected $guarded = [];
    protected $dates = ['deleted_at', 'update_at', 'created_at'];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = ['updated_at', 'created_at', 'updated_by', 'created_by'];

    public function setGroupNameAttribute($value){
    	$this->attributes['group_name'] = strtoupper(trim($value));
    }
}
