<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Responsible;
use App\Traits\Searchable;

class Brand extends Model
{
    use SoftDeletes, Responsible, Searchable;
    protected $table = 'brands';
    protected $fillable = ['name'];
    protected $dates = ['deleted_at', 'update_at', 'created_at'];
	protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = ['created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by'];

    public function setNameAttribute($value){
        $this->attributes['name'] = title_case(trim($value));
    }
}
