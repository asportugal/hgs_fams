<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Responsible;
use App\Traits\Searchable;

class Site extends Model
{
    use Responsible, Searchable;
    protected $table = 'sites';
    protected $fillable = ['name', 'code', 'address'];
    protected $guarded = [];
    protected $dates = ['update_at', 'created_at'];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = ['created_at', 'created_by', 'updated_at', 'updated_by', 'pivot'];

    public function site_access()
    {
        return $this->hasMany('App\Models\SiteAccess', 'site_id', 'user_id');
    }

    public function scopeFindByCode($query, $code)
    {
        $site = $query->where('code', $code)->first(['name']);
        return $site ? $site->name : false;
    }

    public function scopeFindByName($query, $name)
    {
        $site = $query->where('name', $name)->first(['code']);
        return $site ? $site->code : NULL;
    }

    public function setCodeAttribute($value){
        $this->attributes['code'] = strtoupper(trim($value));
    }
}
