<?php
namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Traits\Responsible;
use App\Traits\Searchable;


class User extends Authenticatable
{
    use Notifiable, Responsible, Searchable;

    const ROLE_ADMINISTRATOR = 1;
    const ROLE_APPROVER = 2;
    const ROLE_FINANCE = 3;
    const ROLE_REQUESTOR = 4;
    const ROLE_CUSTODIAN = 5;
    const ROLE_HELPDESK = 6;

    const GROUP_ALL = 0;
    const GROUP_IT = 1;
    const GROUP_BAED = 2;

    protected $fillable = [
        'username', 
        'password', 
        'email', 
        'full_name', 
        'role', 
        'group',
    ];

    protected $guarded = [];
    protected $dates = ['updated_at', 'created_at'];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = [
        'password', 
        'approver',
        'remember_token', 
        'created_at', 
        'created_by', 
        'updated_at', 
        'updated_by', 
        'deleted_at', 
        'deleted_by'
    ];

    public static $roles = [
		self::ROLE_ADMINISTRATOR  => 'Administrator',
		self::ROLE_APPROVER       => 'Approver',
		self::ROLE_FINANCE        => 'Finance',
		self::ROLE_REQUESTOR      => 'Requestor',
		self::ROLE_CUSTODIAN      => 'Custodian',
		self::ROLE_HELPDESK       => 'Helpdesk'
    ];

    public static $groups = [
        self::GROUP_ALL     => 'All',
        self::GROUP_IT      => 'I.T.',
        self::GROUP_BAED    => 'BAED'
    ];

    public function sites()
    {
        return $this->belongsToMany('App\Models\Site', 'site_access', 'user_id', 'site_id');
    }

    public function permissions()
    {
        $permissions = Auth::user()->with('user_permissions.menus')->first();
        return $permissions->user_permissions->sortBy('menu_id')->transform(function($value) {
                return collect([
                    'user_id' => $value->user_id,
                    'menu_id' => $value->menu_id,
                    'name' => $value->menus->name,
                    'link' => $value->menus->link,
                    'sub' => $value->menus->getSub(),
                    'parent' => $value->menus->getParent(),
                    'order' => $value->menus->order,
                    'transactions' => strtolower($value->permission)
                ]);
        });
    }

    public function site_access()
    {
        return $this->hasMany('App\Models\SiteAccess');
    }
    
    public function user_permissions()
    {
        return $this->hasMany('App\Models\UserPermission');
    }

    public function menus()
    {
        return $this->belongsToMany('App\Models\Menu', 'user_permissions', 'user_id', 'menu_id');
    }

    public function getRoleAttribute($value)
    {
        return self::$roles[$value];
    }

    public function getGroupAttribute($value)
    {
        return self::$groups[$value];
    }

    public function getEnabledAttribute($value)
    {
        return $value ? 'YES' : 'NO';
    }

    public function setGroupAttribute($value)
    {
        $this->attributes['group'] = strtoupper($value);
    }

    // public function setEnabledAttribute($value)
    // {
    //     $this->attributes['enabled'] = $value ? 'TRUE' : 'FALSE';
    // }

    // public function getEnabledAttribute($value)
    // {
    //     return ($value == 'TRUE') ? true : false; 
    // }

    public function getApproverAttribute($value)
    {
        $approver = User::where('username', $value)->first(['full_name']);
        return empty($approver) ? '' : $approver->full_name;
    }

    public function getFullNameAttribute($value)
    {
        $name = explode('|', $value);
        return implode($name, ' '); 
    }

    public function is_admin() {
        return Auth::user()->role == 'ADMINISTRATOR' ?: 'false'; 
    }
}
