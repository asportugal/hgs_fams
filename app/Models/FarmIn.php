<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Responsible;
use App\Traits\Searchable;
use App\Traits\Helpers;
use Carbon\Carbon;
class FarmIn extends Model
{
    use SoftDeletes, Responsible, Searchable;
    protected $table = 'farm_in';
    protected $fillable = ['purchase_order', 'peza_permit_status', 'peza_form_no', 'peza_approval_no', 'location', 'vendor', 
    'proforma_invoice_no', 'proforma_invoice_date', 'final_invoice_no', 'final_invoice_date', 'total_amount', 'currency', 
    'request_date', 'date_filed', 'remarks'];
    protected $dates = ['deleted_at', 'update_at', 'created_at'];
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = ['created_by', 'updated_by', 'updated_at', 'created_at'];

    public function lineItems() {
        return $this->hasMany('App\FarmInItems', 'purchase_order', 'purchase_order');
    }

    public function scopePendingDelivery($query, $po = null) {
    	return $this->where('peza_permit_status', 'YES')->where('purchase_order', 'like', $po.'%')
    	->whereHas('lineItems', function ($query) {
		    $query->where('done', 0);
		});
    }

    public function setProformaInvoiceDateAttribute($value){
        $this->attributes['proforma_invoice_date'] = Helpers::hasDateValue($value);
    }

    public function getProformaInvoiceDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function setFinalInvoiceDateAttribute($value){
        $this->attributes['final_invoice_date'] = Helpers::hasDateValue($value);
    }

    public function getFinalInvoiceDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function setRequestDateAttribute($value){
        $this->attributes['request_date'] = Helpers::hasDateValue($value);
    }

    public function getRequestDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function setDateFiledAttribute($value){
        $this->attributes['date_filed'] = Helpers::hasDateValue($value);
    }

    public function getDateFiledAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }
}
