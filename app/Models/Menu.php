<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;

class Menu extends Model
{
    protected $table = 'menus';
    protected $dates = ['deleted_at', 'updated_at', 'created_at'];
    protected $guarded = [];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = ['created_at', 'created_by', 'updated_at', 'updated_by', 'pivot'];

    public function user_permissions()
    {
        return $this->hasMany('App\Models\UserPermission', 'id', 'user_id');
    }

    public function getParent() {
    	$parent = explode('/', $this->attributes['parent']);
    	return strtoupper($parent[0]);
    }

    public function getSub() {
    	$sub = explode('/', $this->attributes['parent']);
    	return isset($sub[1]) ? str_replace('-', ' ', $sub[1]) : '';
	}
}
