<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class SiteAccess extends Model
{
    protected $table = 'site_access';
    protected $fillable = ['site_id', 'user_id'];
    protected $guarded = [];
    public $timestamps = false;
    
    public function sites()
    {
        return $this->belongsTo('App\Models\Site', 'site_id', 'site_id');
    }
}
