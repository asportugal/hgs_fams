<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Responsible;
use App\Traits\Searchable;
use App\Traits\Helpers;
use Carbon\Carbon;

class LoanAsset extends Model
{
    use SoftDeletes, Responsible, Searchable;
    protected $table = 'loan_assets';
    protected $primaryKey = 'loan_id';
    protected $fillable = ['asset_id', 'loan_to', 'loan_start_date', 'loan_end_date', 'loan_remarks', 'itaf_no', 'loan_approver', 'loan_status', 'requested_by'];
    protected $guarded = [];
    protected $dates = ['deleted_at', 'update_at', 'created_at'];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = ['created_at', 'created_by', 'updated_at', 'updated_by', 'deleted_at', 'deleted_by'];

    public function getLoanStartDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function getLoanEndDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function getApprovedDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function getCheckedInDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function getCheckedOutDateAttribute($value){
        return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }

    public function getRequestedByAttribute($value)
    {
        if($value) {
            $user = App\Models\User::where('username', $value)->first();
            return $user->full_name;
        }
    }

    public function getApprovedByAttribute($value){
        if($value) {
            $user = App\Models\User::where('username', $value)->first();
            return $user->full_name;
        }
    }

    public function getLoanApproverAttribute($value){
        if($value) {
            $user = App\Models\User::where('username', $value)->first();
            return $user->full_name;
        }
    }

    public function setItafNoAttribute($value) {
        return $this->attributes['itaf_no'] = strtoupper($value);
    }

    public function setApprovedDateAttribute($value) {
        return $this->attributes['approved_date'] = Helpers::hasDateValue($value);
    }

    public function setCheckedInDateAttribute($value) {
        return $this->attributes['checked_in_date'] = Helpers::hasDateValue($value);
    }

    public function setCheckedOutDateAttribute($value) {
        return $this->attributes['checked_out_date'] = Helpers::hasDateValue($value);
    }

    public function setApprovedByAttribute($value)
    {
        return $this->attributes['approved_by'] = strtoupper($value);
    }

    public function setRequestedByAttribute($value)
    {
        return $this->attributes['requested_by'] = strtoupper($value);
    }

    public function setLoanApproverAttribute($value)
    {
        return $this->attributes['loan_approver'] = strtoupper($value);
    }

}
