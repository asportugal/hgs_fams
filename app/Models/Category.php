<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Responsible;
use App\Traits\Searchable;

class Category extends Model
{
    use Responsible, Searchable;
    protected $table = 'categories';
    protected $fillable = ['name', 'code', 'type'];
    protected $guarded = [];
    protected $dates = ['updated_at', 'created_at'];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    protected $hidden = ['created_at', 'created_by', 'updated_at', 'updated_by'];
   
    public function scopeFindByCode($query, $code)
    {
        $category = $query->categories()->where('code', $code)->first(['name']);
        return $category ? $category->name : NULL;
    }

    public function scopeFindByName($query, $name)
    {
        $category = $query->categories()->where('name', $name)->first(['code']);
        return $category ? $category->code : NULL;
    }

    public function scopeCategories($query)
    {
        return $query->where('type', '=', 'CATEGORY');
    }

    public function scopeSubCategories($query, $code = null)
    {
        $subcategories = $query->where('type', '=', 'SUBCATEGORY');
        if($code) {
            $subcategories->where('code', '=', $code);
        }
    	return $subcategories;
    }

    public function setCodeAttribute($value){
        $this->attributes['code'] = strtoupper(trim($value));
    }

    public function setNameAttribute($value){
        $this->attributes['name'] = title_case(trim($value));
    }
    
    public function setTypeAttribute($value){
        $this->attributes['type'] = strtoupper(trim($value));
    }

}
