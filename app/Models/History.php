<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class History extends Model
{
    protected $table = 'history';
    protected $primaryKey = 'history_id';
	protected $guarded = [];
    protected $fillable = ['column', 'reference_id', 'entity', 'action', 'old_val', 'new_val', 'responsible_user', 'responsible_date'];
    protected $dates = ['responsible_date'];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    public $timestamps = false;

    public function getResponsibleDateAttribute($value) {
    	return ($value) ? Carbon::parse($value)->format('m/d/Y') : '';
    }
}
