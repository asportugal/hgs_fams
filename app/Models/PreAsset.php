<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\Responsible;
use App\Traits\Searchable;
use Carbon\Carbon;
use App\Traits\Helpers;
class PreAsset extends Model
{
    use SoftDeletes, Responsible, Searchable;
    protected $table = 'deliveries';
    protected $fillable = ['airway_delivery_no', 'cargo', 'consignee_name', 'airway_delivery_date', 'shipper', 'vendor_name', 'vendor_delivery_date', 'bldg', 'serial_no', 'received_by', 'vendor_purchase_order_no', 'vendor_delivery_no'];
    protected $dates = ['deleted_at', 'updated_at', 'created_at'];
    protected $guarded = [];
    protected $hidden = ['created_by', 'updated_by', 'updated_at', 'created_at'];
    protected $dateFormat = 'Y-m-d H:i:s.000';
    
    public function farmInItems() {
        return $this->hasMany('App\Models\FarmInItems', 'delivery_id');
    }

    public function farmIn() {
        return $this->hasOne('App\Models\FarmIn', 'purchase_order', 'vendor_purchase_order_no');
    }

    public function setAirwayDeliveryDateAttribute($value){
        $this->attributes['airway_delivery_date'] = Helpers::hasDateValue($value);
    }

    public function setVendorDeliveryDateAttribute($value){
        $this->attributes['vendor_delivery_date'] = Helpers::hasDateValue($value);
    }

    public function getAirwayDeliveryDateAttribute($value){
        if($value) {
            return Carbon::parse($value)->format('m/d/Y');
        }
    }

    public function getVendorDeliveryDateAttribute($value){
        return Carbon::parse($value)->format('m/d/Y');
    }

    public function scopeNotDone($query) {
        return $query->where('done', 0);
    }

    public function scopeCompleted($query) {
        return $query->where('quantity', $this->attributes['tagged_qty']);
    }

    public function site()
    {
        return $this->belongsTo('\Site', 'site_code', 'code');
    }

}
