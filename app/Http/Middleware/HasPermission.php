<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Response;
use Closure;


class HasPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $menus_arr = Auth::user()->menus->map(function($value){ return url($value->link); });
        $menus = array_merge([route('change-password')], $menus_arr->toArray());
        if(!in_array(url()->current(), $menus)){
            return new Response(view('errors.unpermitted'));
        }
        return $next($request);
    }

}
