<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DNS2D;
use Validator;
class PrintAssetController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function initialize(Request $request, $printer)
    {
        $assets = [];
        $printer_name = base64_decode($printer);
        if($request->query('all')) {
            $parameters = $request->query();
            unset($parameters['all']);
            $assets = $this->filter($request->parameters);
        } else {
                $assets = \App\Asset::whereIn('asset_id', $request->asset_id)
                ->get(['asset_id', 'amr_no', 'serial_no', 'supplier']);
        }
        try {
            $handle = printer_open($printer_name);
            printer_set_option($handle, PRINTER_MODE, "raw");
            foreach($assets->toArray() as $asset) {
                $supplier = strlen($asset['supplier'] > '20') ? substr($asset['supplier'], 0, 17) . '...' : $asset['supplier'];
                $barcode = $this->generateBarcode($asset['asset_id']);
                printer_start_doc($handle, $asset['asset_id']);
                printer_start_page($handle);
                printer_draw_bmp($handle, public_path().'\\img\\logo.bmp', 280, 20);
                printer_draw_bmp($handle, $barcode, 270, 60, 120, 120);
                $font1 = printer_create_font('Arial', 16, 12, 700, false, false, false, 0);
                printer_select_font($handle, $font1);
                printer_draw_text($handle, 'AMR No:', 10, 20);
                printer_draw_text($handle, 'Serial No:', 10, 60);
                printer_draw_text($handle, 'Supplier:', 10, 100);
                printer_draw_text($handle, 'Asset-Tag:', 10, 140);
                printer_delete_font($font1);
                $font2 = printer_create_font('Arial', 12, 10, 400, false, false, false, 0);
                printer_select_font($handle, $font2);
                printer_draw_text($handle, $asset['amr_no'], 10, 40);
                printer_draw_text($handle, $asset['serial_no'], 10, 80);
                printer_draw_text($handle, $supplier, 10, 120);
                printer_draw_text($handle, $asset['asset_id'], 10, 160);
                printer_select_font($handle, $font2);
                printer_end_page($handle);
                printer_end_doc($handle);
                unlink($barcode);
            }
            printer_close($handle);
        } catch(\ErrorException $e) {
            return $e->getMessage();
        }
    }

    private function generateBarcode($asset_id) {
        $barcode = DNS2D::getBarcodePNGPath($asset_id, 'QRCODE');
        $image = new \Imagick($barcode);
        unlink($barcode);
        $image->writeImage('BMP2:'.$asset_id.'.bmp');
        return public_path().'\\'.$asset_id.'.bmp';
    }

    private function createFile($asset) {
        $file = fopen('html/barcode.html', 'w');
        fwrite($file, view('partials.barcode', $asset)->render());
        fclose($file);
    }

    private function barcode($asset) {
        $barcode = '<table cellpadding="10" style="font-size: 20px; font-family: Arial;">';
        $barcode.= '<tr><td><strong>AMR:</strong>&nbsp;.'.$asset['amr_no'].'.</td><td rowspan="6"><img src="/img/logo.png" width="210"><br><br>';
        $barcode.= DNS2D::getBarcodeHTML($asset['asset_id'], "QRCODE").'</td></tr>';
        $barcode.= '<tr><td><strong>Serial:</strong></td></tr>';
        $barcode.= '<tr><td>'.$asset['serial_no'].'</td></tr>';
        $barcode.= '<tr><td><strong>Vendor:</strong>&nbsp;.'.$asset['supplier'].'.</td></tr>';
        $barcode.= '<tr><td><strong>Asset Tag:</strong></td></tr>';
        $barcode.= '<tr><td>'.$asset['asset_id'].'</td></tr>';
        return $barcode;
    }

    private function filter($parameters)
    {
        if($parameters) {
            return \App\Asset::filterAssets($parameters)->get(['asset_id', 'amr_no', 'serial_no', 'supplier']);
        } else {
            return \App\Asset::get(['asset_id', 'amr_no', 'serial_no', 'supplier']);
        }
    }

    public function search(Request $request)
    {
        if($request->query()){
            $parameters = $request->query();
            unset($parameters['page']);
            $assets = \App\Asset::filterAssets($parameters)->with('preAsset.farmIn')->paginate(10);
            $assets->setPath('')->toArray();
            return response()->json($assets);
        } else {
            $assets = \App\Asset::with('preAsset.farmIn')->paginate(10);
            $assets->setPath('')->toArray();
            return response()->json($assets);
        }
    }
}
