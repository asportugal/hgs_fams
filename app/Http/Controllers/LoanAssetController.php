<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Mail\LoanAssetNotification;
use App\Mail\RequestResponse;
use Validator;
class LoanAssetController extends Controller
{

    private $attribs;

    public function  __construct() 
    {
    	$this->middleware(['auth']);
        $this->attribs =  ['loan_status' => 'loan_status',
                            'asset_id' => 'asset_id',
                            'loan_approver_group' => 'approver_group',
                            'loan_approver' => 'approver', 
                            'loan_to' => 'requestor',
                            'itaf_no' => 'itaf_no',
                            'loan_start_date' => 'start_date',
                            'loan_end_date' => 'end_date',
                            'loan_remarks' => 'remarks',
                            'checked_in_by' => 'checked_in_by',
                            'checked_out_by' => 'checked_out_by',
                            'checked_in_remarks' => 'checked_in_remarks'];
    }

    public function export(Request $request, $type)
    {
        if($request->query('narrow')) {
             $parameters = array_except($request->query(), ['narrow', 'page']);
             $loan_assets = \App\LoanAsset::narrowSearch($parameters)->get();
        } else {
            $q = $request->query('search');
            $loan_assets = \App\LoanAsset::search($q)->get();
        }

        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('export', $loan_assets);
        }
    }

    public function index(Request $request)
    {
        if($request->query('narrow')) {
             $parameters = array_except($request->query(), ['narrow', 'page']);
             $loan_asset = \App\LoanAsset::narrowSearch($parameters)->paginate(10);
             $loan_asset->setPath('')->toArray();
        } else {
            $q = $request->query('search');
            $loan_asset = \App\LoanAsset::search($q)->paginate(10);
            $loan_asset->setPath('')->toArray();
        }
        return response()->json($loan_asset);
    }

    public function show($id)
    {
        $loan_id = base64_decode($id);
        $loan_asset = \App\LoanAsset::where('loan_id', $loan_id)->first();
        return response()->json($loan_asset);
    }

    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->all()), [
                'asset_id' => 'required|exists:assets,asset_id',
                'approver' => 'required|exists:users,username',
                'requestor'  => 'required',
                'start_date' => 'required|date',
                'end_date' => 'required|date',
                'itaf_no' => 'required',
                'remarks' => 'required',
            ])->validate();

        $loan_asset = new \App\LoanAsset;
        $loan_asset->loan_status = 'PENDING';
        $loan_asset->requested_by = Auth::user()->username;
        foreach(array_except($this->attribs, ['loan_status']) as $key => $value) {
            $loan_asset->{$key} = $request->{$value};
        }
        $loan_asset->save();

        $approver = \App\User::where('username', $request->approver)->first();
        Mail::to($approver->email)->send(new LoanAssetNotification($loan_asset));

        session(['status' => 'Asset loan request is now submitted to '.$request->approver_group.' for approval']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        Validator::make(array_map('trim', $request->all()), [
                'asset_id' => 'required|exists:assets,asset_id',
                'approver' => 'required|exists:users,username',
                'requestor'  => 'required',
                'start_date' => 'date',
                'end_date' => 'date',
                'itaf_no' => 'required',
                'remarks' => 'required',
            ])->validate();

        $loan_asset = \App\LoanAsset::find($id);
        if($request->checked_out_by && (!$loan_asset->checked_out_date)) {
            $loan_asset->checked_out_date = date('m/d/Y');    
        }

        if($request->checked_in_by && (!$loan_asset->checked_in_date)) {
            $loan_asset->checked_in_date = date('m/d/Y');
            $asset = \App\Asset::find($loan_asset->asset_id);
            $asset->status = 'IN STORAGE';
            $asset->save();
        }

        foreach($this->attribs as $key => $value) {
            $loan_asset->{$key} = $request->{$value};
        }

        $loan_asset->save();
        
        session(['status' => 'Loan Asset succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function action(Request $request, $id)
    {
        $loan_asset = \App\LoanAsset::find($id);
        $loan_asset->loan_status = strtoupper($request->loan_status);
        $loan_asset->approved_by = Auth::user()->username;
        $loan_asset->approved_date = date('m/d/Y');
        $loan_asset->save();

        $asset = \App\Asset::find($loan_asset->asset_id);
        $asset->status = 'LOANED OUT';
        $asset->save();

        $request_details = ['id' => $loan_asset->loan_id,
                            'status' => $loan_asset->loan_status, 
                            'itaf_no' => $loan_asset->itaf_no];

        $requestor = \App\Employee::findByName($loan_asset->loan_to)->first();
        Mail::to($requestor->email)->send(new RequestResponse($request_details, 'loan_asset'));
        
        session(['status' => 'Asset Loan succesfully approved!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\LoanAsset::destroy($id);
        session(['status' => 'Loan Asset succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
