<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Mail\LocationTransferNotification;
use App\Mail\RequestResponse;
use PDF;
use Validator;
class LocationTransferController extends Controller
{

    private $attributes_mapping;

    public function  __construct() 
    {
    	$this->middleware(['auth']);
    }

    public function export(Request $request, $type)
    {
        if($request->query('narrow')) {
             $parameters = array_except($request->query(), ['narrow', 'page']);
             $location_transfer = \App\LocationTransfer::narrowSearch($parameters)->get();
        } else {
            $q = $request->query('search');
            $location_transfer = \App\LocationTransfer::search($q)->get();
        }

        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('export', $location_transfer);
        }
    }
    
    public function print_form(Request $request, $id)
    {
        $location_transfer_id = base64_decode($id);
        $location_transfer = \App\LocationTransfer::where('location_transfer_id', $location_transfer_id)->with(['asset'])->first();
        $pdf = PDF::loadView('pdf.location-transfer', ['data' => $location_transfer])->setPaper('a4', 'landscape');
        return $pdf->stream('Gate Pass.pdf');
    }

    public function index(Request $request)
    {
        if($request->query('narrow')) {
             $parameters = array_except($request->query(), ['narrow', 'page']);
             $location_transfer = \App\LocationTransfer::narrowSearch($parameters)->paginate(10);
             $location_transfer->setPath('')->toArray();
        } else {
            $q = $request->query('search');
            $location_transfer = \App\LocationTransfer::search($q)->paginate(10);
            $location_transfer->setPath('')->toArray();
        }
        return response()->json($location_transfer);
    }

    public function show($id)
    {
        $location_transfer_id = base64_decode($id);
        $location_transfer = \App\LocationTransfer::where('location_transfer_id', $location_transfer_id)->first();
        return response()->json($location_transfer);
    }

    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->all()), [
                'asset_id' => 'required|exists:assets,asset_id',
                'transfer_approver' => 'required|exists:users,username',
                'to_site' => 'required|exists:sites,name',
                'itaf_no' => 'required',
                'transfer_remarks' => 'required',
            ])->validate();

        $asset = \App\Asset::find($request->asset_id);
        $attributes = array_merge(['transfer_status' => 'PENDING', 
                                   'requested_by' => Auth::user()->username,
                                   'from_site' => $asset->bldg,
                                   'from_floor' => $asset->floor,
                                   'from_cube' => $asset->cube]
                                   , $request->except('transfer_status'));

        $location_transfer = \App\LocationTransfer::create($attributes);

        $approver = \App\User::where('username', $request->transfer_approver)->first();
        Mail::to($approver->email)->send(new LocationTransferNotification($location_transfer));

        session(['status' => 'Location Transfer request is now submitted to '.$request->transfer_approver_group.' for approval']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        unset($request['_method']);
        Validator::make(array_map('trim', $request->all()), [
            'asset_id' => 'required|exists:assets,asset_id',
            'transfer_approver' => 'required|exists:users,username',
            'to_site' => 'exists:sites,name',
            'itaf_no' => 'required',
            'transfer_remarks' => 'required',
        ])->validate();

        $location_transfer = \App\LocationTransfer::find($id);
        foreach($request->except('asset_image') as $key => $value) {
            $location_transfer->$key = $value;
        }

        $location_transfer->save();
        
        session(['status' => 'Location transfer succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function action(Request $request, $id)
    {
        $location_transfer = \App\LocationTransfer::find($id);
        $location_transfer->transfer_status = strtoupper($request->transfer_status);
        $location_transfer->approved_by = Auth::user()->username;
        $location_transfer->approved_date = date('m/d/Y');

        if($request->transfer_status == 'APPROVED') {
            $asset = \App\Asset::find($location_transfer->asset_id);
            $asset->bldg = $location_transfer->to_site;
            $asset->floor = $location_transfer->to_floor;
            $asset->cube = $location_transfer->to_cube;
            $asset->status = 'IN STORAGE';
            $asset->save();

            $location_transfer->control_no = \App\FarmOut::generateCtrlNo();
            \App\FarmOut::create(['request_date' => date('m/d/Y'), 'control_no' => $location_transfer->control_no]);
        }
        $location_transfer->save();

        $request_details = ['id' => $location_transfer->location_transfer_id,
                            'status' => $location_transfer->transfer_status, 
                            'itaf_no' => $location_transfer->itaf_no];

        $requestor = \App\User::where('username', $location_transfer->getOriginal('requested_by'))->first();
        Mail::to($requestor->email)->send(new RequestResponse($request_details, 'location_transfer'));
        
        session(['status' => 'Location transfer succesfully approved!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\LoanAsset::destroy($id);
        session(['status' => 'Loan Asset succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
