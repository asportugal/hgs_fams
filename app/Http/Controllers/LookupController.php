<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
class LookupController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $lookup_values = \App\LookupValue::search($q)->dynamicValues();
        $lookup_values = $lookup_values->paginate(15)->setPath('')->appends(['search' => $q])->toArray();
        return response()->json($lookup_values);
    }

    public function show($id)
    {
        $lookup_id = base64_decode($id);
        $lookup_values = \App\Brand::find($lookup_id);
        return response()->json($lookup_values);
    }

    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->all()), [
            'tag' => 'required|exists:lookup_values,tag,deleted_at,NULL',
            'name' => 'required|unique:lookup_values,name,NULL,NULL,tag,'.strtoupper($request->tag).',deleted_at,NULL',
        ])->validate();

        $lookup = new \App\LookupValue;
        $lookup->tag = $request->tag;
        $lookup->name = $request->name;
        $lookup->value = $request->name;
        $lookup->save();

        session(['status' => 'Lookup value succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        Validator::make(array_map('trim', $request->all()), [
            'tag' => 'required|exists:lookup_values,tag,deleted_at,NULL',
            'name' => 'required|unique:lookup_values,name,'.$id.',lookup_id,tag,'.strtoupper($request->tag).',deleted_at,NULL',
        ])->validate();

        $lookup = \App\LookupValue::find($id);
        $lookup->tag = $request->tag;
        $lookup->name = $request->name;
        $lookup->value = $request->name;
        $lookup->save();

        session(['status' => 'Lookup value succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\LookupValue::destroy($id);
        session(['status' => 'Lookup succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
