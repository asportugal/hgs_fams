<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Mail\AssetDisposalNotification;
use App\Mail\RequestResponse;
use Validator;
class AssetDisposalController extends Controller
{

    private $attributes_mapping;

    public function  __construct() 
    {
    	$this->middleware(['auth']);
    }

    public function export(Request $request, $type)
    {
        if($request->query('narrow')) {
             $parameters = array_except($request->query(), ['narrow', 'page']);
             $asset_disposal = \App\AssetDisposal::narrowSearch($parameters)->get();
        } else {
            $q = $request->query('search');
            $asset_disposal = \App\AssetDisposal::search($q)->get();
        }

        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('export', $asset_disposal);
        }
    }

    public function index(Request $request)
    {
        if($request->query('narrow')) {
             $parameters = array_except($request->query(), ['narrow', 'page']);
             $asset_disposal = \App\AssetDisposal::narrowSearch($parameters)->paginate(10);
             $asset_disposal->setPath('')->toArray();
        } else {
            $q = $request->query('search');
            $asset_disposal = \App\AssetDisposal::search($q)->paginate(10);
            $asset_disposal->setPath('')->toArray();
        }
        return response()->json($asset_disposal);
    }

    public function show($id)
    {
        $disposal_id = base64_decode($id);
        $asset_disposal = \App\AssetDisposal::where('disposal_id', $disposal_id)->first();
        return response()->json($asset_disposal);
    }

    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->all()), [
                'asset_id' => 'required|exists:assets,asset_id',
                'disposal_approver' => 'required|exists:users,username',
                'disposal_method' => 'required',
                'itaf_no' => 'required',
                'disposal_remarks' => 'required',
            ])->validate();

        $attributes = array_merge(['disposal_status' => 'PENDING', 
                                   'requested_by' => Auth::user()->username]
                                   , $request->except('disposal_status'));
        $asset_disposal = \App\AssetDisposal::create($attributes);

        $approver = \App\User::where('username', $request->disposal_approver)->first();
        Mail::to($approver->email)->send(new AssetDisposalNotification($asset_disposal));

        session(['status' => 'Asset Disposal request is now submitted to '.$request->disposal_approver_group.' for approval']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        unset($request['_method']);
        Validator::make(array_map('trim', $request->all()), [
            'asset_id' => 'required|exists:assets,asset_id',
            'disposal_approver' => 'required|exists:users,username',
            'disposal_method' => 'required',
            'itaf_no' => 'required',
            'disposal_remarks' => 'required',
        ])->validate();

        $asset_disposal = \App\AssetDisposal::find($id);
        foreach($request->except('asset_image') as $key => $value) {
            $asset_disposal->$key = $value;
        }

        $asset_disposal->save();
        
        session(['status' => 'Asset Disposal succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function action(Request $request, $id)
    {
        $asset_disposal = \App\AssetDisposal::find($id);
        $asset_disposal->disposal_status = strtoupper($request->disposal_status);
        $asset_disposal->approved_by = Auth::user()->username;
        $asset_disposal->approved_date = date('m/d/Y');
        $asset_disposal->save();

        $asset = \App\Asset::find($asset_disposal->asset_id);
        $asset->status = 'DISPOSED';
        $asset->save();

        $request_details = ['id' => $asset_disposal->disposal_id,
                            'status' => $asset_disposal->disposal_status, 
                            'itaf_no' => $asset_disposal->itaf_no];

        $requestor = \App\User::where('username', $asset_disposal->getOriginal('requested_by'))->first();
        Mail::to($requestor->email)->send(new RequestResponse($request_details, 'asset_disposal'));
        
        session(['status' => 'Asset Disposal succesfully approved!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\LoanAsset::destroy($id);
        session(['status' => 'Loan Asset succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
