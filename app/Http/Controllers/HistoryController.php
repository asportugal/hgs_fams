<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class HistoryController extends Controller
{
	public function __construct()
	{
		$this->middleware(['auth']);
	}

	public function retrieve($id) {
		return \App\History::where('reference_id', $id)->orderBy('responsible_date')->get();
	}

}