<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
class VendorController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export(Request $request, $type)
    {
        $q = $request->query('search');
        $vendors = \App\Vendor::search($q)->get();
        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('vendors', $vendors);
        }
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $vendors = \App\Vendor::search($q);
        $vendors = $vendors->paginate(15)->setPath('')->appends(['search' => $q])->toArray();
        return response()->json($vendors);
    }

    public function show($id)
    {
        $vendor_id = base64_decode($id);
        $vendor = \App\Vendor::find($vendor_id);
        return response()->json($vendor);
    }

    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->all()), [
            'supplier_name' => 'required|unique:vendors,name,NULL,NULL,deleted_at,NULL',
            'contact_person' => 'required',
            'contact_number' => 'required'
        ])->validate();

        \App\Vendor::create(array_map('trim', [
            'name' => $request->supplier_name,
            'contact_person' => $request->contact_person,
            'contact_no' => $request->contact_number,
            'address' => $request->supplier_address
        ]));
        
        session(['status' => 'Vendor succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        Validator::make(array_map('trim', $request->all()), [
            'supplier_name' => 'required|unique:vendors,name,'.$id.',vendor_id,deleted_at,NULL',
            'contact_person' => 'required',
            'contact_number' => 'required'
        ])->validate();

        $vendor = \App\Vendor::find($id);
        $vendor->name = trim($request->supplier_name);
        $vendor->contact_person = trim($request->contact_person);
        $vendor->contact_no = trim($request->contact_number);
        $vendor->address = trim($request->supplier_address);
        $vendor->save();    

        session(['status' => 'Vendor succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\Vendor::destroy($id);
        session(['status' => 'Vendor succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
