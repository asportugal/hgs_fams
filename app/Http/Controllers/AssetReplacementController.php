<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Mail\AssetReplacementNotification;
use App\Mail\RequestResponse;
use Validator;
class AssetReplacementController extends Controller
{

    private $attributes_mapping;

    public function  __construct() 
    {
    	$this->middleware(['auth']);
    }

    public function export(Request $request, $type)
    {
        if($request->query('narrow')) {
             $parameters = array_except($request->query(), ['narrow', 'page']);
             $asset_replacement = \App\AssetReplacement::narrowSearch($parameters)->get();
        } else {
            $q = $request->query('search');
            $asset_replacement = \App\AssetReplacement::search($q)->get();
        }

        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('export', $asset_replacement);
        }
    }

    public function index(Request $request)
    {
        if($request->query('narrow')) {
             $parameters = array_except($request->query(), ['narrow', 'page']);
             $asset_replacement = \App\AssetReplacement::narrowSearch($parameters)->paginate(10);
             $asset_replacement->setPath('')->toArray();
        } else {
            $q = $request->query('search');
            $asset_replacement = \App\AssetReplacement::search($q)->paginate(10);
            $asset_replacement->setPath('')->toArray();
        }
        return response()->json($asset_replacement);
    }

    public function show($id)
    {
        $replacement_id = base64_decode($id);
        $asset_replacement = \App\AssetReplacement::where('replacement_id', $replacement_id)->first();
        return response()->json($asset_replacement);
    }

    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->all()), [
                'asset_id' => 'required|exists:assets,asset_id',
                'replacement_approver' => 'required|exists:users,username',
                'serial_no' => 'required|unique:assets,serial_no,'.$request->asset_id.',asset_id,serial_no,!NONE',
                'itaf_no' => 'required',
                'replacement_remarks' => 'required',
            ])->validate();

        $attributes = array_merge(['replacement_status' => 'PENDING', 
                                   'requested_by' => Auth::user()->username]
                                   , $request->except('replacement_status'));
        $asset_replacement = \App\AssetReplacement::create($attributes);

        $approver = \App\User::where('username', $request->replacement_approver)->first();
        Mail::to($approver->email)->send(new AssetReplacementNotification($asset_replacement));

        session(['status' => 'Asset Replacement request is now submitted to '.$request->replacement_approver_group.' for approval']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        unset($request['_method']);
        Validator::make(array_map('trim', $request->all()), [
            'asset_id' => 'required|exists:assets,asset_id',
            'replacement_approver' => 'required|exists:users,username',
            'serial_no' => 'required|unique:assets,serial_no,'.$request->asset_id.',asset_id,serial_no,!NONE',
            'itaf_no' => 'required',
            'replacement_remarks' => 'required',
        ])->validate();

        $asset_replacement = \App\AssetReplacement::find($id);
        foreach($request->except('asset_image') as $key => $value) {
            $asset_replacement->$key = $value;
        }

        $asset_replacement->save();
        
        session(['status' => 'Asset replacement succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function action(Request $request, $id)
    {
        $asset_replacement = \App\AssetReplacement::find($id);
        $asset_replacement->replacement_status = strtoupper($request->replacement_status);
        $asset_replacement->approved_by = Auth::user()->username;
        $asset_replacement->approved_date = date('m/d/Y');
        $asset_replacement->save();

        $asset = \App\Asset::find($asset_replacement->asset_id);
        $asset->serial_no = $asset_replacement->serial_no;
        $asset->status = 'IN STORAGE';
        $asset->save();

        $request_details = ['id' => $asset_replacement->replacement_id,
                            'status' => $asset_replacement->replacement_status, 
                            'itaf_no' => $asset_replacement->itaf_no];

        $requestor = \App\User::where('username', $asset_replacement->getOriginal('requested_by'))->first();
        Mail::to($requestor->email)->send(new RequestResponse($request_details, 'asset_replacement'));
        
        session(['status' => 'Asset replacement succesfully approved!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\LoanAsset::destroy($id);
        session(['status' => 'Loan Asset succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
