<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
class RequestApproverController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $approvers = \App\Approver::search($q);
        $approvers = $approvers->paginate(15)->setPath('')->appends(['search' => $q])->toArray();
        return response()->json($approvers);
    }

    public function show($id)
    {
        $approver_id = base64_decode($id);
        $request_approvers = \App\Brand::find($approver_id);
        return response()->json($request_approvers);
    }

    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->all()), [
            'group_name' => 'required|unique:approvers,group_name,NULL,NULL,deleted_at,NULL',
            'approver' => 'required|exists:users,username',
        ])->validate();

        $approver = new \App\Approver;
        $approver->group_name = $request->group_name;
        $approver->approver = $request->approver;
        $approver->save();

        session(['status' => 'Approver succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        Validator::make(array_map('trim', $request->all()), [
            'group_name' => 'required|unique:approvers,group_name,'.$id.',approver_id,deleted_at,NULL',
            'approver' => 'required|exists:users,username',
        ])->validate();

        $approver = \App\Approver::find($id);
        $approver->group_name = $request->group_name;
        $approver->approver = $request->approver;
        $approver->save();

        session(['status' => 'Approver succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\Approver::destroy($id);
        session(['status' => 'Lookup succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
