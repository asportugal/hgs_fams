<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class AttachmentController extends Controller
{
	public function __construct()
	{
		$this->middleware(['auth']);
	}

	public function show(Request $request, $type, $id)
	{
		$attachment = \App\Attachment::typeOf($type)->where('reference_id', $id)->get();
		return $attachment;
	}

	public function store(Request $request)
	{
		session(['image' => $request->file('delivery_attachments')]);
		return response('success', 200)->header('Content-Type', 'text/plain');
	}
}