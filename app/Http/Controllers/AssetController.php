<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Validator;


class AssetController extends Controller
{
    public function  __construct() 
    {
    	$this->middleware(['auth']);
    }

    public function export(Request $request, $type)
    {
        if($request->query('narrow')) {
             $parameters = array_except($request->query(), ['narrow', 'page']);
             $assets = \App\Models\Asset::narrowSearch($parameters)->get();
        } else {
            $q = $request->query('search');
            $assets = \App\Models\Asset::search($q)->get();
        }
        
        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('export', $assets);
        }
    }

    public function index(Request $request)
    {
        if($request->query('narrow')) {
             $parameters = array_except($request->query(), ['narrow', 'page']);
             $assets = \App\Models\Asset::narrowSearch($parameters)->with('preAsset.farmIn')->paginate(10);
             $assets->setPath('')->toArray();
        } else {
            $q = $request->query('search');
            $assets = \App\Models\Asset::search($q)->with('preAsset.farmIn')->paginate(10);
            $assets->setPath('')->toArray();
        }
        return response()->json($assets);
    }

    public function show($id)
    {
        $asset_id = base64_decode($id);
        $asset = \App\Models\Asset::with('preAsset.farmIn')->find($asset_id);
        return response()->json($asset);
    }

    public function store(Request $request)
    {
    	unset($request['peza_transaction_type'], $request['farm_in_date'], $request['peza_in_form_no'], 
    		$request['peza_in_permit_no'], $request['imported'], $request['import_permit_no'], 
    		$request['pbo_no'], $request['import_cewe_no'], $request['import_boat_note'], 
			$request['import_boc_cert'], $request['import_ntc_permit_no']);

    	Validator::make(array_map('trim', $request->except(['asset_image'])), [
            'asset_id' => 'required|unique:assets,asset_id',
            'category' => 'required|exists:categories,name,type,CATEGORY',
            'subcategory' => 'exists:categories,name,type,SUBCATEGORY',
            'bldg' => 'required|exists:sites,name',
            'supplier' => 'exists:vendors,name',
            'brand' => 'exists:brands,name',
            'delivery_receipt_no' => 'required',
            'serial_no' => 'required|unique:assets,serial_no,NONE,serial_no',   
            'purchase_order_no' => 'required',
            'delivery_receipt_date' => 'required|date'
        ])->validate();

        $post_asset_data = \App\Models\Asset::defaultInitialValues($request->except(['asset_image']));
    	$asset = \App\Models\Asset::create(array_map('trim', $post_asset_data));
        $pre_asset = \App\Models\PreAsset::where('vendor_purchase_order_no', 'purchase_order_no')->first();
        if($pre_asset && ($pre_asset->quantity == $pre_asset->tagged_qty)){
            $pre_asset->done = 1;
            $pre_asset->save();
        }

    	if($request->file('asset_image'))
        {
    		\App\Attachment::create([
                'reference_id' => $asset->asset_id,
                'attachment_type' => '1',
                'file_name' => $request->asset_image->getClientOriginalName(), 
                'file_size' => $request->asset_image->getSize(),
                'mime_type' => $request->asset_image->getMimeType(),
                'data' => $request->asset_image
        	]);
        }
        session(['status' => 'Asset succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        unset($request['_method']);
        Validator::make(array_map('trim', $request->except(['asset_image'])), [
            'asset_id' => 'required|unique:assets,asset_id,'.$id.',asset_id',
            'serial_no' => 'required|unique:assets,serial_no,'.$request->serial_no.',serial_no,serial_no,NONE',
            'category' => 'exists:categories,name,type,CATEGORY',
            'subcategory' => 'exists:categories,name,type,SUBCATEGORY',
            'delivery_receipt_date' => 'required|date',
            'bldg' => 'exists:sites,name',
            'supplier' => 'exists:vendors,name',
            'brand' => 'exists:brands,name',
            'purchase_order_no' => 'required'
        ])->validate();

        $asset = \App\Models\Asset::find($id);
        foreach($request->except(['asset_image']) as $key => $value) {
            if((strlen($value) > 0) && ($value !== 'null')) {
                $asset->{$key} = $value;
            }
        }
        $asset->save();
        
        if($request->asset_image) 
        {
            \App\Attachment::updateOrCreate(
                ['attachment_type' => '1', 'reference_id' => $asset->asset_id],
            ['file_name' => $request->asset_image->getClientOriginalName(),
                'file_size' => $request->asset_image->getSize(),
                'mime_type' => $request->asset_image->getMimeType(),
                'data' => $request->asset_image]);
        }

        session(['status' => 'Asset succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function generate($param) 
    {
        $fragment = explode("|", base64_decode($param));
        $category = \App\Category::findByCode($fragment[0]);
        $site = \App\Site::findByCode($fragment[1]);
        $date = date('m/d/y', strtotime($fragment[2]));
        if(!is_null($category) && !is_null($site)) {
            return \App\Models\Asset::createAssetID($category, $site, $date);
        }
    }

    public function summary($scope)
    {
        $assets = '';
        if($scope == 'site') {
            $assets = \App\Models\Asset::select(DB::raw("bldg as label"), DB::raw("count(asset_id) as asset_count"))->groupBy('bldg')->get(title_case('label'), 'asset_count');
        }

        if($scope == 'category') {
            $assets = \App\Models\Asset::select(DB::raw("category as label"), DB::raw("count(asset_id) as asset_count"))->groupBy('category')->get(title_case('label'), 'asset_count');
        }

        if($scope == 'status') {
            $assets = \App\Models\Asset::select(DB::raw("status as label"), DB::raw("count(asset_id) as asset_count"))->groupBy('status')->get(title_case('label'), 'asset_count');
        }

        return response()->json($assets);
    }
}
