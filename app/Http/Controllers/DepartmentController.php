<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
class DepartmentController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export(Request $request, $type)
    {
        $q = $request->query('search');
        $departments = \App\Department::search($q)->get();
        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('departments', $departments);
        }
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $department = \App\Department::search($q);
        $department = $department->paginate(15)->setPath('')->appends(['search' => $q])->toArray();
        return response()->json($department);
    }

    public function show($id)
    {
        $dept_id = base64_decode($id);
        $department = \App\Department::find($dept_id);
        return response()->json($department);
    }

    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->all()), [
            'department_name' => 'required|unique:departments,name,NULL,NULL,deleted_at,NULL',
        ])->validate();

        \App\Department::create(array_map('trim', [
            'name' => $request->department_name
        ]));
        
        session(['status' => 'Department succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        Validator::make(array_map('trim', $request->all()), [
            'department_name' => 'required|unique:departments,name,'.$id.',department_id,deleted_at,NULL'
        ])->validate();

        $departments = \App\Department::find($id);
        $departments->name = trim($request->department_name);
        $departments->save();    

        session(['status' => 'Department succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\Department::destroy($id);
        session(['status' => 'Department succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
