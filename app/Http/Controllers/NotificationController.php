<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
class NotificationController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export(Request $request, $type)
    {
        $q = $request->query('search');
        $notifications = \App\Notification::search($q)->get();
        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('notifications', $notifications);
        }
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $notifications = \App\Notification::search($q);
        $notifications = $notifications->paginate(15)->setPath('')->appends(['search' => $q])->toArray();
        return response()->json($notifications);
    }

    public function show($id)
    {
        $notification_id = base64_decode($id);
        $notification = \App\Notification::find($notification_id);
        return response()->json($notification);
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'group_name' => 'required|unique:notifications,group_name,NULL,NULL,deleted_at,NULL',
            'to_address.*' => 'bail|distinct|email',
            'cc_address.*' => 'bail|distinct|email'
        ])->validate();

        \App\Notification::create(array_map('trim', [
            'group_name' => $request->group_name,
            'to_address' => $request->to_address ? implode(';', $request->to_address) : '',
            'cc_address' => $request->cc_address ? implode(';', $request->cc_address) : ''
        ]));
        
        session(['status' => 'Notification succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        Validator::make($request->all(), [
            'group_name' => 'required|unique:notifications,group_name,'.$id.',notification_id,deleted_at,NULL',
            'to_address.*' => 'bail|distinct|email',
            'cc_address.*' => 'bail|distinct|email'
        ])->validate();

        $notification = \App\Notification::find($id);
        $notification->group_name = trim($request->group_name);
        $notification->to_address = trim(implode(";", $request->to_address));
        $notification->cc_address = trim(implode(";", $request->cc_address));
        $notification->save();    

        session(['status' => 'Notification succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\Notification::destroy($id);
        session(['status' => 'Notification succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
