<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
class BrandController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export(Request $request, $type)
    {
        $q = $request->query('search');
        $brands = \App\Brand::search($q)->get();
        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('brands', $brands);
        }
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $brands = \App\Brand::search($q);
        $brands = $brands->paginate(15)->setPath('')->appends(['search' => $q])->toArray();
        return response()->json($brands);
    }

    public function show($id)
    {
        $brand_id = base64_decode($id);
        $brand = \App\Brand::find($brand_id);
        return response()->json($brand);
    }

    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->all()), [
            'brand_name' => 'required|unique:brands,name,NULL,NULL,deleted_at,NULL'
        ])->validate();

        \App\Brand::create(array_map('trim', [
            'name' => $request->brand_name,
        ]));
        
        session(['status' => 'Brand succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        Validator::make(array_map('trim', $request->all()), [
            'brand_name' => 'required|unique:brands,name,'.$id.',brand_id,deleted_at,NULL'
        ])->validate();

        $brand = \App\Brand::find($id);
        $brand->name = trim($request->brand_name);
        $brand->save();    

        session(['status' => 'Brand succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\Brand::destroy($id);
        session(['status' => 'Brand succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
