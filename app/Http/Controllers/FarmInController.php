<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Traits\FileHelper;
use Validator;
class FarmInController extends Controller
{
    use FileHelper;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export(Request $request, $type)
    {
        $q = $request->query('search');
        $farm_in = \App\FarmIn::search($q)->get();
        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('farm_in', $farm_in);
        }
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $farm_in = \App\FarmIn::search($q)->with('lineItems');
        $farm_in = $farm_in->paginate(10)->setPath('')->appends(['search' => $q])->toArray();
        return response()->json($farm_in);
    }

    public function show($id)
    {
        $farm_in_id = base64_decode($id);
        $farm_in = \App\FarmIn::with('lineItems')->find($farm_in_id);
        return response()->json($farm_in);
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'purchase_order' => 'required|unique:farm_in,purchase_order,NULL,NULL,deleted_at,NULL',
            'peza_permit_status' => 'required',
            'location' => 'required|exists:sites,name',
            'vendor' => 'required|exists:vendors,name',
            'total_amount' => 'required',
            'currency' => 'required',
            'description.*' => 'required|distinct',
            'quantity.*' => 'required|integer'
        ])->validate();

        $farm_in = \App\FarmIn::create(array_map('trim', $request->except(['description', 'quantity', 'farm_in_attachments'])));
        $line_items = collect($request->description)->zip($request->quantity);
        $this->createLineItems($line_items->all(), $request->purchase_order);

        if($request->file('farm_in_attachments')) {
        	static::saveAttachments($farm_in->farm_in_id, '6', $request->file('farm_in_attachments'));
        }
        
        session(['status' => 'Peza Farm In 8105 succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        unset($request['_method']);
        Validator::make($request->all(), [
            'purchase_order' => 'required|unique:farm_in,purchase_order,'.$id.',farm_in_id,deleted_at,NULL',
            'peza_permit_status' => 'required',
            'location' => 'required|exists:sites,name',
            'vendor' => 'required|exists:vendors,name',
            'total_amount' => 'required',
            'currency' => 'required',
            'description.*' => 'required|distinct',
            'quantity.*' => 'required|integer'
        ])->validate();

        $farm_in = \App\FarmIn::find($id);
        foreach($request->except(['description', 'quantity', 'farm_in_attachments']) as $key => $value) {
            $farm_in->$key = $value;
        }
        $farm_in->save();

        \App\FarmInItems::where('purchase_order', $farm_in->purchase_order)->where('done', 0)->delete();
        $line_items = collect($request->description)->zip($request->quantity);
        $this->createLineItems($line_items->all(), $request->purchase_order);

        if($request->file('farm_in_attachments')) {
        	\App\Attachment::farmInAttachment($farm_in->farm_in_id)->delete();
        	static::saveAttachments($farm_in->farm_in_id, '6', $request->file('farm_in_attachments'));
        }
        
        session(['status' => 'Peza Farm In 8105 succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
    	$farm_in = \App\FarmIn::find($id);
        \App\Attachment::farmInAttachment($farm_in->farm_in_id)->delete();
        \App\FarmInItems::where('purchase_order', $farm_in->purchase_order)->where('done', 0)->delete();
        $farm_in->delete();
        session(['status' => 'Farm In succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function search(Request $request)
    {
        $q = $request->query('q');
        return \App\FarmIn::pendingDelivery($q)->get(['purchase_order'])->take(10)->transform(function($item){
            return $item->purchase_order;
        })->toJson();
    }

     public function details(Request $request, $purchase_order)
    {
    	$farm_in = \App\FarmIn::where('purchase_order', $purchase_order)->with(['lineItems' => function ($query) {
		    $query->where('done', 0);
		}])->first();
        return response()->json($farm_in);
    }

    private function createLineItems($items, $purchase_order)
    {
        foreach($items as $item) {
            \App\FarmInItems::firstOrCreate([
                'purchase_order' => $purchase_order,
                'description' => $item[0], 
                'quantity' => $item[1]
            ]);
        }
    }
}
