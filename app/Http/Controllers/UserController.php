<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Mail\NewUserNotification;
use App\Mail\ResetPasswordNotification;

use App\Models\User;
use App\Models\SiteAccess;
use App\Models\UserPermission;

use Validator;

class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export(Request $request, $type)
    {
        $q = $request->query('search');
        $users = User::search($q)->get();
        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('users', $users);
        }
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $users = User::search($q);
        $users = $users->paginate(15)->setPath('')->appends(['search' => $q])->toArray();

        return response()->json($users);
    }

    public function show($id)
    {
        $user = User::with('user_permissions','site_access')->find($id);
        return response()->json(array_replace(
            $user->toArray(), array_only($user->getOriginal(), 
                ['group', 'role', 'full_name', 'enabled'])
        ));
    }

    public function store(Request $request)
    {

        Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'role' => 'required',
            'group' => 'required',
            'email' => 'required|email|unique:users,email',
            'username' => 'required|unique:users,username'
        ])->validate();

        $random_str = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',5)),0,10);

        $user = User::create([
            'username' => $request->username,
            'email' => $request->email,
            'full_name' => $request->first_name. '|' .$request->middle_name. '|' .$request->last_name,
            'role' => $request->role,
            'group' => $request->group,
            'password' => bcrypt($random_str)
        ]);

        if($user) {
            if($request->scope) {
                foreach($request->scope as $scope) {
                    SiteAccess::create([
                        'site_id' => $scope,
                        'user_id' => $user->id
                    ]);
                }
            }
            if($request->modules) {
                $user_permission = array_combine($request->modules, $request->permission);

                foreach($user_permission as $key => $permission) {
                    UserPermission::create([
                        'permission' => ($permission == '') ? 'NONE' : $permission,
                        'menu_id' => $key,
                        'user_id' => $user->id
                    ]);
                }
            }
        }
        Mail::to($user->email)->send(new NewUserNotification($user, $random_str));
        session(['status' => 'User succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
       Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'role' => 'required',
            'group' => 'required',
            'email' => 'required|email|unique:users,email,'.$id.',id',
            'username' => 'required|unique:users,username,'.$id.',id'
        ])->validate();

        $user = User::find($id);
        $user->role = $request->role;
        $user->group = $request->group;
        $user->email = $request->email;
        $user->save();
        if($user) {
            SiteAccess::where('id', $user->id)->forceDelete();
            if($request->scope) {
                foreach($request->scope as $scope) {
                    SiteAccess::create([
                        'site_id' => $scope,
                        'user_id' => $user->id
                    ]);
                }
            }
            UserPermission::where('id', $user->id)->forceDelete();
            if($request->user_permission) {
                foreach($request->user_permission as $key => $permission) {
                    $privilege = explode(',', $permission);
                    UserPermission::create([
                        'permission' => ($permission == '') ? 'NONE' : $permission,
                        'menu_id' => $key,
                        'user_id' => $user->id
                    ]);
                }
            }
        }

        session(['status' => 'User succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function menus(Request $request)
    {
        $retVal = Menu::get(['menu_id', 'name', 'parent', 'transactions']);
        if($request->query('q') != '') {
            $retVal = \App\Models\Menu::where('name', 'like', $request->query('q').'%')->get(['menu_id', 'name', 'parent', 'transactions']);
        }
        return $retVal;
    }

    public function change(Request $request)
    {
        Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required|different:old_password',
            'repeat_password' => 'same:new_password'
        ])->validate();

        $user = User::find(Auth::user()->id);
        $user->password = bcrypt($request->new_password);
        $user->save();

        session(['status' => 'Password succesfully changed!']);
        return redirect()->back();
    }

    public function search(Request $request)
    {
        $q = $request->query('q');
        $users = User::where('full_name', 'like', '%'.$q.'%')->get(['username', 'full_name']);

        return $users;
    }

    public function activation(Request $request, $id)
    {
        $user = User::find($id);
        $user->enabled = $request->enabled;
        $user->save();
        $status = $user->enabled ? 'activated' : 'deactivated';
        session(['status' => 'User account succesfully ' .  $status]);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function reset(Request $request, $id) 
    {
        $random_str = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',5)),0,10);
        $user = User::find($id);
        $user->password = bcrypt($random_str);
        $user->save();

        Mail::to($user->email)->send(new ResetPasswordNotification($user, $random_str));
        session(['status' => 'User password succesfully reset']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        User::destroy($id);
        session(['status' => 'User succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
