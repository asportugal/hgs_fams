<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Models\Site;
use App\Models\Category;
use App\Models\Vendor;
use App\Models\User;
use App\Models\Department;
use App\Models\Employee;
use App\Models\business_entity;
use App\Models\Brand;
use App\Models\Notification;
use App\Models\Menu;

use DB;

class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','has_permission']);
    }

    public function new_asset(Request $request)
    {
        $emps = \Employee::get(['first_name', 'middle_name', 'last_name'])->transform(function($item){
            $name = $item->first_name .' ';
            $name .= ($item->middle_name) ? $item->middle_name . ' ' . $item->last_name : $item->last_name;
            return $name;
        });

        $data = [
            'financial_treatment' => \LookupValue::financialTreatment()->get(['name'])->toArray(),
            'vendors' => \Vendor::get(['name'])->toArray(),
            'brands' => \Brand::get(['name'])->toArray()
,            'lobs' => \LOB::get(['name'])->toArray(),
            'categories' => \Category::categories()->get(['name', 'code']),
            'employees' => $emps->toArray(),
            'sites' =>   \Site::get(['site_id', 'name', 'code'])
        ];
        return view('transactions.new_asset', $data);
    }

    public function open_records(Request $request)
    {
        $data = [
            'vendors' => Vendor::get(['name'])->toArray(),
            'brands' => Brand::get(['name'])->toArray(),
            'lobs' => LOB::get(['name'])->toArray(),
            'categories' => Category::categories()->get(['name', 'code']),
            'status' => array(),
            'sites' =>   Site::get(['name'])->toArray(),
        ];
        return view('transactions.open_records', $data);
    }

    public function bulk_upload(Request $request)
    {
        return view('transactions.bulk_upload');
    }

    public function location_transfer(Request $request)
    {
        $data = [
            'disposal_methods' => \LookupValue::disposalMethods()->get(['name'])->toArray(),
            'status' => \LookupValue::assetStatus()->get(['name'])->toArray(),
            'sites' =>   \Site::get(['name'])->toArray(),
            'loan_approver' => \Approver::retrieveAll()
        ];
        return view('transactions.location_transfer', $data);
    }

    public function asset_replacement(Request $request)
    {
        $data = [
            'disposal_methods' => \LookupValue::disposalMethods()->get(['name'])->toArray(),
            'loan_approver' => \Approver::retrieveAll()
        ];
        return view('transactions.asset_replacement', $data);
    }

    public function asset_disposal(Request $request)
    {
        $data = [
            'disposal_methods' => \LookupValue::disposalMethods()->get(['name'])->toArray(),
            'status' => \LookupValue::assetStatus()->get(['name'])->toArray(),
            'loan_approver' => \Approver::retrieveAll()
        ];
        return view('transactions.asset_disposal', $data);
    }

    public function pre_asset(Request $request)
    {
         $data = [
         	'pos' => \FarmIn::pendingDelivery()->get(['purchase_order'])->take(10)->toArray(),
            'notifications' => \Notification::get(['group_name'])->toArray(),
            'vendors' => \Vendor::get(['name'])->toArray(),
            'sites' => \Site::get(['name'])->toArray()
        ];
        return view('transactions.pre_asset', $data);
    }

    public function loan_asset(Request $request)
    {
        $data = [
            'sites' => \Site::get(['name'])->toArray(),
            'categories' => \Category::categories()->get(['name', 'code']),
            'status' => \LookupValue::assetStatus()->get(['name'])->toArray(),
            'loan_approver' => \Approver::retrieveAll()
        ];
        return view('transactions.loan_assets', $data);
    }

    public function categories(Request $request)
    {
        return view('administration.master-files.categories');
    }

    public function subcategories(Request $request)
    {

        $data = [
            'categories' => Category::categories()->get(['name', 'code'])
        ];

        return view('administration.master-files.subcategories', $data);
    }

    public function vendors(Request $request)
    {
        return view('administration.master-files.vendors');
    }

    public function brands(Request $request)
    {
        return view('administration.master-files.brands');
    }

    public function notifications(Request $request)
    {
        return view('administration.master-files.notifications');
    }

    public function sites(Request $request)
    {
        return view('administration.master-files.sites');
    }

    public function business_entity(Request $request)
    {
        return view('administration.master-files.business-entities');
    }

    public function departments(Request $request)
    {
        return view('administration.master-files.departments');
    }

    public function employees(Request $request)
    {
        $data = [
            'emp_status' => \LookupValue::employeeStatus()->get(['name'])->toArray(),
            'departments' => \Department::get(['name'])->toArray()
        ];
        return view('administration.master-files.employees', $data);
    }

    public function lob(Request $request)
    {
        return view('administration.master-files.lob');
    }

    public function user_management(Request $request)
    {
        $data = [
            'menus' => Menu::get(['id', 'name', 'parent', 'transactions'])->toJson(),
            'groups' => User::$groups,
            'sites' => Site::get(['name', 'id'])->toJson(),
            'roles' => objarr_encode(User::$roles)
        ];

    	return view('administration.user-management', $data);
    }

    public function lookup_values()
    {
        $data = [
            'tags' => \LookupValue::dynamicValues()->distinct()->get(['tag'])->toArray()
        ];
        return view('administration.lookup-values', $data);
    }

    public function request_approvers()
    {
        $data = [
            'users' => \User::get(['username', 'full_name'])
        ];
        return view('administration.request-approvers', $data);
    }

    public function change_password()
    {
        return view('auth.passwords.change');
    }

    public function warranty_report(\App\Models\Asset $asset)
    {
        $warranty_report = [];

        foreach(Category::categories()->get(['name']) as $category) {
            $out_warranty = $asset::select(DB::raw("count(asset_id) as asset_count"))
                ->whereDate('warranty_end_date', '<', date('m/d/Y'))
                ->where('category', $category->name)->count();
            $warranty_report['category'][] = ['label' => $category->name, 'asset_count' => $out_warranty];
        }

        foreach(Site::get(['name']) as $site) {
            $out_warranty = $asset::select(DB::raw("count(asset_id) as asset_count"))
                ->whereDate('warranty_end_date', '<', date('m/d/Y'))
                ->where('bldg', $site->name)->count();
            $warranty_report['site'][] = ['label' => $site->name, 'asset_count' => $out_warranty];
        }

        foreach(Category::subcategories()->get(['name']) as $subcategory) {
            $out_warranty = $asset::select(DB::raw("count(asset_id) as asset_count"))
                ->whereDate('warranty_end_date', '<', date('m/d/Y'))
                ->where('subcategory', $subcategory->name)->count();
            $warranty_report['subcategory'][] = ['label' => $subcategory->name, 'asset_count' => $out_warranty];
        }
        $options = objarr_encode(array_keys($warranty_report));
        return view('report.warranty_report', ['report' => $warranty_report, 'options' => $options]);
    }

    public function farm_in()
    {
        $data = [
            'sites' => \Site::get(['name'])->toArray(),
            'vendors' => \Vendor::get(['name'])->toArray(),
        ];

        return view('transactions.farm_in', $data);
    }

    public function farm_out()
    {
        return view('transactions.farm_out');
    }

    public function print_assets()
    {
        $handler = printer_list(PRINTER_ENUM_LOCAL| PRINTER_ENUM_SHARED);
        $printers = array_map(function($item) { return $item['NAME'];  }, $handler);
        $data = [
            'brands' => \Brand::get(['name'])->toArray(),
            'vendors' => \Vendor::get(['name'])->toArray(),
            'sites' => \Site::get(['name'])->toArray(),
            'status' => \LookupValue::assetStatus()->get(['name'])->toArray(),
            'categories' => \Category::categories()->get(['name'])->toArray(),
            'lobs' => \LOB::get(['name'])->toArray(),
            'printers' => $printers
        ];
        return view('administration.print-assets', $data);
    }
}
