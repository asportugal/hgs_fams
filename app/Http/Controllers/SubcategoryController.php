<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
class SubcategoryController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export(Request $request, $type)
    {
        $q = $request->query('search');
        $subcategories = \App\Category::search($q)->subcategories()->get();
        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('subcategories', $subcategories);
        }
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $subcategories = \App\Category::search($q)->subcategories();
        $subcategories = $subcategories->paginate(15)->setPath('')->appends(['search' => $q])->toArray();
        return response()->json($subcategories);
    }

    public function name($code)
    {
        return \App\Category::subcategories($code)->get(['name']);
    }

    public function show($id)
    {
        $subcat_id = base64_decode($id);
        $subcategory = \App\Category::find($subcat_id);
        return response()->json($subcategory);
    }

    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->all()), [
            'subcategory_name' => 'required|unique:categories,name,NULL,NULL,deleted_at,NULL',
            'category' => 'required|alphanum|exists:categories,code,type,CATEGORY,deleted_at,NULL'
        ])->validate();

        \App\Category::create(array_map('trim', [
            'type' => 'SUBCATEGORY',
            'name' => $request->subcategory_name,
            'code' => $request->category
        ]));
        
        session(['status' => 'Subcategory succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        Validator::make(array_map('trim', $request->all()), [
            'subcategory_name' => 'required|unique:categories,name,'.$id.',category_id,deleted_at,NULL',
            'category' => 'required|alphanum|exists:categories,code,type,CATEGORY,deleted_at,NULL'
        ])->validate();

        $category = \App\Category::find($id);
        $category->name = trim($request->subcategory_name);
        $category->code = trim($request->category);
        $category->save();

        session(['status' => 'Subcategory succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\Category::destroy($id);
        session(['status' => 'Category succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
