<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Contracts\Pagination\Paginator;
use Validator;
class CategoryController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export(Request $request, $type)
    {
        $q = $request->query('search');
        $categories = \App\Category::search($q)->categories()->get();
        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('categories', $categories);
        }
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $categories = \App\Category::search($q)->categories();
        $categories = $categories->paginate(15)->setPath('')->appends(['search' => $q])->toArray();
        return response()->json($categories);
    }

    public function show($id)
    {
        $category_id = base64_decode($id);
        $category = \App\Category::find($category_id);
        return response()->json($category);
    }

    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->all()), [
            'category_name' => 'required|unique:categories,name,NULL,NULL,type,CATEGORY,deleted_at,NULL',
            'category_code' => 'required|alphanum|unique:categories,code,NULL,NULL,type,CATEGORY,deleted_at,NULL'
        ])->validate();

        \App\Category::create(array_map('trim', [
            'type' => 'CATEGORY',
            'name' => $request->category_name,
            'code' => $request->category_code
        ]));
        
        session(['status' => 'Category succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        Validator::make(array_map('trim', $request->all()), [
            'category_name' => 'required|unique:categories,name,'.$id.',category_id,type,CATEGORY,deleted_at,NULL',
            'category_code' => 'required|alphanum|unique:categories,code,'.$id.',category_id,type,CATEGORY,deleted_at,NULL'
        ])->validate();

        $category = \App\Category::find($id);
        $category->name = trim($request->category_name);
        $category->code = trim($request->category_code);
        $category->save();

        session(['status' => 'Category succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\Category::destroy($id);
        session(['status' => 'Category succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
