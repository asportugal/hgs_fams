<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Traits\FileHelper;
use Validator;
class FarmOutController extends Controller
{
    use FileHelper;
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export(Request $request, $type)
    {
        $q = $request->query('search');
        $farm_out = \App\FarmOut::search($q)->get();
        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('farm_out', $farm_out);
        }
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $farm_out = \App\FarmOut::search($q);
        $farm_out = $farm_out->paginate(10)->setPath('')->appends(['search' => $q])->toArray();
        return response()->json($farm_out);
    }

    public function show($id)
    {
        $farm_out_id = base64_decode($id);
        $farm_out = \App\FarmOut::find($farm_out_id);
        return response()->json($farm_out);
    }

    public function update(Request $request, $id)
    {
        unset($request['_method']);
        Validator::make($request->all(), [
            'control_no' => 'required|unique:farm_out,control_no,'.$id.',farm_out_id,deleted_at,NULL',
            'peza_permit_status' => 'required'
        ])->validate();

        $farm_out = \App\FarmOut::find($id);
        foreach($request->except(['farm_out_attachments']) as $key => $value) {
            $farm_out->$key = $value;
        }
        $farm_out->save();

        if($request->file('farm_out_attachments')) {
        	\App\Attachment::FarmOutAttachment($farm_out->farm_out_id)->delete();
        	static::saveAttachments($farm_out->farm_out_id, '7', $request->file('farm_out_attachments'));
        }
        
        session(['status' => 'Peza Farm Out succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
    	$farm_out = \App\FarmOut::find($id);
        \App\Attachment::FarmOutAttachment($farm_out->farm_out_id)->delete();
        \App\FarmOutItems::where('purchase_order', $farm_out->purchase_order)->where('done', 0)->delete();
        $farm_out->delete();
        session(['status' => 'Farm In succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function search(Request $request)
    {
        $q = $request->query('q');
        return \App\FarmOut::pendingDelivery($q)->get(['purchase_order'])->take(10)->transform(function($item){
            return $item->purchase_order;
        })->toJson();
    }

     public function details(Request $request, $purchase_order)
    {
    	$farm_out = \App\FarmOut::where('purchase_order', $purchase_order)->with(['lineItems' => function ($query) {
		    $query->where('done', 0);
		}])->first();
        return response()->json($farm_out);
    }
}
