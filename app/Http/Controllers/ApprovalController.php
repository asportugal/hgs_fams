<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class ApprovalController extends Controller
{
	public function __construct()
	{
		$this->middleware(['auth']);
	}

	public function details($id) {
		$data = ['request' => \App\Approval::find($id)];
		return view('approvals.request', $data);
	}

	public function action(Request $request) {
		$param = explode('&', substr(base64_decode($request->query('aid')), 1));
		$id = str_replace('id=', '', $param[0]);
		$action = str_replace('action=', '', $param[1]);
		$approver = str_replace('approver=', '', $param[2]);
		$approval = \App\Approval::find($id);
		$approver_name = \App\User::where('username', $approver)->first();
		$approval->status = strtoupper($action.'d');
		$approval->approver = $approver_name->full_name;
		$approval->save();
	}

}