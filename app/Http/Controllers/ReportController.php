<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function  __construct() 
    {
    	$this->middleware(['auth']);
    }

    public function warranty($scope)
    {
        $assets = '';
        if($scope == 'site') {
            $sites = [];
            foreach(\App\Site::get(['name']) as $site) {
                $out_warranty = \App\Asset::select(DB::raw("count(asset_id) as asset_count"))
                    ->where('warranty_end_date', '<', date('m/d/Y'))
                    ->where('bldg', $site->name)->count();
                $sites[] = ['label' => $site->name, 'asset_count' => $out_warranty];
            }
            $assets = $sites;
        }

        if($scope == 'category') {
            $categories = [];
            foreach(\App\Category::categories()->get(['name']) as $category) {
                $out_warranty = \App\Asset::select(DB::raw("count(asset_id) as asset_count"))
                    ->where('warranty_end_date', '<', date('m/d/Y'))
                    ->where('category', $category->name)->count();
                $categories[] = ['label' => $category->name, 'asset_count' => $out_warranty];
            }
            $assets = $categories;
        }

        return response()->json($assets);
    }
}
