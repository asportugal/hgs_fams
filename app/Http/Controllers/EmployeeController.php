<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
class EmployeeController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export(Request $request, $type)
    {
        $q = $request->query('search');
        $employees = \App\Employee::search($q)->get();
        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('employees', $employees);
        }
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $employees = \App\Employee::search($q);
        $employees = $employees->paginate(10)->setPath('')->appends(['search' => $q])->toArray();
        return response()->json($employees);
    }

    public function name(Request $request)
    {
        if($request->query('eid')) {
            $emp_name = base64_decode($request->query('eid'));
            return \App\Employee::findByName($emp_name)->first();
        }
    }

    public function show($id)
    {
        $emp_id = base64_decode($id);
        $employee = \App\Employee::find($emp_id);
        return response()->json($employee);
    }

    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->except('employee_image')), [
            'date_hired' => 'required',
            'department' => 'required|exists:departments,name',
            'department_head' => 'required',
            'email' => 'required|email',
            'employee_no' => 'required|unique:employees,employee_no,NULL,NULL,deleted_at,NULL',
            'employee_status' => 'required|required|exists:lookup_values,name,tag,EMPLOYEE_STATUS',
            'first_name' => 'required',
            'last_name' => 'required',
            'supervisor' => 'required',
        ])->validate(); 

        $employee = \App\Employee::create(array_map('trim', $request->except('employee_image')));
        
        if($request->employee_image) 
        {
            \App\Attachment::create([
                'reference_id' => $employee->employee_id,
                'attachment_type' => '4',
                'file_name' => $request->employee_image->getClientOriginalName(), 
                'file_size' => $request->employee_image->getSize(),
                'mime_type' => $request->employee_image->getMimeType(),
                'data' => $request->employee_image
            ]);
        }

        session(['status' => 'Employee succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        unset($request['_method']);
        Validator::make(array_map('trim', $request->except('employee_image')), [
            'date_hired' => 'required',
            'department' => 'required|exists:departments,name',
            'department_head' => 'required',
            'email' => 'required|email',
            'employee_no' => 'required|unique:employees,employee_no,'.$id.',employee_id,deleted_at,NULL',
            'employee_status' => 'required|required|exists:lookup_values,name,tag,EMPLOYEE_STATUS',
            'first_name' => 'required',
            'last_name' => 'required',
            'supervisor' => 'required',
        ])->validate();

        $employee = \App\Employee::find($id);
        foreach($request->except(['employee_image']) as $key => $value) {
            $employee->{$key} = $value;
        }
        $employee->save();

        if($request->employee_image) 
        {
            $image = \App\Attachment::updateOrCreate(
                ['attachment_type' => '4', 'reference_id' => $employee->employee_id],
                ['file_name' => $request->employee_image->getClientOriginalName(), 
                'file_size' => $request->employee_image->getSize(),
                'mime_type' => $request->employee_image->getMimeType(),
            'data' => $request->employee_image]);
        }

        session(['status' => 'Employee succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function search(Request $request)
    {
        $q = $request->query('q');
        $employees = \App\Models\Employee::asyncSearch($q)->get(['first_name', 'middle_name', 'last_name'])->transform(function($item){
            $name = $item->first_name .' ';
            $name .= ($item->middle_name) ? $item->middle_name . ' ' . $item->last_name : $item->last_name;
            return $name;
        })->toJson();

        return $employees;
    }

    public function destroy($id)
    {
        \App\Employee::destroy($id);
        \App\Attachment::employeeImage($id)->first()->delete();
        session(['status' => 'Employee succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
