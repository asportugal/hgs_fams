<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
class BusinessEntityController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export(Request $request, $type)
    {
        $q = $request->query('search');
        $lobs = \App\LOB::search($q)->get();
        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('lobs', $lobs);
        }
    }

    public function show($id)
    {
        $lob_id = base64_decode($id);
        $lob = \App\LOB::find($lob_id);
        return response()->json($lob);
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $lobs = \App\LOB::search($q);
        $lobs = $lobs->paginate(15)->setPath('')->appends(['search' => $q])->toArray();
        return response()->json($lobs);
    }
    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->all()), [
            'lob' => 'required|unique:business_entity,name,NULL,NULL,deleted_at,NULL'
        ])->validate();

        \App\LOB::create(array_map('trim', [
            'name' => $request->lob,
        ]));
        
        session(['status' => 'LOB succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        Validator::make(array_map('trim', $request->all()), [
            'lob' => 'required|unique:business_entity,name,'.$id.',business_entity_id,deleted_at,NULL'
        ])->validate();

        $brand = \App\LOB::find($id);
        $brand->name = trim($request->lob);
        $brand->save();    

        session(['status' => 'LOB succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\LOB::destroy($id);
        session(['status' => 'LOB succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
