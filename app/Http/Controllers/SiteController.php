<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
class SiteController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function export(Request $request, $type)
    {
        $q = $request->query('search');
        $sites = \App\Site::search($q)->get();
        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('sites', $sites);
        }
    }

    public function index(Request $request)
    {
        $q = $request->query('search');
        $sites = \App\Site::search($q);
        $sites = $sites->paginate(15)->setPath('')->appends(['search' => $q])->toArray();
        return response()->json($sites);
    }

    public function show($id)
    {
        $site_id = base64_decode($id);
        $site = \App\Site::find($site_id);
        return response()->json($site);
    }

    public function store(Request $request)
    {
        Validator::make(array_map('trim', $request->all()), [
            'site_name' => 'required|unique:sites,name,NULL,NULL,deleted_at,NULL',
            'site_code' => 'required|unique:sites,code,NULL,NULL,deleted_at,NULL'
        ])->validate();

        \App\Site::create(array_map('trim', [
            'name' => $request->site_name,
            'code' => $request->site_code,
            'address' => $request->address
        ]));
        
        session(['status' => 'Site succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        Validator::make(array_map('trim', $request->all()), [
            'site_name' => 'required|unique:sites,name,'.$id.',site_id,deleted_at,NULL',
            'site_code' => 'required|unique:sites,code,'.$id.',site_id,deleted_at,NULL'
        ])->validate();

        $site = \App\Site::find($id);
        $site->name = trim($request->site_name);
        $site->code = trim($request->site_code);
        $site->address = trim($request->address);
        $site->save();    

        session(['status' => 'Site succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function destroy($id)
    {
        \App\Site::destroy($id);
        session(['status' => 'Site succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
