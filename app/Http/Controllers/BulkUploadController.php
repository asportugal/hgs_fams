<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Validator;
class BulkUploadController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth');
    }

    public function messages($rowNumber) 
    {
    	$exception_string = 'Error on row no. ' . $rowNumber . ' ';
    	return [
			'required' => $exception_string . 'missing value on column :attribute.', 
			'exists' => $exception_string .'column :attribute value does not exist in :attribute table.',
			'email' => $exception_string . 'incorrect email address',
			'date' => $exception_string . 'column :attribute is incorrect, make sure it is on "mm/dd/yyyy" format.',
            'unique' => $exception_string . ':attribute has already been taken.'
        	];
    }

    public function excelParser($file)
    {
    	return Excel::selectSheets('Sheet1')->load($file, function($reader) {
    		$reader->setDateFormat('Y-m-d H:i:s.000');
    	})->get();
    }

    public function assets(Request $request)
    {   
        $result = $this->excelParser($request->assets);
        DB::beginTransaction();
        $i = 1;
        foreach($result as $row) {
            $validator = Validator::make($row->all(), $this->asset_validator($request->type, $row->all()), $this->messages($i));
            if($validator->fails()) {
                DB::rollBack();
                session(['file_content' => $result]);
                return redirect()->back()->withErrors($validator)->withInput();
            }

            $notes = ['notes' => $request->comment];
            if($request->type == 'create') {
                $date = date('m/d/y', strtotime($row->delivery_receipt_date));
                $asset_id = ['asset_id' => \App\Asset::createAssetID($row->category, $row->bldg, $date)];
                $post_asset_data = \App\Asset::defaultInitialValues($row->toArray());
                \App\Asset::create(array_map('trim', array_merge($post_asset_data, $asset_id, $notes)));
            } else if($request->type == 'update') {
                $asset = \App\Asset::find($row->asset_id);
                $asset->update($row->toArray());
            }
            $i++;
        }
        DB::commit();
        $trans = ($request->type == 'create') ? 'new records added to database' : 'records has been succesfully updated';
        session(['status' => 'File successfully loaded! ' . count($result) . ' '. $trans]);
        return redirect()->back();
    }

    public function asset_validator($type, $data) {
        $validator = [
            'category' => 'exists:categories,name,type,CATEGORY',
            'subcategory' => 'exists:categories,name,type,SUBCATEGORY',
            'bldg' => 'exists:sites,name',
            'supplier' => 'exists:vendors,name',
            'brand' => 'exists:brands,name',
            'serial_no' => 'required|unique:assets,serial_no,NONE,serial_no',
            'delivery_receipt_date' => 'required|date'
        ];
        if($type == 'update') {
            $validator['asset_id'] = 'required|exists:assets,asset_id';
            $validator['serial_no'] = 'required|unique:assets,serial_no,'.$data['asset_id'].',asset_id,serial_no,!NONE';
        }
        return $validator;
    }

    public function employees(Request $request)
    {
    	$result = $this->excelParser($request->employees->getPathName());
    	DB::beginTransaction();
    	$i = 1;
    	foreach($result as $row) {
    		$validator = Validator::make(array_map('trim', $row->all()), [
				'employee_no' => 'required|unique:employees,employee_no',
				'first_name' => 'required',
				'last_name' => 'required',
				'department' => 'required|exists:departments,name',
				'employee_status' => 'required|exists:lookup_values,name,tag,EMP_STATUS',
				'department_head' => 'required',
				'supervisor' => 'required',
				'email' => 'required|email',
				'date_hired' => 'required|date',
			], $this->messages($i));

    		if($validator->fails()) {
    			DB::rollBack();
    			return redirect()->back()
    				->withErrors($validator)
    				->withInput();
    		}
    		\App\Employee::create(array_map('trim', $row->all()));
    		$i++;
    	}
    	DB::commit();
    	session(['status' => 'File successfully loaded! ' . count($result) . ' new records added to database.']);
    	return redirect()->back();
    }
}
