<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Traits\FileHelper;
use App\Mail\PreAssetNotification;
use Illuminate\Pagination\LengthAwarePaginator;
use Validator;
class PreAssetController extends Controller
{
	use FileHelper;
    public function  __construct() 
    {
    	$this->middleware(['auth']);
    }

    public function export(Request $request, $type)
    {
        if($request->query('narrow')) {
             $parameters = array_except($request->query(), ['narrow', 'page']);
             $pre_assets = \App\Models\PreAsset::narrowSearch($parameters)->get();
            
        } else {
            $q = $request->query('search');
            $pre_assets = \App\Models\PreAsset::search($q)->get();
        }

        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('export', $pre_assets);
        }
    }

    public function pendings_export(Request $request, $type)
    {
        if($request->query('narrow')) {
            $parameters = array_except($request->query(), ['narrow', 'page']);
            $query = \App\Models\PreAsset::narrowSearch($parameters)->with('farmIn')->get();
        } else {
            $q = $request->query('search');
            $query = \App\Models\PreAsset::search($q)->with('farmIn')->get();
        }

        $pre_assets = $query->filter(function($value, $key){
            return ($value->tagged_qty != $value->quantity); 
        });

        if(in_array($type, ['excel', 'csv'])){
            \App\Traits\Export::{$type}('export', $pre_assets);
        }
    }

    public function pending(Request $request)
    {
        if($request->query('narrow')) {
            $parameters = array_except($request->query(), ['narrow', 'page']);
            $query = \App\Models\PreAsset::narrowSearch($parameters)->with('farmIn')->get();
        } else {
            $q = $request->query('search');
            $query = \App\Models\PreAsset::search($q)->with('farmIn')->get();
        }
        $pre_assets = $query->filter(function($value, $key){
            return ($value->tagged_qty != $value->quantity); 
        });
        $page = $request->query('page') ? $request->query('page') : 1;
        $deliveries = new LengthAwarePaginator(array_merge($pre_assets->forPage($page, 10)->toArray()), $pre_assets->count(), 10);

        return response()->json($deliveries);
    }

    public function index(Request $request)
    {
        if($request->query('narrow')) {
             $parameters = array_except($request->query(), ['narrow', 'page']);
             $pre_assets = \App\Models\PreAsset::narrowSearch($parameters)->with('farmIn')->paginate(10);
             $pre_assets->setPath('')->toArray();
        } else {
            $q = $request->query('search');
            $pre_assets = \App\Models\PreAsset::search($q)->with('farmIn')->paginate(10);
            $pre_assets->setPath('')->toArray();
        }
        return response()->json($pre_assets);
    }

    public function show($id)
    {
        $delivery_id = base64_decode($id);
        $delivery = \App\Models\PreAsset::with(['farmInItems' => function($query){
            $query->addSelect(['delivery_id', 'description']);
        }])->find($delivery_id);
        return response()->json($delivery);
    }

    public function store(Request $request)
    {
        Validator::make($request->except('delivery_attachments'), [
			'vendor_purchase_order_no' => 'required|exists:farm_in,purchase_order',
			'vendor_delivery_no' => 'required',
			'airway_delivery_date' => 'date',
            'description' => 'required',
			'vendor_delivery_date' => 'required|date',
			'bldg' => 'exists:sites,name',
			'brand' => 'exists:brands,name',
			'vendor_name' => 'exists:vendors,name',
			'notification' => 'required|exists:notifications,group_name'
        ])->validate();

        $pre_asset = \App\Models\PreAsset::create(array_map('trim', 
    		$request->except(['delivery_attachments', 'tagged_quantity', 'notification', 'description'])
    	));

    	\App\FarmInItems::where('purchase_order', $pre_asset->vendor_purchase_order_no)
    		->whereIn('description', $request->description)
    		->update(['done' => 1, 'delivery_id' => $pre_asset->delivery_id]);

        $files = [];
    	if($request->file('delivery_attachments')) {
        	static::saveAttachments($pre_asset->delivery_id, '2', $request->file('delivery_attachments'));
            $files = \App\Models\Attachment::deliveryAttachment($pre_asset->delivery_id)->get()->toArray();
        }

        $deliveries = array_merge($pre_asset->toArray(), ['line_items' => $pre_asset->farmInItems->toArray(), 'attachments' => $files]);
        $notification = \App\Models\Notification::where('group_name', $request->notification)->first();
        $carbon = $notification->cc_address ? explode(";", $notification->cc_address) : [];
        $to = $notification->to_address ? explode(";", $notification->cc_address) : [];

        Mail::to($to)
            ->cc($carbon)
            ->send(new PreAssetNotification($deliveries));
		
        session(['status' => 'Pre Asset succesfully created!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function update(Request $request, $id)
    {
        unset($request['_method'], $request['tagged_quantity']);
        Validator::make(array_map('trim', $request->except('delivery_attachments')), [
            'vendor_purchase_order_no' => 'required|exists:farm_in,purchase_order',
            'vendor_delivery_no' => 'required',
            'quantity'  => 'required|numeric',
            'description' => 'required',
            'airway_delivery_date' => 'date',
            'vendor_delivery_date' => 'required|date',
            'bldg' => 'exists:sites,name',
            'brand' => 'exists:brands,name',
            'vendor_name' => 'exists:vendors,name'
        ])->validate();
        
        $pre_asset = \App\Models\PreAsset::find($id);
        foreach($request->except(['delivery_attachments', 'tagged_quantity', 'notification']) as $key => $value) {
            $pre_asset->$key = $value;
        }
        $pre_asset->save();

        if($request->file('delivery_attachments')) {
            \App\Models\Attachment::deliveryAttachment($pre_asset->delivery_id)->delete();
            static::saveAttachments($pre_asset->delivery_id, '2', $request->file('delivery_attachments'));
        }
        session(['status' => 'Pre Asset succesfully updated!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }

    public function items($po)
    {
        $line_items = \App\Models\FarmInItems::where('purchase_order', $po)->get();
        $line_items->transform(function($item, $key){
            return $item->description;
        });
        return response()->json($line_items); 
    }

    public function destroy($id)
    {
        \App\Models\PreAsset::destroy($id);
        \App\Models\Attachment::deliveryAttachment($id)->delete();
        session(['status' => 'Pre Asset succesfully deleted!']);
        return response('success', 200)->header('Content-Type', 'text/plain');
    }
}
