<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$data = [
    		// 'requests' => \App\Approval::where('status', 'PENDING')->limit(5)->get(),
            'role' => strtoupper(Auth::user()->role)
    	];
        return view('dashboard.index', $data);
    }
}
