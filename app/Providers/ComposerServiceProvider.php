<?php

namespace App\Providers;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\File;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return public
     */
    public function boot()
    {
        View::composer(['partials.sidebar', 'partials.buttons'], function ($view) {
            $menus = [];
            foreach(Auth::user()->permissions() as $menu)
            {
                $activeClass = '';
                if(url($menu['link']) == url()->current()) {
                    $menus['active']['parent'] = $menu['parent'];
                    $menus['active']['sub'] = $menu['sub'];
                    $activeClass = 'active';
                    $view->with('buttons', explode('|', $menu['transactions']));
                }
                $menus['items'][$menu['parent']][$menu['order']] = [
                    'parent' => $menu['parent'],
                    'sub' => $menu['sub'],
                    'name' => $menu['name'],
                    'link' => url($menu['link']),
                    'active' => $activeClass
                ];
            }

            $menus['active'] = isset($menus['active']) ? $menus['active'] : ['parent' => '', 'sub' => ''];
            $menus['items'] = array_map(function($arr) {
                                ksort($arr);
                                return $arr;
                            }, array_values($menus['items']));
            $view->with('sidebar', $menus);
        });
    }

    public function register()
    {
        
    }
}