<?php

namespace App\Providers;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return public
     */
    public function boot()
    {
        Blade::extend(function($view, $compiler) {
            $pattern = '!\@setvar\(\s*([^,]+)\s*,\s*(.+)\s*\)!';
            return preg_replace($pattern, '<?php ${$1} = $2 ?>', $view);
        });

        Blade::directive('clock', function ($expression) {
            return "<?php echo date({$expression}) ?>";
        });

        Blade::directive('cleanstr', function ($expression) {
            $expression = str_replace('_', ' ' , $expression);
            return "<?php echo {$expression} ?>";
        });

        Blade::directive('dataUri', function ($expression) {
            $image = base64_encode(file_get_contents(asset($expression)));
            $mime = mime_content_type($image);
            $uri = 'data:'.$mime.';base64,'. $image;
            return '<?php echo {$uri}; ?>';
        });

        Blade::directive('dump', function ($expression) {
            return "<?php echo '<pre>'; print_r({$expression}); ?>";
        });

        Blade::directive('date', function ($expression) {
            return "<?php echo date($expression); ?>";
        });

        Blade::directive('arr2str', function($expression) {
            return "<?php echo e(json_encode(array_flatten($expression))); ?>";
        });

        Blade::directive('tableRow', function ($expression) {
            $var = '$temp_arr'; $val = [];
            return "<?php {$var} = {$expression};
                while(key({$var}) !== NULL) {
                    echo '<td name=\"'.key({$var}).'\" >' . current({$var}) . '</td>';
                    next({$var}); } ?>";
        });

    }

    public function register()
    {
        
    }
}