<?php
if (!function_exists('objarr_encode')) {
	function objarr_encode($arr) {
		$options = [];
		foreach($arr as $key => $val) {
			$options[] = [
				'id' => $key, 
				'name' => title_case($val)
			];
		}
		return json_encode($options);
	}
}

if (!function_exists('encryptKey')) {
	function encryptKey($id) {
        $key = md5(env('KEY'), true);
	    $id = base_convert($id, 10, 36);
	    $data = mcrypt_encrypt(MCRYPT_BLOWFISH, $key, $id, 'ecb');
	    $data = bin2hex($data);
	    return $data;
	}
}

if (!function_exists('decryptKey')) {
	function decryptKey($encrypted_id) {
		$key = md5(env('KEY'), true);
	    $data = pack('H*', $encrypted_id);
	    $data = mcrypt_decrypt(MCRYPT_BLOWFISH, $key, $data, 'ecb');
	    $data = base_convert($data, 36, 10);
	    return $data;
	}
}